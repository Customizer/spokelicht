package de.spokelicht.android.control;

/**
 * Created by ypsilon on 14.02.15.
 */
public class Global {
    public static final BluetoothFacade frontWheelFacade = new BluetoothFacade();
    public static final BluetoothFacade rearWheelFacade = new BluetoothFacade();

    public static BluetoothFacade getFrontWheelFacade() {
        return frontWheelFacade;
    }

    public static BluetoothFacade getRearWheelFacade() {
        return rearWheelFacade;
    }
}
