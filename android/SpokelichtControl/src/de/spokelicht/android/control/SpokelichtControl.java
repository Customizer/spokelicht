package de.spokelicht.android.control;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.Toast;
import org.androidannotations.annotations.*;
import org.androidannotations.annotations.res.StringRes;

import javax.security.auth.Subject;
import java.util.Set;

@EActivity(R.layout.main)
@OptionsMenu(R.menu.spokelichtcontrolmenu)
public class SpokelichtControl extends FragmentActivity implements DeviceChoserDialog.DeviceChoserResultListener {
    @ViewById(R.id.pager)
    ViewPager pager;

    private static final int REQUEST_ENABLE_BT = 1;

    private ControlFragment frontWheelFragment;
    private ControlFragment rearWheelFragment;

    private int instanceId;
    private static int instanceCount = 0;


    public SpokelichtControl() {
        this.instanceId = instanceCount++;
        Log.d("Instance","SpokelichtControl created " + instanceId);
        frontWheelFragment = new ControlFragment_();
        rearWheelFragment = new ControlFragment_();
    }

    /**
     * Called when the activity is first created.
     */
    @AfterViews
    public void afterViewsCreated() {
        Log.d("Instance","SpokelichtControl#afterViewsCreated " + instanceId);

        frontWheelFragment.setFrontWheel(true);
        rearWheelFragment.setFrontWheel(false);

        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        if (pager!=null && pager.getAdapter() == null) {
            Fragment [] fragments = new Fragment[] {frontWheelFragment,rearWheelFragment};
            // since FragmentPagerAdapter does not support screen rotation properly we have to do the
            // work manually.
            // See:
            // http://tinyurl.com/issue54823
            // and
            // http://tinyurl.com/stackoverflowOn54823
            // for details.

            final FragmentManager supportFragmentManager = getSupportFragmentManager();
            pager.setAdapter(new PagerAdapter() {
                @Override
                public Object instantiateItem(ViewGroup container, int i) {
                    Fragment fragment = fragments[i];
                    FragmentTransaction trans = supportFragmentManager.beginTransaction();
                    trans.add(container.getId(),fragment,"fragment:"+i);
                    trans.commit();
                    return fragment;
                }

                @Override
                public boolean isViewFromObject(View view, Object o) {
                    return ((Fragment) o).getView() == view;
                }
                @Override
                public int getCount() {
                    return fragments.length;
                }
                @Override
                public void destroyItem(ViewGroup container, int position, Object object) {
                    assert(0 <= position && position < 2);
                    FragmentTransaction trans = supportFragmentManager.beginTransaction();
                    trans.remove(fragments[position]);
                    trans.commit();
                    fragments[position] = null;
                }

            });
        }

    }

    @OptionsItem(R.id.action_chose_device)
    void showDeviceChoser() {
        DialogFragment deviceChoser = new DeviceChoserDialog_();
        deviceChoser.show(getSupportFragmentManager(), "devicechoser");

    }

    @Override
    public void deviceChosen(BluetoothInfo btInfo, boolean front) {
        Toast toast = Toast.makeText(getBaseContext(), btInfo.toString() + front, Toast.LENGTH_LONG);
        if (front) {
            frontWheelFragment.connect(btInfo);
        } else {
            rearWheelFragment.connect(btInfo);
        }
        toast.show();
    }
}
