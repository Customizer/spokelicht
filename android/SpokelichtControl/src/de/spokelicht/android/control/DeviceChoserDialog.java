package de.spokelicht.android.control;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.*;
import org.androidannotations.annotations.*;
import org.androidannotations.annotations.res.StringRes;

import java.util.Set;

/**
 * Created by ypsilon on 10.02.15.
 */
@EFragment(R.layout.devicechoser)
public class DeviceChoserDialog extends DialogFragment {

    @ViewById(R.id.lv_paired_devices)
    ListView pairedDevices;

    @ViewById(R.id.lv_scanned_devices)
    ListView scannedDevices;

    @ViewById(R.id.b_toggle_scanning)
    Button toggleScanning;

    @ViewById(R.id.tv_no_scan_results)
    TextView noScanResults;

    @ViewById(R.id.pb_scanning)
    ProgressBar scanningProgress;

    @ViewById(R.id.rg_front_rear_radios)
    RadioGroup frontReadRadios;

    @StringRes(R.string.bt_start_scanning)
    String buttonTextStartScanning;

    @StringRes(R.string.bt_stop_scanning)
    String buttonTextStopScanning;

    @StringRes(R.string.nt_select_front_rear)
    String toastTextSelectFrontRear;

    @StringRes(R.string.nt_select_device)
    String toastTextSelectDevice;

    BluetoothInfo selectedInfo;
    boolean scanning = false;

    BluetoothBroadcastReceiver bluetoothBroadcastReceiver;
    private ArrayAdapter<BluetoothInfo> scannedDevicesAdapter;
    private BluetoothAdapter bluetoothAdapter;

    public interface DeviceChoserResultListener {
        final boolean FRONT = true;
        final boolean REAR = false;

        void deviceChosen(BluetoothInfo btInfo, boolean front);
    }

    @Receiver(actions = BluetoothDevice.ACTION_FOUND)
    void deviceFound(Intent intent) {
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        // If it's already paired, skip it, because it's been listed already
        if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
            scannedDevicesAdapter.add(new BluetoothInfo(device));
        }
        Log.d("Debug:BluetoothBroadcastReceiver.deviceFound", intent.getAction());
    }

    @Receiver(actions = BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
    void discoveryFinished(Intent intent) {
        // set Scanning to true so that the correct action is one
        // by #toggleScanning()
        scanning = true;
        toggleScanning();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bluetoothAdapter.cancelDiscovery();
    }

    @Click(R.id.b_toggle_scanning)
    void toggleScanning() {

        if (scanning) {
            bluetoothAdapter.cancelDiscovery();
            scanning = false;
            toggleScanning.setText(buttonTextStartScanning);
            scanningProgress.setVisibility(View.GONE);
        } else {
            scannedDevicesAdapter.clear();
            scannedDevices.setVisibility(View.VISIBLE);
            noScanResults.setVisibility(View.GONE);
            scanningProgress.setVisibility(View.VISIBLE);
            toggleScanning.setText(buttonTextStopScanning);
            scanning = true;
            // Start the discovery. Any discovered device will trigger an
            // ACTION_FOUND event, which will be handled by #deviceFound()
            bluetoothAdapter.startDiscovery();

        }
    }

    @AfterViews
    void initPairedDevices() {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        ArrayAdapter<BluetoothInfo> pairedDevicesAdapter = new ArrayAdapter<>(this.getActivity().getApplicationContext(),
                R.layout.device_name);

        Set<BluetoothDevice> bondedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        for (BluetoothDevice bondedDevice : bondedDevices) {
            pairedDevicesAdapter.add(new BluetoothInfo(bondedDevice));
        }
        pairedDevices.setAdapter(pairedDevicesAdapter);
    }


    @AfterViews
    void initScannedDevices() {
        scannedDevicesAdapter = new ArrayAdapter<>(this.getActivity().getApplicationContext(),
                R.layout.device_name);
        scannedDevices.setAdapter(scannedDevicesAdapter);
    }

    @ItemClick(R.id.lv_paired_devices)
    void itemClickedPairedDevices(int index) {
        if (index != -1) {
            scannedDevices.setItemChecked(-1, true);
            selectedInfo = (BluetoothInfo) pairedDevices.getItemAtPosition(index);
        }
    }

    @ItemClick(R.id.lv_scanned_devices)
    void itemClickedScannedDevices(int index) {
        if (index != -1) {
            pairedDevices.setItemChecked(-1, true);
            selectedInfo = (BluetoothInfo) scannedDevices.getItemAtPosition(index);

        }
    }

    @Click(R.id.b_select)
    void selectClicked() {
        int checkedRadioButtonId = frontReadRadios.getCheckedRadioButtonId();
        DeviceChoserResultListener parent = (DeviceChoserResultListener) getActivity();
        if (checkedRadioButtonId == -1) {
            Toast toast = Toast.makeText(getActivity().getBaseContext(), toastTextSelectFrontRear, Toast.LENGTH_LONG);
            toast.show();
            return;
        }

        if (selectedInfo == null) {
            Toast toast = Toast.makeText(getActivity().getBaseContext(), toastTextSelectDevice, Toast.LENGTH_LONG);
            toast.show();
            return;

        }
        if (checkedRadioButtonId == R.id.rb_frontwheel) {
            parent.deviceChosen(selectedInfo, DeviceChoserResultListener.FRONT);
            this.dismiss();
        } else if (checkedRadioButtonId == R.id.rb_rearwheel) {
            parent.deviceChosen(selectedInfo, DeviceChoserResultListener.REAR);
            this.dismiss();
        }

    }

}
