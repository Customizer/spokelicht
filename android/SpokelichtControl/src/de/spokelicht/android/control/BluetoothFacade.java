package de.spokelicht.android.control;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by ypsilon on 12.02.15.
 * the bluetooth workers. Modeled as a retained Fragment.
 *
 * @see http://www.androiddesignpatterns.com/2013/04/retaining-objects-across-config-changes.html for details.
 */
public class BluetoothFacade extends Fragment {
    private Handler handler;
    private final Sender sender;

    private BluetoothDevice device;
    private volatile InputStream inputStream;
    private volatile OutputStream outputStream;
    private volatile BluetoothSocket socket;
    private volatile boolean connected;

    /**
     * connection process ongoing
     */
    static final int MESSAGE_CONNECTING = 1;
    /**
     * device is now connected
     */
    static final int MESSAGE_CONNECTED = 2;
    /**
     * connection could not be established
     */
    static final int MESSAGE_CONNECTION_FAILED = 3;
    /**
     * command could not be sent.
     */
    static final int MESSAGE_COMMAND_FAILED = 4;
    /**
     * connection was unexpectedly lost
     */
    static final int MESSAGE_CONNECTION_LOST = 5;
    /**
     * Connection was closed as requested
     */
    static final int MESSAGE_CONNECTION_CLOSED = 6;


    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    private int instanceId;
    private static int instanceCount = 0;


    public BluetoothFacade() {
        this.sender = new Sender();
        this.connected = false;
        instanceId = instanceCount++;
        Log.d("Debug:BluetoothFacade.BluetoothFacade#instance", "created: " + instanceId);
    }

    public int getInstanceId() {
        return instanceId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void brighter() {
        sender.execCommand(sender.CMD_BRIGHTER);
    }

    public void darker() {
        sender.execCommand(sender.CMD_DARKER);
    }

    public void nextImage() {
        sender.execCommand(sender.CMD_NEXT_IMAGE);
    }

    public void previousImage() {
        sender.execCommand(sender.CMD_PREVIOUS_IMAGE);
    }

    public void disconnect() {
        disconnect(MESSAGE_CONNECTION_CLOSED);
    }

    private void disconnect(int reason) {
        if (outputStream == null && inputStream == null && socket == null) {
            return;
        }
        try {
            outputStream.close();

        } catch (Exception ex) {
            // ignored
        } finally {
            outputStream = null;
        }
        try {
            inputStream.close();
        } catch (Exception ex) {
            // ignored
        } finally {
            inputStream = null;
        }
        try {
            socket.close();
        } catch (Exception ex) {
            // ignored
        } finally {
            socket = null;
        }
        try {
            synchronized (sender) {
                sender.notify();
            }
        } catch (Exception e) {
            Log.d("Debug:BluetoothFacade.disconnect", "exception while disconnecting", e);
        }
        inputStream = null;
        outputStream = null;
        socket = null;
        // todo: there should be some feedback from the bluetooth stack. Listen to that and toggle the status
        // accordingly

        connected = false;
        handler.obtainMessage(reason).sendToTarget();
    }

    public void connect(BluetoothDevice device) {
        try {
            if (connected && device == this.device) {
                return;
            }
            this.device = device;
            handler.obtainMessage(MESSAGE_CONNECTING).sendToTarget();
            Log.d("Debug:BluetoothFacade.connect", "create socket");
            socket = device.createRfcommSocketToServiceRecord(Constants.MY_UUID_INSECURE);
            Log.d("Debug:BluetoothFacade.connect", "socket created");
            new Thread() {
                @Override
                public void run() {
                    try {
                        Log.d("Debug:BluetoothFacade.run", "about to connect");
                        socket.connect();

                        Log.d("Debug:BluetoothFacade.run", "connected");
                        inputStream = socket.getInputStream();
                        outputStream = socket.getOutputStream();
                        new Thread(sender).start();
                        Log.d("Debug:BluetoothFacade.run", "inputstream obtained");
                        handler.obtainMessage(MESSAGE_CONNECTED).sendToTarget();
                        connected = true;
                        // todo: receive data...
                        byte inByte = 0;

                        while (inputStream != null) {

                            inByte = (byte) inputStream.read();
                        }
                    } catch (IOException e) {
                        Log.e("Debug:BluetoothFassade.run", "Connection fail", e);
                        if (connected) {
                            disconnect(MESSAGE_CONNECTION_LOST);
                        } else {
                            disconnect(MESSAGE_CONNECTION_FAILED);
                        }
                    }

                }
            }.start();
        } catch (IOException e) {
            Log.e("Debug:BluetoothFassade.run", "Connection fail", e);
            handler.obtainMessage(MESSAGE_CONNECTION_FAILED).sendToTarget();
            disconnect(MESSAGE_CONNECTION_FAILED);

        }
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    public void reconnect() {
        disconnect();
        connect(device);
    }

    private class Sender implements Runnable {
        private final byte[] CMD_BRIGHTER = new byte[]{'b', '$'};
        private final byte[] CMD_DARKER = new byte[]{'d', '$'};
        // not supported by the firmware yet.
        //private final byte[] CMD_DARKER = new byte[]{'d', '$'};
        private final byte[] CMD_NEXT_IMAGE = new byte[]{'n', '$'};
        private final byte[] CMD_PREVIOUS_IMAGE = new byte[]{'p', '$'};

        private byte[] toSend;

        @Override
        public void run() {

            synchronized (this) {
                while (outputStream != null) {
                    if (toSend != null) {
                        try {
                            Log.d("Debug:Sender.run", "sending " + toSend.length + " bytes");
                            outputStream.write(toSend);
                            toSend = null;
                        } catch (IOException e) {

                            disconnect(MESSAGE_COMMAND_FAILED);

                        }
                    } else {
                        try {
                            wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            // ignored.
                        }
                    }
                }
            }
        }

        void execCommand(byte[] command) {
            synchronized (this) {
                toSend = command;
                notify();
            }
        }

    }
}
