package de.spokelicht.android.control;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import org.androidannotations.annotations.EReceiver;
import org.androidannotations.annotations.ReceiverAction;

/**
 * Created by ypsilon on 11.02.15.
 */
@EReceiver
class BluetoothBroadcastReceiver extends BroadcastReceiver {
    BroadcastListener listener;

    public void setListener(BroadcastListener listener) {
        this.listener = listener;
    }

    @ReceiverAction(BluetoothDevice.ACTION_FOUND)
    void deviceFound(Intent intent) {
        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        // If it's already paired, skip it, because it's been listed already
        if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
            listener.deviceDiscovered(device.getName() + "\n" + device.getAddress());
        }
        Log.d("Debug:BluetoothBroadcastReceiver.deviceFound",intent.getAction());
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("Debug:onReceive",intent.getAction());
    }

    interface BroadcastListener {
        void deviceDiscovered(String deviceName);
    }

}
