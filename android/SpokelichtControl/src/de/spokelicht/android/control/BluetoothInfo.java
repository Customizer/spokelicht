package de.spokelicht.android.control;

import android.bluetooth.BluetoothDevice;

/**
 * Created by ypsilon on 12.02.15.
 */
public class BluetoothInfo {
    private final BluetoothDevice device;

    public BluetoothInfo(BluetoothDevice device) {
        this.device = device;
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    @Override
    public String toString() {
        return device.getName() + "\n" + device.getAddress();
    }

}
