package de.spokelicht.android.control;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.*;
import org.androidannotations.annotations.*;
import org.androidannotations.annotations.res.DrawableRes;
import org.androidannotations.annotations.res.StringRes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ypsilon on 10.02.15.
 */
@EFragment(R.layout.control)
public class ControlFragment extends Fragment implements Handler.Callback {
    private static final boolean ENABLED = true;
    private static final boolean DISABLED = false;
    @ViewById(R.id.tv_control_title)
    TextView titleView;
    @ViewById(R.id.b_control_previous)
    Button previousButton;
    @ViewById(R.id.b_control_next)
    Button nextButton;
    @ViewById(R.id.b_darker)
    Button darkerButton;
    @ViewById(R.id.b_brighter)
    Button brighterButton;
    //todo: the name sucks
    @ViewById(R.id.bt_connection_button)
    Button connectionButton;

    /**
     * the bluetooth interface
     */
    private BluetoothFacade bluetoothFacade;
    /**
     * a handler through which the communication related threads can communicate with this view.
     */
    private final Handler handler = new Handler(this);


    @StringRes(R.string.nt_connected)
    String toastTextConnected;
    @StringRes(R.string.nt_front_wheel)
    String toastTextFrontWheel;
    @StringRes(R.string.nt_rear_wheel)
    String toastTextRearWheel;
    @StringRes(R.string.nt_connection_failed)
    String messageConnectionFailed;
    /**
     * Title text for front wheel control
     */
    @StringRes(R.string.tit_front_wheel)
    String titleFrontWheel;
    /**
     * Title text for rear wheel control
     */
    @StringRes(R.string.tit_rear_wheel)
    String titleRearWheel;


    @StringRes(R.string.bt_connect)
    String buttonTextConnect;
    @StringRes(R.string.bt_disconnect)
    String buttonTextDisconnect;
    @StringRes(R.string.bt_select_device)
    String buttonTextSelectDevice;
    @InstanceState
    Boolean frontWheel;

    @DrawableRes(R.drawable.bt_active)
    Drawable iconConnected;


    @DrawableRes(R.drawable.bt_inactive)
    Drawable iconDisconnected;


    private List<View> enablableViews;

    private int instanceId;
    private static int instanceCount = 0;

    private static final String FACADE_FRONT_WHEEL = "__facade_front_wheel";
    private static final String FACADE_REAR_WHEEL = "__facade_rear_wheel";

    public void setBluetoothFacade(BluetoothFacade bluetoothFacade) {
        this.bluetoothFacade = bluetoothFacade;
    }

    public void setFrontWheel(boolean frontWheel) {
        Log.d("Debug:ControlFragment.setFrontWheel", "" + instanceId);
        this.frontWheel = frontWheel;
    }


    public ControlFragment() {
        instanceId = instanceCount++;
        Log.d("Instance", "ControlFragment created " + instanceId);
        if (instanceId > 3) {
            new RuntimeException().printStackTrace();
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("Debug:ControlFragment.onCreate", "" + instanceId + " " + frontWheel);
        FragmentManager fragmentManager = getFragmentManager();
        if (frontWheel == null) {
            Log.d("Debug:ControlFragment.onCreate", "frontWheel is null " + instanceId);
        }
        if (frontWheel == null) return;

        String tag = frontWheel ? FACADE_FRONT_WHEEL : FACADE_REAR_WHEEL;
        BluetoothFacade bluetoothFacade = (BluetoothFacade) fragmentManager.findFragmentByTag(tag);
        if (bluetoothFacade == null) {
            bluetoothFacade = new BluetoothFacade();
            fragmentManager.beginTransaction().add(bluetoothFacade, tag).commit();
        }
        this.bluetoothFacade = bluetoothFacade;
        bluetoothFacade.setHandler(handler);
        Log.d("Debug:ControlFragment.onCreate","bluetoothFacade used " + bluetoothFacade.getInstanceId());

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d("Debug:ControlFragment.onAttach", "" + instanceId + " " + frontWheel);
    }

    @Click(R.id.b_control_next)
    void nextClicked() {
        bluetoothFacade.nextImage();
    }

    @Click(R.id.b_control_previous)
    void previousClicked() {
        bluetoothFacade.previousImage();
    }

    @Click(R.id.b_brighter)
    void brighterClicked() {
        bluetoothFacade.brighter();
    }

    @Click(R.id.b_darker)
    void darkerClicked() {
        bluetoothFacade.darker();
    }

    @Click(R.id.bt_connection_button)
    void connectDisconnectClicked() {
        if (bluetoothFacade.isConnected()) {
            bluetoothFacade.disconnect();
        } else {
            bluetoothFacade.reconnect();
        }
    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.d("Instance", "ControlFragment#onViewStateRestored " + instanceId);

        titleView.setText(frontWheel ? titleFrontWheel : titleRearWheel);
        enablableViews = new ArrayList<>();
        enablableViews.add(previousButton);
        enablableViews.add(nextButton);
        enablableViews.add(darkerButton);
        enablableViews.add(brighterButton);
        if (bluetoothFacade.isConnected()) {
            setControlsState(ENABLED);
           // this.connectionButton.setEnabled(true);
            connected();
        } else {
            setControlsState(DISABLED);
            //this.connectionButton.setEnabled(false);

            if (bluetoothFacade.getDevice() != null) {
                disconnected();
            }
        }

    }

    public void connect(BluetoothInfo btInfo) {
        bluetoothFacade.connect(btInfo.getDevice());

    }

    @Override
    public boolean handleMessage(Message msg) {
        if (bluetoothFacade.isConnected()) {
            connected();
        } else {
            disconnected();

        }
        switch (msg.what) {

            case BluetoothFacade.MESSAGE_CONNECTION_FAILED:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.nt_connection_failed)
                        .setPositiveButton(R.string.bt_ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                bluetoothFacade.reconnect();
                            }
                        })
                        .setNegativeButton(R.string.bt_cancel, null);
                // Create the AlertDialog object and return it
                builder.create().show();
                disconnected();

                break;
            case BluetoothFacade.MESSAGE_CONNECTED:
                String txt = String.format(toastTextConnected, bluetoothFacade.getDevice().getName(),
                        frontWheel ? toastTextFrontWheel : toastTextRearWheel);
                Toast toast = Toast.makeText(getActivity().getBaseContext(), txt, Toast.LENGTH_LONG);
                toast.show();
                connected();
                break;
        }
        return true;
    }

    /**
     * Brings this Fragment in the diconnected mode. This disables all controls which require bluetooth and
     * updates any other controls.
     */
    private void disconnected() {
        connectionButton.setText(String.format(buttonTextConnect, bluetoothFacade.getDevice().getName()));
        connectionButton.setCompoundDrawablesWithIntrinsicBounds(null, null, iconDisconnected, null);
        setControlsState(DISABLED);
    }

    /**
     * Brings this Fragment in the connected mode. This enables all controls which require bluetooth and
     * updates any other controls.
     */
    private void connected() {
        connectionButton.setText(String.format(buttonTextDisconnect, bluetoothFacade.getDevice().getName()));
        connectionButton.setCompoundDrawablesWithIntrinsicBounds(null, null, iconConnected, null);

        setControlsState(ENABLED);
    }

    /**
     * sets the enabled/disabled state of all controls that work only if a device is connected.
     *
     * @param enabled the desired state.
     */
    void setControlsState(boolean enabled) {
        for (View enablableView : enablableViews) {
            enablableView.setEnabled(enabled);
        }
    }
}

