package de.localtoast.spokelicht.gui;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.util.Callback;

public class FrameCellCallback implements
				Callback<ListView<WritableImage>, ListCell<WritableImage>> {
	@Override
	public ListCell<WritableImage> call(ListView<WritableImage> list) {
		return new ListCell<WritableImage>() {
			@Override
			protected void updateItem(WritableImage item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setText(null);
					setGraphic(null);
				} else {
					ImageView view = new ImageView();
					view.setFitHeight(68);
					view.setFitWidth(68);
					if (item != null) {
						view.setImage(item);
						setGraphic(view);
					}
				}
			}
		};
	}
}
