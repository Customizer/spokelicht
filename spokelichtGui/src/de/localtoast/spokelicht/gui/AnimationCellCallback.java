package de.localtoast.spokelicht.gui;

import de.localtoast.spokelicht.animations.AnimationSetController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.util.Callback;

public class AnimationCellCallback implements
				Callback<ListView<AnimationViewItem>, ListCell<AnimationViewItem>> {

	private AnimationSetController controller;
	private ListView<AnimationViewItem> animationsListView;
	private AnimationSetFileHandler fileHandler;

	public AnimationCellCallback(AnimationSetController controller,
															 ListView<AnimationViewItem> animationsListView,
															 AnimationSetFileHandler fileHandler) {
		this.controller = controller;
		this.animationsListView = animationsListView;
		this.fileHandler = fileHandler;
	}

	@Override
	public ListCell<AnimationViewItem> call(ListView<AnimationViewItem> list) {
		return new ListCell<AnimationViewItem>() {
			@Override
			protected void updateItem(AnimationViewItem item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setText(null);
					setGraphic(null);
				} else {
					ImageView view = new ImageView();
					view.setFitHeight(68);
					view.setFitWidth(68);
					ObservableList<WritableImage> frameImages = FXCollections.observableArrayList
									(controller.getOriginalFrames(item.getAnimationIndex()));
					if (frameImages.isEmpty()) {
						// Display a button to add animations
						Button addButton = new Button();

						addButton.getStylesheets().add(
										"file://"
														+ getClass().getResource(
														"buttons.css").getFile());
						addButton.setId("add");
						addButton.setOnAction(event -> addAnimation());

						addButton.setTooltip(new Tooltip(
										"Add new animation or picture"));

						setAlignment(Pos.CENTER);
						setGraphic(addButton);
					} else {
						view.setImage(frameImages.get(0));
						setGraphic(view);
					}
				}
			}
		};
	}

	private void addAnimation() {
		fileHandler.addAnimation(animationsListView.getItems());
	}
}