package de.localtoast.spokelicht.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RotateImageHandler implements EventHandler<ActionEvent> {

	private final int degrees;
	private final MainController mainController;

	public RotateImageHandler(MainController mainController, int degrees) {
		this.mainController = mainController;
		this.degrees = degrees;
	}

	@Override
	public void handle(ActionEvent event) {
		mainController.addRotation(degrees);
		mainController.repaintCanvases();
	}
}
