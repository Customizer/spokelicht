package de.localtoast.spokelicht.gui;

import de.localtoast.spokelicht.LinesToImageConverter;
import de.localtoast.spokelicht.animations.Animation;
import de.localtoast.spokelicht.animations.AnimationSet;
import de.localtoast.spokelicht.animations.AnimationSetController;
import de.localtoast.spokelicht.animations.FrameCache;
import de.localtoast.spokelicht.gui.settings.Preferences;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class AnimationsViewController {
	@FXML
	private ListView<AnimationViewItem> animationsListView;
	private Stage primaryStage;
	private Preferences preferences;
	private MainController mainController;

	public void init(Stage primaryStage, Canvas canvasOriginal, Canvas canvasConverted,
									 FramesViewController framesViewController, Preferences preferences,
									 MainController mainController) {
		this.primaryStage = primaryStage;
		this.preferences = preferences;
		this.mainController = mainController;
		animationsListView.setCellFactory(new AnimationCellCallback(mainController
						.getAnimationSetController(), animationsListView, mainController
						.getAnimationSetFileHandler()));
		animationsListView.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		animationsListView.getSelectionModel().selectedItemProperty()
						.addListener(new ChangeListener<AnimationViewItem>() {
							@Override
							public void changed(
											ObservableValue<? extends AnimationViewItem> observable,
											AnimationViewItem oldValue, AnimationViewItem newValue) {

								AnimationSetController controller =
												mainController.getAnimationSetController();

								if (newValue != null && controller != null) {
									int index = animationsListView.getSelectionModel().getSelectedIndex();


									FrameCache cache = FrameCache.INSTANCE;
									framesViewController.setItems(FXCollections.observableArrayList
													(controller.getOriginalFrames(index)));
									if (controller.getOriginalFrames(newValue.getAnimationIndex()).size() == 0) {
										GraphicsContext contextOriginal = canvasOriginal
														.getGraphicsContext2D();
										contextOriginal.setFill(Color.BLACK);
										contextOriginal.fillRect(0, 0,
														canvasOriginal.getWidth(),
														canvasOriginal.getHeight());
										GraphicsContext contextConverted = canvasConverted
														.getGraphicsContext2D();
										contextConverted.setFill(Color.BLACK);
										contextConverted.fillRect(0, 0,
														LinesToImageConverter.TARGET_WIDTH_AND_HEIGHT, LinesToImageConverter
																		.TARGET_WIDTH_AND_HEIGHT);
									} else {
										mainController.resetZoomLevelOriginalCanvas(controller.getOriginalFrames
														(newValue.getAnimationIndex()).get(0));
										framesViewController.selectFirst();
									}
								} else {
									canvasOriginal.setWidth(150);
									canvasOriginal.setHeight(150);
									canvasOriginal.setScaleX(1);
									canvasOriginal.setScaleY(1);
								}
							}
						});

		ObservableList<AnimationViewItem> initialItem = FXCollections
						.observableArrayList();
		initialItem.add(new AnimationViewItem());
		animationsListView.setItems(initialItem);
		animationsListView.getSelectionModel().selectFirst();

		animationsListView.setOnKeyPressed(event -> {
			if (event.getCode() == KeyCode.DELETE) {
				AnimationViewItem item = animationsListView.getSelectionModel()
								.getSelectedItem();
				if (mainController.getAnimationSetController().getOriginalFrames(item
								.getAnimationIndex()).size() > 0) {
					int index = animationsListView.getSelectionModel().getSelectedIndex();
					mainController.getAnimationSetController().removeAnimation(index);
					animationsListView.getItems().remove(index);
				}
			}
		});
	}

	public void clearItems() {
		animationsListView.getItems().clear();
		animationsListView.getItems().add(new AnimationViewItem());
	}

	public ObservableList<AnimationViewItem> getItems() {
		return animationsListView.getItems();
	}

	public void setAnimationSet(AnimationSet animationSet) {
		ObservableList<AnimationViewItem> animationViewItems = FXCollections
						.observableArrayList();
		for (int i = 0; i < animationSet.getAnimations().size(); i++) {
			Animation settingsItem = animationSet.getAnimations().get(i);
			animationViewItems.add(new AnimationViewItem(i));
		}
		setItems(animationViewItems);
	}

	private void setItems(ObservableList<AnimationViewItem> items) {
		items.add(new AnimationViewItem());
		animationsListView.setItems(items);
		animationsListView.getSelectionModel().selectFirst();
	}

	public int getSelectedAnimationIndex() {
		return animationsListView.getSelectionModel().getSelectedItem()
						.getAnimationIndex();
	}
}
