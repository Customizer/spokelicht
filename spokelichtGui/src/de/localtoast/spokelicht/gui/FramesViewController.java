package de.localtoast.spokelicht.gui;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.control.SelectionMode;
import javafx.scene.image.WritableImage;
import javafx.util.Duration;

public class FramesViewController {
	@FXML
	private ListView<WritableImage> frames;

	@FXML
	private Button play;
	@FXML
	private Button pause;


	private Timeline animationTimeline = new Timeline(new KeyFrame(
					Duration.millis(100),
					ae -> {
						jumpToNextFrame();
					}));


	public void init(MainController mainController) {
		frames.setOrientation(Orientation.HORIZONTAL);
		frames.setCellFactory(new FrameCellCallback());
		frames.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		frames.getSelectionModel().selectedItemProperty()
						.addListener(new ChangeListener<WritableImage>() {
							@Override
							public void changed(
											ObservableValue<? extends WritableImage> observable,
											WritableImage oldValue, WritableImage newValue) {

								if (newValue != null) {
									mainController.repaintCanvases();
								}
							}
						});

		animationTimeline.setCycleCount(Animation.INDEFINITE);
		play.setOnAction(event -> {
			mainController.setAnimationPlaying(true);
			animationTimeline.play();
		});

		pause.setOnAction(event -> {
			animationTimeline.stop();
			mainController.setAnimationPlaying(false);
			// Repaint once, with animationPlaying flag turned off, to get the "real" image
			mainController.repaintCanvases();
		});
	}

	public void setItems(ObservableList<WritableImage> items) {
		frames.setItems(items);
	}

	public void selectFirst() {
		frames.getSelectionModel().selectFirst();
	}

	public void clearItems() {
		frames.getItems().clear();
	}

	public WritableImage getSelectedItem() {
		return frames.getSelectionModel().getSelectedItem();
	}

	public int getSelectedIndex() {
		return frames.getSelectionModel().getSelectedIndex();
	}

	private void jumpToNextFrame() {
		MultipleSelectionModel<WritableImage> selectionModel = frames.getSelectionModel();
		int index = selectionModel.getSelectedIndex();
		if (index < frames.getItems().size() - 1) {
			index++;
		} else {
			index = 0;
		}
		selectionModel.clearSelection();
		selectionModel.select(index);
		frames.scrollTo(index);
	}
}
