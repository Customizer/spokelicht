package de.localtoast.spokelicht.gui;

import de.localtoast.spokelicht.LineCreator;
import de.localtoast.spokelicht.LinesToImageConverter;
import de.localtoast.spokelicht.animations.Animation;
import de.localtoast.spokelicht.animations.AnimationSetController;
import de.localtoast.spokelicht.animations.Frame;
import de.localtoast.spokelicht.gui.settings.Preferences;
import de.localtoast.spokelicht.utils.Line;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.*;

public class MainController {

	@FXML
	private Button newAnimationSet;
	@FXML
	private Button open;
	@FXML
	private Button save;
	@FXML
	private Button saveAs;
	@FXML
	private ToggleButton move;
	@FXML
	private Button rotate90;
	@FXML
	private Button rotate270;
	@FXML
	private ToggleButton scale;
	@FXML
	private Button sendDataArduino;
	@FXML
	private Button sendDataSmartphone;
	@FXML
	private ScrollPane scrollPaneOriginal;
	@FXML
	private ScrollPane scrollPaneConverted;
	@FXML
	private Canvas canvasOriginal;
	@FXML
	private Canvas canvasConverted;
	@FXML
	private Menu menuOpenRecent;
	@FXML
	private MenuItem menuSaveAnimationSet;
	@FXML
	private MenuItem menuSaveAnimationSetAs;
	@FXML
	private MenuItem menuNewAnimationSet;
	@FXML
	private FramesViewController framesViewController;
	@FXML
	private AnimationsViewController animationsViewController;
	@FXML
	private PaletteViewController paletteViewController;
	@FXML
	private ProgressBar progressBar;
	@FXML
	private MenuBar menuBar;
	@FXML
	private ToolBar toolBar;
	@FXML
	private GridPane mainPane;

	private Stage primaryStage;
	private ImageTransformationHandler imageTransformationHandlerConverted;
	private ImageTransformationHandler imageTransformationHandlerOriginal;
	private Preferences preferences;
	private AnimationSetFileHandler animationSetFileHandler;
	private ZoomEventHandler<KeyEvent> zoomHandlerOriginal;
	private boolean transformationInProgress = false;
	private boolean animationPlaying;
	private AnimationSetController animationSetController;

	public void setPrimaryStage(Stage primaryStage) {
		this.primaryStage = primaryStage;
	}

	public void init() throws Exception {
		animationSetController = new AnimationSetController();
		preferences = new Preferences(this, menuOpenRecent);
		initFileHandlingButtons();
		initTools();
		framesViewController.init(this);
		paletteViewController.init(this);
		animationSetFileHandler = new AnimationSetFileHandler(primaryStage,
						animationsViewController, menuSaveAnimationSet, preferences, animationSetController);
		animationsViewController.init(primaryStage, canvasOriginal, canvasConverted,
						framesViewController, preferences, this);
		initMenu();
		setNoAnimationSetLoaded(true);
	}

	private void initFileHandlingButtons() {
		newAnimationSet.setOnAction(event -> newAnimationSet());
		open.setOnAction(event -> loadAnimationSet());
		save.setOnAction(event -> saveAnimationSet());
		saveAs.setOnAction(event -> saveAnimationSetAs());
	}

	private void initMenu() {
		menuNewAnimationSet.setOnAction(event -> newAnimationSet());
	}

	private void newAnimationSet() {
		animationSetFileHandler.createNewAnimationSet();
		animationsViewController.clearItems();
		framesViewController.clearItems();
		fillCanvasBlack(canvasOriginal);
		GraphicsContext contextConverted = canvasConverted
						.getGraphicsContext2D();
		contextConverted.setFill(Color.BLACK);
		contextConverted.fillRect(0, 0,
						LinesToImageConverter.TARGET_WIDTH_AND_HEIGHT,
						LinesToImageConverter.TARGET_WIDTH_AND_HEIGHT);
		setNoAnimationSetLoaded(false);
	}

	private void initTools() {
		initSendDataTool();
		initRotationTool();
		initMoveTool();
		initZoomTool();
	}

	private void initZoomTool() {
		zoomHandlerOriginal = new ZoomEventHandler<>(canvasOriginal, 2);
		scrollPaneOriginal.addEventFilter(KeyEvent.KEY_PRESSED,
						zoomHandlerOriginal);
		ZoomEventHandler<KeyEvent> zoomHandlerConverted = new ZoomEventHandler<>(
						canvasConverted, 0.375);
		scrollPaneConverted.addEventFilter(KeyEvent.KEY_PRESSED,
						zoomHandlerConverted);
	}

	private void initSendDataTool() {
		sendDataArduino.setOnAction(new DataSendHandler(animationsViewController, this));
	}

	private void initRotationTool() {
		rotate90.setOnAction(new RotateImageHandler(this, 90));
		rotate270.setOnAction(new RotateImageHandler(this, -90));
	}

	private void initMoveTool() {
		imageTransformationHandlerConverted = new ImageTransformationHandler(
						this, canvasConverted, scrollPaneConverted);
		imageTransformationHandlerOriginal = new ImageTransformationHandler(
						this, canvasOriginal, scrollPaneOriginal);

		move.setOnAction(event -> {
			setTool(Tool.MOVE);
		});

		scale.setOnAction(event -> {
			setTool(Tool.SCALE);
		});
	}

	private void disableAllToggleButtons() {
		move.setSelected(false);
		scale.setSelected(false);
	}

	private void setTool(Tool tool) {
		disableAllToggleButtons();

		if (framesViewController.getSelectedItem() != null
						) {
			switch (tool) {
				case MOVE:
					move.setSelected(true);
					break;
				case SCALE:
					scale.setSelected(true);
					break;
				default:
					break;
			}

			imageTransformationHandlerOriginal.setTool(tool);
			imageTransformationHandlerConverted.setTool(tool);
		} else {
			imageTransformationHandlerOriginal.setTool(Tool.NONE);
			imageTransformationHandlerConverted.setTool(Tool.NONE);
		}
	}

	public void repaintCanvases() {
		int animationIndex = animationsViewController.getSelectedAnimationIndex();
		int frameIndex = framesViewController.getSelectedIndex();

		// get updated frame from controller
		WritableImage manipulatedFrame = animationSetController.getManipulatedFrame(animationIndex,
						frameIndex);

		canvasOriginal.setWidth(manipulatedFrame.getWidth());
		canvasOriginal.setHeight(manipulatedFrame.getHeight());
		fillCanvasBlack(canvasOriginal);
		canvasOriginal.getGraphicsContext2D().drawImage(manipulatedFrame, 0, 0);
		List<Line> lines = new LineCreator(manipulatedFrame)
						.createLines();

		// TODO no canvas in linestoimageconverter! at least none, which is passed from here!
		new LinesToImageConverter().convert(lines, canvasConverted, LineCreator.LEDS);

		updateColorPalette(manipulatedFrame);
		WritableImage sourceImage = animationSetController.getOriginalFrames(animationIndex).get
						(frameIndex);

		imageTransformationHandlerConverted.setCorrectionFactor(sourceImage.getWidth() /
						canvasConverted.getWidth());
	}

	private void updateColorPalette(Image image) {
		if (!(transformationInProgress || animationPlaying)) {
			Set<Color> colors = new HashSet<>();
			PixelReader reader = image.getPixelReader();
			for (int x = 0; x < image.getWidth(); x++) {
				for (int y = 0; y < image.getHeight(); y++) {
					Color color = reader.getColor(x, y);
					colors.add(color);
				}
			}
			paletteViewController.setColors(new ArrayList<>(colors));
		}
	}

	private void fillCanvasBlack(Canvas canvas) {
		GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
		graphicsContext.setFill(Color.BLACK);
		graphicsContext.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
	}

	void addXMove(int xMove) {
		Animation settings = getSelectedAnimationSettings();
		settings.setxMove(settings.getxMove() + xMove);
	}

	void addYMove(int yMove) {
		Animation settings = getSelectedAnimationSettings();
		settings.setyMove(settings.getyMove() + yMove);
	}

	void addXScale(double xScale) {
		Animation settings = getSelectedAnimationSettings();
		settings.setxScale(settings.getxScale() + xScale);
	}

	void addYScale(double yScale) {
		Animation settings = getSelectedAnimationSettings();
		settings.setyScale(settings.getyScale() + yScale);
	}

	double getXMove() {
		return getSelectedAnimationSettings().getxMove();
	}

	double getYMove() {
		return getSelectedAnimationSettings().getyMove();
	}

	void addRotation(int degrees) {
		if (framesViewController.getSelectedItem() != null) {
			Animation settings = getSelectedAnimationSettings();
			settings.setRotationInDegrees(settings.getRotationInDegrees() + degrees);
		}
	}

	private Animation getSelectedAnimationSettings() {
		return animationSetFileHandler.getAnimation(animationsViewController.getSelectedAnimationIndex
						());
	}

	@FXML
	public void loadAnimationSet() {
		animationSetFileHandler.loadAnimationSet();
		setNoAnimationSetLoaded(false);
	}

	@FXML
	private void saveAnimationSet() {
		animationSetFileHandler.saveAnimationSet();
	}

	@FXML
	private void saveAnimationSetAs() {
		animationSetFileHandler.saveAnimationSetAs();
	}

	public void loadAnimationSet(String filePath) {
		animationSetFileHandler.loadAnimationSet(filePath);
		setNoAnimationSetLoaded(false);
	}

	public void resetZoomLevelOriginalCanvas(Image image) {
		zoomHandlerOriginal.setZoomLevel(300 / image.getWidth());
	}

	public void setTransformationInProgress(boolean transformationInProgress) {
		this.transformationInProgress = transformationInProgress;
	}

	public void setAnimationPlaying(boolean animationPlaying) {
		this.animationPlaying = animationPlaying;
	}


	public void replaceColor(Color oldColor, Color newColor) {
		Map<Integer, Frame> frameSettingsMap = getSelectedAnimationSettings()
						.getFrameSettings();
		Frame frameSettings = frameSettingsMap.get(framesViewController.getSelectedIndex());
		if (frameSettings == null) {
			frameSettings = new Frame();
			frameSettingsMap.put(framesViewController.getSelectedIndex(), frameSettings);
		}

		frameSettings.setReplacementColor(oldColor, newColor);

		repaintCanvases();
	}

	/**
	 * Disables or enables most of the GUI elements at once.
	 *
	 * @param enabled
	 */
	public void setEnabled(boolean enabled) {
		menuBar.setDisable(!enabled);
		toolBar.setDisable(!enabled);
		mainPane.setDisable(!enabled);
	}

	public void setNoAnimationSetLoaded(boolean value) {
		mainPane.setDisable(value);
		menuSaveAnimationSet.setDisable(value);
		menuSaveAnimationSetAs.setDisable(value);
		save.setDisable(value);
		saveAs.setDisable(value);
		sendDataArduino.setDisable(value);
		sendDataSmartphone.setDisable(value);
		rotate270.setDisable(value);
		rotate90.setDisable(value);
		move.setDisable(value);
		scale.setDisable(value);
	}

	public ProgressBar getProgressBar() {
		return progressBar;
	}

	public AnimationSetFileHandler getAnimationSetFileHandler() {
		return animationSetFileHandler;
	}

	public AnimationSetController getAnimationSetController() {
		return animationSetController;
	}

	public enum Tool {
		NONE, MOVE, SCALE
	}

}
