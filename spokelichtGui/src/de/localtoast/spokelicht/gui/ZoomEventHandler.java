package de.localtoast.spokelicht.gui;

import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class ZoomEventHandler<Event extends KeyEvent> implements
				EventHandler<Event> {

	// TODO minimum and maximum zoom level
	protected Canvas canvas;
	protected double zoomLevel;

	public ZoomEventHandler(Canvas canvas, double initialZoomLevel) {
		this.zoomLevel = initialZoomLevel;
		this.canvas = canvas;
		zoom();
	}

	@Override
	public void handle(KeyEvent event) {
		double zoomIncrement = 1.1;
		if (KeyCode.PLUS.equals(event.getCode())
						|| KeyCode.ADD.equals(event.getCode())) {
			zoomLevel = zoomLevel * zoomIncrement;
		} else if (KeyCode.MINUS.equals(event.getCode())
						|| KeyCode.SUBTRACT.equals(event.getCode())) {
			zoomLevel = zoomLevel / zoomIncrement;
		}
		zoom();
	}

	public void zoom() {
		canvas.setScaleX(zoomLevel);
		canvas.setScaleY(zoomLevel);

		/*
		 * Setting the scale of the canvas does not move the center of the
		 * canvas. This looks in several situations pretty odd. The following
		 * lines compensate this effect.
		 */
		double offset = 0;
		offset = (canvas.getWidth() - canvas.getWidth() * zoomLevel) / 2;
		canvas.setTranslateX(-offset);
		canvas.setTranslateY(-offset);
	}

	public double getZoomLevel() {
		return zoomLevel;
	}

	public void setZoomLevel(double zoomLevel) {
		this.zoomLevel = zoomLevel;
		zoom();
	}
}
