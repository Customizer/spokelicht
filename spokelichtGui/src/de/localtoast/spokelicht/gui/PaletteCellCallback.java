package de.localtoast.spokelicht.gui;

import javafx.scene.control.ColorPicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import javafx.util.Callback;

public class PaletteCellCallback implements Callback<ListView<Color>, ListCell<Color>> {
	private PaletteViewController paletteViewController;

	public PaletteCellCallback(PaletteViewController paletteViewController) {
		this.paletteViewController = paletteViewController;
	}

	@Override
	public ListCell<Color> call(ListView<Color> param) {
		return new ListCell<Color>() {
			@Override
			protected void updateItem(Color color, boolean empty) {
				super.updateItem(color, empty);
				if (empty || color == null) {
					setText(null);
					setGraphic(null);
				} else {
					ColorPicker colorPicker = new ColorPicker(color);
					colorPicker.setOnAction(event -> {
						paletteViewController.replaceColor(color, colorPicker.getValue());
					});
					colorPicker.getStyleClass().add("button");
					colorPicker.setStyle("-fx-color-label-visible: false");
					setGraphic(colorPicker);
				}
			}
		};
	}
}
