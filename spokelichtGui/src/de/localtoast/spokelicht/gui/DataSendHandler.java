package de.localtoast.spokelicht.gui;

import de.localtoast.spokelicht.arduinoconnection.DataSender;
import de.localtoast.spokelicht.arduinoconnection.Memory;
import de.localtoast.spokelicht.arduinoconnection.ProgressListener;
import de.localtoast.spokelicht.arduinoconnection.SerialConnectionException;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ProgressBar;

public class DataSendHandler implements EventHandler {

	private AnimationsViewController animationsViewController;
	private MainController mainController;

	public DataSendHandler(AnimationsViewController animationsViewController,
												 MainController mainController) {
		this.animationsViewController = animationsViewController;
		this.mainController = mainController;
	}

	@Override
	public void handle(Event event) {
		mainController.setEnabled(false);
		Memory memory = mainController.getAnimationSetController().getMemoryObject();

		if (memory.isEmpty()) {
			mainController.setEnabled(true);
			// TODO print some kind of error message somewhere in the GUI
		} else {

			Task task = new Task<Void>() {
				@Override
				public Void call() {
					DataSender sender = new DataSender();
					sender.setProgressListener(new ProgressListener() {
						@Override
						public void update(int workDone, int max) {
							updateProgress(workDone, max);
						}
					});
					try {
						sender.updateArduinoMemory(memory);
					} catch (SerialConnectionException e) {
						// TODO add proper error handling
						e.printStackTrace();
					}
					return null;
				}
			};
			ProgressBar bar = mainController.getProgressBar();
			bar.progressProperty().bind(task.progressProperty());
			new Thread(task).start();
			task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
				@Override
				public void handle(WorkerStateEvent event) {
					resetGui(bar);
				}
			});
		}
	}

	private void resetGui(ProgressBar bar) {
		mainController.setEnabled(true);
		bar.progressProperty().unbind();
		bar.progressProperty().set(0);
	}
}
