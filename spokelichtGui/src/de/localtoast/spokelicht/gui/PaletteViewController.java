package de.localtoast.spokelicht.gui;

import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.paint.Color;

import java.util.List;

public class PaletteViewController {
	@FXML
	private ListView<Color> colors;
	private MainController mainController;

	public void init(MainController mainController) {
		this.mainController = mainController;
		colors.setCellFactory(new PaletteCellCallback(this));
	}

	public void setColors(List<Color> colors) {
		this.colors.getItems().clear();
		this.colors.getItems().addAll(colors);
	}

	public void replaceColor(Color oldColor, Color newColor) {
		mainController.replaceColor(oldColor, newColor);
	}
}
