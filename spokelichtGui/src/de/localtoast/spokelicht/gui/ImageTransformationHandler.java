package de.localtoast.spokelicht.gui;

import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;

public class ImageTransformationHandler {
	private final MainController mainController;
	private double correctionFactor = 1;
	private int dragDeltaX = 0;
	private int dragDeltaY = 0;
	private int lastXOffset = 0;
	private int lastYOffset = 0;
	private DragStart dragStart = null;
	private MainController.Tool selectedTool = MainController.Tool.NONE;

	public ImageTransformationHandler(MainController mainController, Canvas canvas, ScrollPane
					scrollPane) {
		this.mainController = mainController;

		canvas.setOnMousePressed(event -> {
			switch (selectedTool) {
				case MOVE:
					mainController.setTransformationInProgress(true);
					dragDeltaX = (int) event.getX();
					dragDeltaY = (int) event.getY();
					break;
				case SCALE:
					mainController.setTransformationInProgress(true);
					dragDeltaX = (int) event.getX();
					dragDeltaY = (int) event.getY();

					int x = (int) event.getX();
					int y = (int) event.getY();
					int width = (int) canvas.getWidth();
					int height = (int) canvas.getHeight();

					if (x < width / 2 && y < height / 2) {
						dragStart = DragStart.NW;
					} else if (x > width / 2 && y < height / 2) {
						dragStart = DragStart.NE;
					} else if (x < width / 2 && y > height / 2) {
						dragStart = DragStart.SW;
					} else if (x > width / 2 && y > height / 2) {
						dragStart = DragStart.SE;
					} else {
						dragStart = null;
					}
					break;
				default:
					mainController.setTransformationInProgress(false);
					dragDeltaX = 0;
					dragDeltaY = 0;
					break;
			}
		});

		canvas.setOnMouseDragged(event -> {

			int xOffset = calcXOffset(event);
			int yOffset = calcYOffset(event);
			switch (selectedTool) {
				case MOVE:
					mainController.addXMove(xOffset - lastXOffset);
					mainController.addYMove(yOffset - lastYOffset);
					mainController.repaintCanvases();
					break;
				case SCALE:
					double scalingCorrection = canvas.getWidth() * correctionFactor;
					switch (dragStart) {
						case NW:
							mainController.addXScale(-calcXScale(xOffset, scalingCorrection));
							mainController.addYScale(-calcYScale(yOffset, scalingCorrection));
							mainController.addXMove(xOffset - lastXOffset);
							mainController.addYMove(yOffset - lastYOffset);
							break;
						case NE:
							mainController.addXScale(calcXScale(xOffset, scalingCorrection));
							mainController.addYScale(-calcYScale(yOffset, scalingCorrection));
							mainController.addYMove(yOffset - lastYOffset);
							break;
						case SW:
							mainController.addXScale(-calcXScale(xOffset, scalingCorrection));
							mainController.addYScale(calcYScale(yOffset, scalingCorrection));
							mainController.addXMove(xOffset - lastXOffset);
							break;
						case SE:
							mainController.addXScale(calcXScale(xOffset, scalingCorrection));
							mainController.addYScale(calcYScale(yOffset, scalingCorrection));
							break;
						default:
							break;
					}
					mainController.repaintCanvases();
					break;
				default:
					break;
			}

			lastXOffset = xOffset;
			lastYOffset = yOffset;
		});

		canvas.setOnMouseReleased(event -> {
			mainController.setTransformationInProgress(false);
			// Some performance-hungry features are turned off during transformations, so we need
			// to repaint the image once after a completed transformation
			mainController.repaintCanvases();
			lastXOffset = 0;
			lastYOffset = 0;
		});

		canvas.setOnMouseMoved(event -> {
			switch (selectedTool) {
				case MOVE:
					canvas.setCursor(Cursor.MOVE);
					break;
				case SCALE:
					double x = event.getX();
					double y = event.getY();
					double width = canvas.getWidth();
					double height = canvas.getHeight();

					if (x < width / 2 && y < height / 2) {
						canvas.setCursor(Cursor.NW_RESIZE);
					} else if (x > width / 2 && y < height / 2) {
						canvas.setCursor(Cursor.NE_RESIZE);
					} else if (x < width / 2 && y > height / 2) {
						canvas.setCursor(Cursor.SW_RESIZE);
					} else if (x > width / 2 && y > height / 2) {
						canvas.setCursor(Cursor.SE_RESIZE);
					}
					break;
				default:
					canvas.setCursor(Cursor.DEFAULT);
					break;
			}
		});

		/*
		 * This listener _has_ to be placed on the scrollPane and not on the
		 * image view. At least with standard settings.
		 */
		scrollPane.setOnKeyPressed(event -> {
			switch (selectedTool) {
				case MOVE:
					if (KeyCode.LEFT.equals(event.getCode())) {
						mainController.addXMove(-1);
					}
					if (KeyCode.RIGHT.equals(event.getCode())) {
						mainController.addXMove(1);
					}
					if (KeyCode.UP.equals(event.getCode())) {
						mainController.addYMove(-1);
					}
					if (KeyCode.DOWN.equals(event.getCode())) {
						mainController.addYMove(1);
					}
					mainController.repaintCanvases();
				default:
					break;
			}
		});
	}

	public void setCorrectionFactor(double correctionFactor) {
		this.correctionFactor = correctionFactor;
	}

	private double calcYScale(double yOffset, double scalingCorrection) {
		return yOffset / scalingCorrection - (lastYOffset / scalingCorrection);
	}

	private double calcXScale(double xOffset, double scalingCorrection) {
		return xOffset / scalingCorrection - (lastXOffset / scalingCorrection);
	}

	private int calcYOffset(MouseEvent event) {
		return (int) ((event.getY() - dragDeltaY) * this.correctionFactor);
	}

	private int calcXOffset(MouseEvent event) {
		return (int) ((event.getX() - dragDeltaX) * this.correctionFactor);
	}

	public void setTool(MainController.Tool tool) {
		selectedTool = tool;
	}

	private enum DragStart {
		NW, NE, SW, SE
	}
}