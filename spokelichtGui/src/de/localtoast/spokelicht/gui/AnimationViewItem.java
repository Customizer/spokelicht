package de.localtoast.spokelicht.gui;

/**
 * Created by arne on 8/6/15.
 */
public class AnimationViewItem {
	private boolean addButton = false;
	private int animationIndex = -1;

	public AnimationViewItem() {
		this.addButton = true;
	}

	public AnimationViewItem(int animationIndex) {
		this.animationIndex = animationIndex;
	}

	public int getAnimationIndex() {
		return animationIndex;
	}
}
