package de.localtoast.spokelicht.gui.settings;

import de.localtoast.spokelicht.gui.MainController;
import de.localtoast.spokelicht.gui.SpokelichtGui;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Preferences {
	private static final String RECENT_ANIMATION_SETS_FILE_PATH = "recentAnimationSetsFilePath";

	private static final String LAST_USED_PATH = "lastUsedPath";

	private final java.util.prefs.Preferences prefs;
	private MainController mainController;
	private Menu menuOpenRecent;

	public Preferences(MainController mainController, Menu menuOpenRecent) {
		this.mainController = mainController;
		this.menuOpenRecent = menuOpenRecent;
		prefs = java.util.prefs.Preferences.userNodeForPackage(SpokelichtGui.class);
		updateRecentAnimationsMenu();
	}

	public void addFileToRecentAnimations(String file) {
		List<String> recentAnimationSets = getRecentAnimationSets();
		recentAnimationSets.remove(file);
		recentAnimationSets.add(0, file);
		if (recentAnimationSets.size() == 11) {
			recentAnimationSets.remove(10);
		}
		updateRecentAnimationsMenu(recentAnimationSets);
		savePreferences(recentAnimationSets);
	}

	public File getLastUsedPath() {
		String path = "";
		String lastUsed = prefs.get(LAST_USED_PATH, "");
		if (lastUsed != null) {
			path = lastUsed;
		}
		return new File(path);
	}

	private void updateRecentAnimationsMenu() {
		updateRecentAnimationsMenu(getRecentAnimationSets());
	}

	private void updateRecentAnimationsMenu(List<String> recentAnimationSets) {
		ObservableList<MenuItem> items = menuOpenRecent.getItems();
		items.clear();
		for (String filePath : recentAnimationSets) {
			MenuItem menuItem = new MenuItem(filePath);
			menuItem.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					mainController.loadAnimationSet(filePath);
				}
			});
			items.add(menuItem);
		}
	}

	private List<String> getRecentAnimationSets() {
		List<String> recentAnimationSets = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			String filePath = prefs.get(RECENT_ANIMATION_SETS_FILE_PATH + i, "");
			if (!"".equals(filePath)) {
				recentAnimationSets.add(filePath);
			}
		}
		return recentAnimationSets;
	}

	private void savePreferences(List<String> recentAnimationSets) {
		for (int i = 0; i < 10; i++) {
			if (i < recentAnimationSets.size()) {
				prefs.put(RECENT_ANIMATION_SETS_FILE_PATH + i,
								recentAnimationSets.get(i));
			} else {
				prefs.put(RECENT_ANIMATION_SETS_FILE_PATH + i, "");
			}

			if (i == 0) {
				prefs.put(LAST_USED_PATH, FilenameUtils.getFullPath(recentAnimationSets.get(i)));
			}
		}
	}

}
