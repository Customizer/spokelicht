package de.localtoast.spokelicht.gui;

import de.localtoast.spokelicht.animations.Animation;
import de.localtoast.spokelicht.animations.AnimationSet;
import de.localtoast.spokelicht.animations.AnimationSetController;
import de.localtoast.spokelicht.gui.settings.Preferences;
import javafx.collections.ObservableList;
import javafx.scene.control.MenuItem;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

public class AnimationSetFileHandler {

	private Stage primaryStage;
	private AnimationsViewController animationsViewController;
	private MenuItem menuSaveAnimationSet;
	private Preferences preferences;
	AnimationSetController controller;

	public AnimationSetFileHandler(Stage primaryStage,
																 AnimationsViewController animationsViewController,
																 MenuItem menuSaveAnimationSet,
																 Preferences preferences,
																 AnimationSetController controller) {
		this.primaryStage = primaryStage;
		this.animationsViewController = animationsViewController;
		this.menuSaveAnimationSet = menuSaveAnimationSet;

		this.preferences = preferences;
		this.controller = controller;
	}

	public void loadAnimationSet() {
		File file = showOpenFileDialog();

		if (file != null) {
			try {
				loadAnimationSet(file.getCanonicalPath());
			} catch (IOException e) {
				e.printStackTrace();
				// TODO add proper error handling
			}
		}
	}

	private File showOpenFileDialog() {
		return createFileChooser().showOpenDialog(primaryStage);
	}

	private File showSaveFileDialog() {
		return createFileChooser().showSaveDialog(primaryStage);
	}

	private FileChooser createFileChooser() {
		FileChooser fileChooser = new FileChooser();

		File lastUsedPath = preferences.getLastUsedPath();
		if (lastUsedPath != null && lastUsedPath.exists()) {
			fileChooser.setInitialDirectory(lastUsedPath);
		}

		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
						"Spokelicht XML files (" + AnimationSetController.FILE_EXTENSION_SXML + ")", "*"
						+ AnimationSetController.FILE_EXTENSION_SXML);
		fileChooser.getExtensionFilters().add(extFilter);
		return fileChooser;
	}

	public void loadAnimationSet(String filePath) {
		AnimationSet animationSet = controller.loadAnimationSet(new File(filePath));
		animationsViewController.setAnimationSet(animationSet);
		preferences.addFileToRecentAnimations(filePath);
		menuSaveAnimationSet.setDisable(false);
	}

	public void saveAnimationSetAs() {
		File file = showSaveFileDialog();

		if (file != null) {
			saveAnimationSetAs(file);
		}

	}

	public void saveAnimationSet() {
		controller.saveAnimationSet();
		menuSaveAnimationSet.setDisable(false);
	}

	private void saveAnimationSetAs(File file) {

		try {
			controller.saveAnimationSetAs(file);
			preferences.addFileToRecentAnimations(controller.getAnimationSetFileName()
							.getCanonicalPath());
			menuSaveAnimationSet.setDisable(false);
		} catch (IOException e) {
			e.printStackTrace();
			// TODO add proper error handling
		}
	}

	public void createNewAnimationSet() {
		controller.createNewAnimationSet(showSaveFileDialog());
	}

	public void addAnimation(ObservableList<AnimationViewItem> items) {
		FileChooser fileChooser = new FileChooser();
		File lastUsedPath = preferences.getLastUsedPath();
		if (lastUsedPath != null && lastUsedPath.exists()) {
			fileChooser.setInitialDirectory(lastUsedPath);
		}
		File file = fileChooser.showOpenDialog(primaryStage);

		if (file != null) {
			int newIndex = addAnimation(file);
			items.add(items.size() - 1, new AnimationViewItem(newIndex));
		}
	}

	private int addAnimation(File imageFile) {
		try {
			int animationIndex = controller.addAnimation(imageFile);
			File newLocationOfImageFile = controller.getFullImagePath(animationIndex);
			return animationIndex;
		} catch (IOException e) {
			e.printStackTrace();
			// TODO add proper error handling
		}
		return -1;
	}


	public Animation getAnimation(int index) {
		return controller.getAnimation(index);
	}
}
