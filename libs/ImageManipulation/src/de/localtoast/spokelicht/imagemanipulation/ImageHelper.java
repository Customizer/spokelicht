package de.localtoast.spokelicht.imagemanipulation;

import Catalano.Imaging.FastBitmap;
import Catalano.Imaging.Filters.Resize;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

public class ImageHelper {

	private static final Map<String, String> MIME_TYPES = new HashMap<>();

	static {
		MIME_TYPES.put("bmp", "image/bmp");
		MIME_TYPES.put("gif", "image/gif");
		MIME_TYPES.put("jpeg", "image/jpeg");
		MIME_TYPES.put("jpg", "image/jpeg");
		MIME_TYPES.put("jpe", "image/jpeg");
		MIME_TYPES.put("png", "image/png");
		MIME_TYPES.put("tiff", "image/tiff");
		MIME_TYPES.put("tif", "image/tiff");

	}


	// TODO remove dependencies to canvas and graphicscontext and perhaps to swingfxutils, too
	public static List<WritableImage> loadImages(List<File> imageFiles) throws IOException {
		List<WritableImage> frames = new ArrayList<>();
		for (File imageFile : imageFiles) {


			String mimeType = Files.probeContentType(imageFile
							.toPath());

			// thanks to http://bugs.java.com/bugdatabase/view_bug.do?bug_id=7133484
			// or to be more precise oracle's unwilling to fix this there is no mime-type list
			// on the mac (at least). So here is our own...
			if (mimeType == null || mimeType.length() == 0) {
				int dotIndex = imageFile.getName().lastIndexOf('.');
				if (dotIndex != -1) {
					mimeType = MIME_TYPES.get(imageFile.getName().substring(dotIndex+1).toLowerCase());
				}
			}
			if (mimeType == null || mimeType.length() == 0) {
				throw new IOException("Imagefile cannot be read, mime-type unknown. Only PNG, JPG, TIFF, " +
								"GIF and BMP files will work.");
			}
			Iterator<ImageReader> imageReaders = ImageIO
							.getImageReadersByMIMEType(mimeType);

			if (imageReaders.hasNext()) {
				ImageReader imageReader = imageReaders.next();
				imageReader.setInput(ImageIO
								.createImageInputStream(new FileInputStream(imageFile
												.getCanonicalPath())));

			/*
			 * The following part is ugly as hell, but I don't know how else I
			 * should do it. It is even advised to do so in the JavaDoc of the
			 * method getNumImages. If anybody knows of a better way to do this,
			 * please tell me!
			 */
				int i = 0;
				try {
					while (true) {
						frames.add(ensureSameSuitableDimensions(SwingFXUtils
										.toFXImage(imageReader.read(i), null)));
						i++;
					}
				} catch (IndexOutOfBoundsException e) {
					// The loop is finished now, there are no frames left
				}
			}
		}
		return resize(frames);
	}

	// We scale down big images, to ensure smooth performance in image operations
	private static List<WritableImage> resize(List<WritableImage> frames) {
		int maxSize = 200;
		List<WritableImage> resizedFrames = new ArrayList<>();
		for (WritableImage frame : frames) {
			int width = (int) frame.getWidth();
			int height = (int) frame.getHeight();
			if (width > maxSize || height > maxSize) {
				Resize filter = new Resize(maxSize, (int) (height * ((double) maxSize / width)));
				if (height > width) {
					filter = new Resize((int) (width * ((double) maxSize / height)), maxSize);
				}
				FastBitmap fastBitmap = new FastBitmap(SwingFXUtils.fromFXImage(frame, null));
				filter.applyInPlace(fastBitmap);
				resizedFrames.add(SwingFXUtils.toFXImage(fastBitmap.toBufferedImage(), null));
			} else {
				resizedFrames.add(frame);
			}
		}

		return resizedFrames;
	}

	private static WritableImage ensureSameSuitableDimensions(WritableImage image) {
		double width = image.getWidth();
		double height = image.getHeight();
		double newWidth = width;
		double newHeight = height;

		newWidth = Math.max(newWidth, newHeight);
		newHeight = newWidth;

		Canvas canvas = new Canvas(newWidth, newHeight);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		gc.drawImage(image, (newWidth - width) / 2, (newHeight - height) / 2, width, height);

		return image;
	}
}