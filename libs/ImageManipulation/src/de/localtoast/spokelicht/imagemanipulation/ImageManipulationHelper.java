package de.localtoast.spokelicht.imagemanipulation;

import Catalano.Imaging.FastBitmap;
import Catalano.Imaging.Filters.Resize;
import Catalano.Imaging.Filters.Rotate;
import com.bric.image.pixel.quantize.ErrorDiffusionImageQuantization;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by arne on 8/30/15.
 */
public class ImageManipulationHelper {
	private FastBitmap image;

	private int xMove = 0;
	private int yMove = 0;
	private int rotationInDegrees = 0;
	private int originalWidth = 0;
	private int originalHeight = 0;
	private int newWidth = 0;
	private int newHeight = 0;
	private Map<String, String> replacementColors = new HashMap<>();

	public ImageManipulationHelper(BufferedImage image,
																 int xMove,
																 int yMove,
																 double xScale,
																 double yScale,
																 int rotationInDegrees,
																 int originalWidth,
																 int originalHeight,
																 Map<String, String> replacementColors) {
		this.originalWidth = originalWidth;
		this.originalHeight = originalHeight;
		this.image = new FastBitmap(image);

		double newSize = Math.max(image.getWidth(), image.getHeight());
		int newWidth = (int) (xScale * newSize);
		int newHeight = (int) (yScale * newSize);

		setMovement(xMove, yMove);
		setRotationInDegrees(-rotationInDegrees);
		setNewSize(newWidth, newHeight);

		this.replacementColors = replacementColors;
	}

	public BufferedImage runManipulations() {
		rotate();
		resize();
		moveAndMakeQuadratic();
		reduceImage();
		replaceColors();

		return image.toBufferedImage();
	}

	private void setMovement(int xMove, int yMove) {
		this.xMove = xMove;
		this.yMove = yMove;
	}

	private void setRotationInDegrees(int rotationInDegrees) {
		this.rotationInDegrees = rotationInDegrees % 360;
	}

	private void setNewSize(int newWidth, int newHeight) {
		this.newWidth = Math.max(newWidth, 10);
		this.newHeight = Math.max(newHeight, 10);
	}

	private void rotate() {
		Rotate filter = new Rotate(rotationInDegrees);
		filter.applyInPlace(image);
	}

	private void moveAndMakeQuadratic() {
		int sourceWidth = image.getWidth();
		int sourceHeight = image.getHeight();
		int newWidthAndHeight = Math.max(originalWidth, originalHeight);
		FastBitmap temp = new FastBitmap(newWidthAndHeight, newWidthAndHeight);

		for (int x = 0; x < newWidthAndHeight; x++) {
			int xOriginal = x - xMove;
			for (int y = 0; y < newWidthAndHeight; y++) {
				int yOriginal = y - yMove;
				// There seems to be a bug in the setRGB and getRGB methods of the FastBitmap
				// class. x and y have to be switched for correct results.
				if (xOriginal < 0 || xOriginal >= sourceWidth
								|| yOriginal < 0 || yOriginal >= sourceHeight) {
					temp.setRGB(y, x, 0, 0, 0);
				} else {
					temp.setRGB(y, x, image.getRGB(yOriginal, xOriginal));
				}
			}
		}

		image = temp;
	}

	private void resize() {
		Resize filter = new Resize(newWidth, newHeight);
		filter.applyInPlace(image);
	}

	private void reduceImage() {
		int colorDepth = 16;
		image = new FastBitmap(ErrorDiffusionImageQuantization.reduce(image.toBufferedImage(),
						colorDepth));
	}

	private void replaceColors() {
		// TODO too many conversions between image buffer formats
		WritableImage writableImage = SwingFXUtils.toFXImage(image.toBufferedImage(), null);
		PixelWriter writer = writableImage.getPixelWriter();
		PixelReader reader = writableImage.getPixelReader();

		for (int x = 0; x < image.getWidth(); x++) {
			for (int y = 0; y < image.getHeight(); y++) {
				for (String oldColorWeb : replacementColors.keySet()) {
					Color oldColor = Color.web(oldColorWeb);
					if (reader.getColor(x, y).equals(oldColor)) {
						writer.setColor(x, y, Color.web(replacementColors.get(oldColorWeb)));
						break;
					}
				}
			}
		}
		image = new FastBitmap(SwingFXUtils.fromFXImage(writableImage, null));
	}
}
