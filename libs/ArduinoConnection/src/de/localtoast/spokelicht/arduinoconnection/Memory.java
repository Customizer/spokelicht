package de.localtoast.spokelicht.arduinoconnection;

import de.localtoast.spokelicht.utils.ArrayUtils;

import java.util.List;

public class Memory {
	private List<Animation> animations;

	public Memory(List<Animation> animations) {
		this.animations = animations;
	}

	public byte[] toBinary() {
		byte[] memoryBytes = new byte[]{};
		int prevAddress = 0;
		for (byte i = 0; i < animations.size(); i++) {
			byte[] prevAddressBytes = toUInt16(prevAddress);
			byte[] binary = animations.get(i).toBinary();

			int currentAddress = memoryBytes.length;
			int nextAddress = currentAddress + 5 + binary.length;
			byte[] nextAddressBytes = toUInt16(nextAddress);
			System.out.println("Animation : " + i);
			System.out.println("CurrentAnimation : " + i);
			System.out.println("binary length : " + binary.length);
			System.out.println("total length : "
							+ (currentAddress + 5 + binary.length));

			System.out.println("previousAddress : " + prevAddress + " --> "
							+ debugBytes(prevAddressBytes));
			System.out.println("nextAddress : " + nextAddress + " --> "
							+ debugBytes(nextAddressBytes));
			System.out.println("# of pics "
							+ animations.get(i).getPictures().size());
			memoryBytes = ArrayUtils.concatArrays(memoryBytes,
							new byte[]{i}, prevAddressBytes, nextAddressBytes,
							binary);

			if (i == animations.size() - 1) {
				System.arraycopy(toUInt16(0), 0, memoryBytes,
								currentAddress + 3, 2);
			}

			prevAddress = currentAddress;
		}

		// set last address as previous address of first animation
		System.arraycopy(toUInt16(prevAddress), 0, memoryBytes, 1, 2);

		return memoryBytes;
	}

	private String debugBytes(byte[] b) {
		String s = "";
		String space = "";
		for (byte c : b) {
			s += space + "0x" + Integer.toHexString(c);
			space = " ";
		}
		return s;
	}

	public byte[] toUInt16(int value) {
		return new byte[]{(byte) (value >> 8), (byte) value};
	}

	public boolean isEmpty() {
		return animations.isEmpty();
	}
}
