/*
 *  Spokelicht GUI for the project Spokelicht
 *  Copyright (C) 2014 Arne Augenstein
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.localtoast.spokelicht.arduinoconnection;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

import static de.localtoast.spokelicht.utils.ByteHelper.b2h;
import static de.localtoast.spokelicht.utils.ByteHelper.b2s;

/**
 * This class sends the transformed image data to the Arduino.
 *
 * @author Arne Augenstein
 */
public class DataSender {
	public static final boolean DEBUG = false;

	private static final String SERIAL_PORT_PROPERTY = "SERIAL_PORT";
	private static final int BAUD_RATE = 57600;
	// TODO make ports configurable
	// private static final String DEFAULT_UNIX_SERIAL_PORT = "/dev/ttyUSB0";
	private static final String DEFAULT_UNIX_SERIAL_PORT = "/dev/rfcomm0";
	private static final String DEFAULT_MAC_SERIAL_PORT = "/dev/tty.SLAB_USBtoUART";
	private ProgressListener progressListener;

	public void setProgressListener(ProgressListener progressListener) {
		this.progressListener = progressListener;
	}

	public void updateArduinoMemory(Memory memory)
					throws SerialConnectionException {
		try {
			progressListener.update(-1, 0);
			memory.toBinary();
			SerialPort serialPort = openPort();
			transferDataToArduino(serialPort, memory.toBinary());
			// The Arduino does not automatically update the current picture, so we reset it.
			resetArduino(serialPort);
			serialPort.closePort();
		} catch (InterruptedException | SerialPortException | SerialPortTimeoutException e) {
			throw new SerialConnectionException(e);
		}
		progressListener.update(100, 100);
	}

	public SerialPort openPort() throws InterruptedException,
					SerialPortException, SerialConnectionException {
		String portName = "";
		if (System.getProperty(SERIAL_PORT_PROPERTY) != null) {
			portName = System.getProperty(SERIAL_PORT_PROPERTY);
		} else if (isUnix()) {
			portName = DEFAULT_UNIX_SERIAL_PORT;
		} else if (isMac()) {
			portName = DEFAULT_MAC_SERIAL_PORT;
		}

		SerialPort serialPort = new SerialPort(portName);
		serialPort.openPort();

		// wait for Arduino's reset to complete
		Thread.sleep(3000);
		if (!serialPort.setParams(BAUD_RATE, 8, 1, 0)) {
			throw new SerialConnectionException(
							"Parameters of serial connection could not be set.");
		}
		return serialPort;
	}

	private boolean isMac() {

		return (getOsProperty().indexOf("mac") >= 0);

	}

	private boolean isUnix() {

		String osProperty = getOsProperty();
		return (osProperty.indexOf("nix") >= 0
						|| osProperty.indexOf("nux") >= 0 || osProperty.indexOf("aix") > 0);

	}

	private String getOsProperty() {
		return System.getProperty("os.name").toLowerCase();
	}

	public void transferDataToArduino(SerialPort port, byte[] dataToSend)
					throws InterruptedException, SerialPortException,
					SerialPortTimeoutException, SerialConnectionException {

		int dataSize = dataToSend.length;
		if (DEBUG) {
			System.out.println("Storing " + dataSize + " bytes.");
			System.out.println("Wait for reset");
		}


		// When using Bluetooth, the Arduino does not reset, so we don't have to wait.
		//if (DEBUG) {
		//System.out.println("Wait for reset");
		//}
		//Thread.sleep(3000);

		// flush receiver...
		port.readBytes();
		System.out.println("Request write...");
		byte[] command = {'w', (byte) (dataSize >> 24),
						(byte) (dataSize >> 16), (byte) (dataSize >> 8),
						(byte) dataSize, '$'};

		if (DEBUG) {
			System.out.print("Size-Bytes 1: " + b2h(command[1]));
			System.out.print(" 2: " + b2h(command[2]));
			System.out.print(" 3: " + b2h(command[3]));
			System.out.println(" 4: " + b2h(command[4]));
		}
		port.writeBytes(command);

		// wait for the answer
		byte[] ack = port.readBytes(2, 500);
		if (ack[0] != 'o') {
			// something wrong happened.
			String chars = "";
			String hex = "";
			for (int j = 0; j < ack.length; j++) {
				chars += (char) ack[j];
				hex += b2h(ack[j]) + ", ";

			}
			throw new IllegalStateException(" Expected OK, but got: " + chars
							+ " / " + hex);
		}

		progressListener.update(0, dataSize);

		int chunkSize = 15;
		byte[] sendBuffer = new byte[chunkSize];
		byte chksum = 0;
		for (int i = 0; i < dataToSend.length; i++) {
			if (i >= chunkSize && i % chunkSize == 0) {
				byte chkrepeat = writeChunk(port, sendBuffer);
				if (chkrepeat != chksum) {
					throw new SerialConnectionException("Checksum failure at " + (i - 1) + " "
									+ b2s(chksum) + " vs " + b2s(chkrepeat));
				}

				chksum = 0;
				if (i + chunkSize > dataToSend.length) {
					sendBuffer = new byte[dataToSend.length - i];
				}

			}
			// adjust send buffer size for last chunk.
			sendBuffer[i % chunkSize] = dataToSend[i];

			// System.out.print("Adding " + b2s(byteArray[i]) + " to checksum "
			// + b2s(chksum));
			chksum ^= dataToSend[i];
			// System.out.println(" result " + b2s(chksum));

			progressListener.update(i, dataSize);
		}
		if (DEBUG) {
			System.out.println("Raus mit dem Rest!!!");
		}
		byte chkrepeat = writeChunk(port, sendBuffer);
		if (chkrepeat != chksum) {
			System.out.println("Checksum failure at last chunk " + b2s(chksum)
							+ " vs " + b2s(chkrepeat));

		}
		System.out.println();
		System.out.println("done sending.");
	}


	private void resetArduino(SerialPort port) throws SerialPortException {
		System.out.println("resetting Arduino.");
		byte[] command = {'r', '$'};
		port.writeBytes(command);
	}

	byte writeChunk(final SerialPort port, byte[] temp)
					throws SerialPortException, InterruptedException {
		if (DEBUG) {
			System.out.print("Sende: ");
		}

		port.writeBytes(temp);
		if (DEBUG) {
			System.out.println();
		}

		byte[] chkrepeat;
		while ((chkrepeat = port.readBytes()) == null) {
			// System.out.println("wait for arduino to answer.");
			Thread.sleep(1);
		}
		if (DEBUG) {
			System.out.println("received " + b2h(chkrepeat[0]) + " / "
							+ b2s(chkrepeat[0]) + " / " + (char) chkrepeat[0]);
		}
		if (chkrepeat.length > 1) {
			for (int i = 1; i < chkrepeat.length; i++) {
				if (DEBUG) {
					System.out.println("received " + b2h(chkrepeat[i]) + " / "
									+ b2s(chkrepeat[i]) + " / " + (char) chkrepeat[i]);
				}

			}
		}
		if (DEBUG) {
			System.out.print(".");
		}
		return chkrepeat[0];
	}

}
