package de.localtoast.spokelicht.arduinoconnection;

/**
 * Created by Arne Augenstein on 7/24/15.
 */
public interface ProgressListener {
	public void update(int workDone, int max);
}
