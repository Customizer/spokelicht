package de.localtoast.spokelicht.arduinoconnection;

import de.localtoast.spokelicht.utils.ArrayUtils;
import de.localtoast.spokelicht.utils.ExtendedBitSet;
import de.localtoast.spokelicht.utils.Line;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * convertable representation of a picture consisting of several lines.
 *
 * @author Peer Hartmann
 */
public class Picture {

	private final List<Line> lines;
	private final byte displayDuration;
	private final Map<Color, Byte> colorToPosition = new HashMap<>();
	private final List<Color> colors = new ArrayList<>();

	public Picture(List<Line> lines, int displayDuration) {
		super();
		if (displayDuration > 0xFF) {
			throw new IllegalArgumentException(
							"DisplayDuration must not be bigger than 0xFF");
		}
		this.lines = lines;
		{
			byte colorIndex = 0;
			for (Line line : lines) {
				for (int i = 0; i < line.getPixelCount(); i++) {
					Color color = line.getPixel(i);
					if (colorToPosition.get(color) == null) {
						colorToPosition.put(color, colorIndex);
						colors.add(color);
						colorIndex++;
					}
				}
			}
			if (colors.size() > 16) {
				throw new IllegalArgumentException("too many colors in picture");

			}
		}
		this.displayDuration = (byte) displayDuration;
	}

	/**
	 * returns all the colors in the lines in the format r1,g1,b1,r2,g2,b2,...
	 *
	 * @return
	 */
	public byte[] getColorMap() {
		byte[] retval = new byte[colors.size() * 3];
		int i = 0;
		for (Color c : colors) {
			c = correctColor(c);
			retval[i++] = (byte) (c.getRed() * 255);
			retval[i++] = (byte) (c.getGreen() * 255);
			retval[i++] = (byte) (c.getBlue() * 255);
		}
		return retval;
	}

	public static Color correctColor(Color c) {
		c = new Color(c.getRed(), c.getGreen(), c.getBlue() / 2, c.getOpacity());
		return c;
	}

	/**
	 * returns the lines data in the format [color-index1 | color-index2,
	 * color-index3 | color-index4 ] where color-index is the index of
	 * the color of the pixel within the colormap. Two pixels share one byte.
	 *
	 * @return
	 */
	public byte[] getLineData() {
		ExtendedBitSet bitSet = new ExtendedBitSet();
		for (int i = 0; i < lines.size() / 2; i++) {
			// We send the lines in an order, which can be read efficiently out of the EEPROM.
			// I.e., we save them in the order, in which they get displayed on the two opposite
			// LED arms.
			bitSet.add(getColorData(lines.get(i)));
			bitSet.add(getColorData(lines.get(i + lines.size() / 2)));
		}
		return bitSet.toByteArray();
	}

	private ExtendedBitSet getColorData(Line line) {
		ExtendedBitSet bitSet = new ExtendedBitSet();
		for (int pxIdx = 0; pxIdx < line.getPixelCount(); pxIdx++) {
			Color pixel = line.getPixel(pxIdx);
			Byte value = colorToPosition.get(pixel);

			if (value == null) {
				value = 0x00;
			}
			bitSet.add(value, 4);
		}

		return bitSet;
	}

	private List<Line> getLines() {
		// const-pattern.
		return new ArrayList<Line>(lines);
	}

	private byte getDisplayDuration() {
		return displayDuration;
	}

	/**
	 * returns the complete binary format:
	 * <p>
	 * <pre>
	 * Picture-Header
	 * 		 --------------
	 * 		 0              Color count: How many colors are actually used by this image?
	 * 		                Maximum: 0x0F (16)
	 * 		 1              Display time: How long should this picture be displayed (0 -> unlimited)
	 * 		 2-(3*bit0)     Support for 2^4=16 Colors with 24-Bit depth each per picture, so maximum:
	 * 		                0x30 (48)
	 *
	 * </pre>
	 *
	 * @return
	 */
	public byte[] toBinary() {
		System.out.println("------------------- Picture -------------------");
		System.out.println("colors size: " + colors.size());
		System.out.println("disp duration: " + displayDuration);
		System.out.println("color map size: " + getColorMap().length);
		System.out.println("line Data size: " + getLineData().length);
		for (Map.Entry<Color, Byte> colorEntry : colorToPosition.entrySet()) {
			System.out.println("Position " + colorEntry.getValue() + " Color "
							+ colorEntry.getKey());

		}
		byte[] colorMap = getColorMap();
		for (int i = 0; i < colorMap.length; i++) {
			byte b = colorMap[i];
			if ((i % 3) == 0) {
				System.out.println("");
				System.out.print("color: " + (i / 3) + " ");
			}
			if (b > 0) {
				System.out.print(b);
			} else {
				System.out.print(b & 0xFF);
			}
			System.out.print(" ");
		}

		// for (Line line2 : lines) {
		// for (int i = 0; i < line2.getPixelCount(); i++) {
		//
		// System.out.print(colorToPosition.get(line2.getPixel(i)));
		//
		// }
		// System.out.println();
		// }
		//
		// byte[] lineData = getLineData();
		// for (int i = 0; i < lineData.length; i++) {
		// byte b = lineData[i];
		// if ((i % 10) == 0) {
		// System.out.println();
		// System.out.println(i / 10);
		// System.out.println();
		// }
		// if (b > 0) {
		// System.out.print(b);
		// } else {
		// System.out.print(b & 0xFF);
		// }
		// System.out.print(" ");
		// }

		System.out.println("-----------------------------------------------");

		byte retval[] = ArrayUtils.concatArrays(new byte[]{(byte) colors.size(), displayDuration},
						getColorMap(),
						getLineData());
		return retval;
	}

}
