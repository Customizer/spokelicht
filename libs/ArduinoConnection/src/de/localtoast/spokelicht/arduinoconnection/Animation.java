package de.localtoast.spokelicht.arduinoconnection;

import de.localtoast.spokelicht.utils.ArrayUtils;

import java.util.List;

public class Animation {
	private final List<Picture> pictures;

	public Animation(List<Picture> pictures) {
		super();
		if (pictures.size() > 0xFF) {
			throw new IllegalArgumentException(
							"Number of pictures per animation must not be bigger than 0xFF");
		}
		this.pictures = pictures;
	}

	public List<Picture> getPictures() {
		return pictures;
	}

	public byte[] toBinary() {
		byte[] pictureBytes = new byte[]{(byte) pictures.size()};

		for (int i = 0; i < pictures.size(); i++) {
			pictureBytes = ArrayUtils.concatArrays(pictureBytes, pictures.get(i).toBinary());
		}

		return pictureBytes;
	}

}
