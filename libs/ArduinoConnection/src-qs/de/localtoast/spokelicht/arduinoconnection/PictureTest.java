package de.localtoast.spokelicht.arduinoconnection;

import de.localtoast.spokelicht.arduinoconnection.Picture;
import de.localtoast.spokelicht.utils.Line;
import javafx.scene.paint.Color;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class PictureTest {
	Color c1 = Color.rgb(1, 2, 3);
	Color c2 = Color.rgb(11, 12, 13);
	Color c3 = Color.rgb(21, 22, 23);

	@Test
	public void testColorMapOnePixel() {
		Line l1 = new Line();
		l1.addPixel(c1);
		Picture sut = new Picture(l(l1), 5);
		Assert.assertArrayEquals(new byte[]{getCorrectedRed(c1), getCorrectedGreen(c1),
										getCorrectedBlue(c1)},
						sut.getColorMap());
		Assert.assertArrayEquals(new byte[]{0}, sut.getLineData());
		Assert.assertArrayEquals(new byte[]{1, 5, getCorrectedRed(c1), getCorrectedGreen(c1),
										getCorrectedBlue(c1), 0},
						sut.toBinary());

	}

	@Test
	public void testColorMapThreePixels() {
		Line l1 = new Line();
		l1.addPixel(c1);
		l1.addPixel(c1);
		l1.addPixel(c1);
		Line l2 = new Line();
		l2.addPixel(c2);
		l2.addPixel(c2);
		l2.addPixel(c2);

		Color correctedColor1 = Picture.correctColor(c1);
		Color correctedColor2 = Picture.correctColor(c2);
		Picture sut = new Picture(l(l1, l2), 5);
		Assert.assertArrayEquals(new byte[]{
										getCorrectedRed(c1), getCorrectedGreen(c1),
										getCorrectedBlue(c1), getCorrectedRed(c2), getCorrectedGreen(c2),
										getCorrectedBlue(c2)},
						sut.getColorMap());
		Assert.assertArrayEquals(new byte[]{0, 1 << 4, 1 << 4 | 1},
						sut.getLineData());
		Assert.assertArrayEquals(new byte[]{2, 5, getCorrectedRed(c1), getCorrectedGreen(c1),
						getCorrectedBlue(c1), getCorrectedRed(c2), getCorrectedGreen(c2),
						getCorrectedBlue(c2), 0, 1 << 4,
						1 << 4 | 1}, sut.toBinary());

	}

	@Test
	public void testColorMapMixedPixels() {
		Line l1 = new Line();
		l1.addPixel(c1);
		l1.addPixel(c2);
		l1.addPixel(c3);
		Line l2 = new Line();
		l2.addPixel(c3);
		l2.addPixel(c2);
		l2.addPixel(c1);
		Picture sut = new Picture(l(l1, l2), 5);
		Assert.assertArrayEquals(new byte[]{getCorrectedRed(c1), getCorrectedGreen(c1),
										getCorrectedBlue(c1), getCorrectedRed(c2), getCorrectedGreen(c2),
										getCorrectedBlue(c2), getCorrectedRed(c3), getCorrectedGreen(c3),
										getCorrectedBlue(c3)},
						sut.getColorMap());
		Assert.assertArrayEquals(new byte[]{0 | 1 << 4, 2 | 2 << 4, 1 | 0},
						sut.getLineData());
		Assert.assertArrayEquals(new byte[]{3, 5, getCorrectedRed(c1), getCorrectedGreen(c1),
						getCorrectedBlue(c1), getCorrectedRed(c2), getCorrectedGreen(c2),
						getCorrectedBlue(c2), getCorrectedRed(c3), getCorrectedGreen(c3),
						getCorrectedBlue(c3), 0 | 1 << 4,
						2 | 2 << 4, 1 | 0}, sut.toBinary());

	}

	private List<Line> l(Line... lines) {
		return Arrays.asList(lines);
	}

	private byte getCorrectedRed(Color c) {
		return (byte) (Picture.correctColor(c).getRed() * 255);
	}

	private byte getCorrectedGreen(Color c) {
		return (byte) (Picture.correctColor(c).getGreen() * 255);
	}

	private byte getCorrectedBlue(Color c) {
		return (byte) (Picture.correctColor(c).getBlue() * 255);
	}
}
