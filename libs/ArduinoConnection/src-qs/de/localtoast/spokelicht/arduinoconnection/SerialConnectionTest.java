package de.localtoast.spokelicht.arduinoconnection;

import de.localtoast.spokelicht.utils.ExtendedBitSet;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import org.junit.Assert;
import org.junit.Test;

import static de.localtoast.spokelicht.utils.ByteHelper.b2s;

public class SerialConnectionTest extends AbstractSerialTest {

	/**
	 * This test sends data to the Arduino and compares the sent data to the
	 * data the Arduino sends back.
	 *
	 * @throws Exception
	 */
	@Test
	public void send() throws Exception {
		ExtendedBitSet bitSet = new ExtendedBitSet();
		int maxSize = 32000 * 8; // 3584;// 3840;
		for (int i = 0; i < maxSize; i += 7) {
			bitSet.set(i);
		}
		bitSet.set(maxSize - 1);
		byte[] byteArray = bitSet.toByteArray();
		System.out.println("Waiting for reset...");

		DataSender sender = new DataSender();
		final SerialPort port = sender.openPort();

		Thread.sleep(3000);
		System.out.println("Writing...");
		int chunkSize = 32;
		byte[] temp = new byte[chunkSize];
		byte chksum = 0;
		for (int i = 0; i < byteArray.length; i++) {
			if (i >= chunkSize && i % chunkSize == 0) {
				byte chkrepeat = writeBytes(port, temp);
				if (chkrepeat != chksum) {
					System.out.println("Checksum failure at " + (i - 1) + " "
									+ b2s(chksum) + " vs " + b2s(chkrepeat));

				}
				chksum = 0;
			}
			temp[i % chunkSize] = byteArray[i];

			// System.out.print("Adding " + b2s(byteArray[i]) + " to checksum "
			// + b2s(chksum));
			chksum ^= byteArray[i];
			// System.out.println(" result " + b2s(chksum));

		}
		byte chkrepeat = writeBytes(port, temp);
		if (chkrepeat != chksum) {
			System.out.println("Checksum failure at last chunk " + b2s(chksum)
							+ " vs " + b2s(chkrepeat));

		}
		System.out.println();
		System.out.println("done sending.");
		System.out.println("Reading...");
		// cross coupling??? WTF?
		MySerialPortEventListener listener = new MySerialPortEventListener(port);
		port.addEventListener(listener);

		int totalWaitTime = 0;
		while (listener.getTotalBytes() != bitSet.length() / 8
						&& totalWaitTime < 240000) {
			Thread.sleep(500);
			totalWaitTime += 500;

		}

		System.out.println();
		port.closePort();

		/*
		 * If the original bit set is not divisible by 8, the last bits are
		 * filled with zeroes. If we want to compare, what we sent, we have to
		 * think of that.
		 */
		int length = bitSet.length();
		if (length % 8 != 0) {
			for (int i = length; i < length + (8 - length % 8); i++) {
				bitSet.clear(i);
			}
		}

		ExtendedBitSet receivedBitSet = listener.getSet();

		if (!bitSet.equals(receivedBitSet)) {
			if (bitSet.length() != receivedBitSet.length()) {
				System.out.println("Not all bits received! Sent "
								+ bitSet.length() + " , received: "
								+ receivedBitSet.length());

			}
			for (int i = 0; i < receivedBitSet.length(); i++) {
				boolean sent = bitSet.get(i);
				boolean received = receivedBitSet.get(i);
				if (sent != received) {
					System.out.println("Error at: " + i + " Sent: "
									+ (sent ? "1" : "0") + " received "
									+ (received ? "1" : "0"));
				}
			}
			System.out.println("Sent:");
			System.out.println(bitSet);
			System.out.println("Reveived");
			System.out.println(receivedBitSet);

		}

		Assert.assertTrue("Sent bits differ from received bits",
						bitSet.equals(receivedBitSet));
		System.out.println("Fertig.");
	}

	private class MySerialPortEventListener implements SerialPortEventListener {
		ExtendedBitSet set = new ExtendedBitSet();
		private SerialPort port;

		public MySerialPortEventListener(SerialPort port) {
			this.port = port;
		}

		public ExtendedBitSet getSet() {
			return set;
		}

		private volatile int totalBytes = 0;

		@Override
		public void serialEvent(SerialPortEvent event) {
			if (event.isRXCHAR()) {
				int byteCount = event.getEventValue();
				if (byteCount > 0) {
					try {
						totalBytes += byteCount;
						/*
						 * System.out.println("Received " + byteCount +
						 * " bytes, total " + totalBytes + " bytes");
						 */
						System.out.print(".");
						byte[] received = port.readBytes(byteCount);

						set.add(received);
					} catch (SerialPortException e) {
						System.out.println(e.getMessage());
					}
				}
			}
		}

		public int getTotalBytes() {
			return totalBytes;
		}

	}
}
