package de.localtoast.spokelicht.arduinoconnection;

import de.localtoast.spokelicht.utils.ArrayUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class MemoryTest {
	@Test
	public void testConcatOneAnimation() {
		Animation a1 = new AnimationMock(new byte[][]{//
						new byte[]{0xA, 0xB, 0xC} //
		});
		Memory sut = new Memory(Arrays.asList(a1));
		Assert.assertArrayEquals(new byte[]{0, 0, 0, 0, 0, 0xA, 0xB, 0xC},
						sut.toBinary());
	}

	@Test
	public void testConcatTwoAnimations() {
		Animation a1 = new AnimationMock(new byte[][]{//
						new byte[]{0xA, 0xB, 0xC} //
		});
		Animation a2 = new AnimationMock(new byte[][]{//
						new byte[]{0xD, 0xE, 0xF} //
		});
		Memory sut = new Memory(Arrays.asList(a1, a2));
		Assert.assertArrayEquals(new byte[]{ //
										0, 0, 8, 0, 8, 0xA, 0xB, 0xC, //
										1, 0, 0, 0, 0, 0xD, 0xE, 0xF} //
						, //
						sut.toBinary());
	}

	@Test
	/*
	 * This test was created to test the calculation of the next and previous addresses of the
	 * animations, when these addresses are a little bit bigger.
	 */
	public void testConcatTwoBigAnimations() {
		int firstAnimationSize = 2006;

		byte[] array = new byte[firstAnimationSize];
		for (int i = 0; i < firstAnimationSize; i += 3) {
			array[i] = 0xA;
			if (i + 1 < firstAnimationSize - 2) {
				array[i + 1] = 0xB;
			}
			if (i + 2 < firstAnimationSize - 1) {
				array[i + 2] = 0xC;
			}
		}
		Animation a1 = new AnimationMock(new byte[][]{//
						array //
		});
		Animation a2 = new AnimationMock(new byte[][]{//
						new byte[]{0xD, 0xE, 0xF} //
		});
		Memory sut = new Memory(Arrays.asList(a1, a2));

		byte[] targetArray = new byte[firstAnimationSize + 13];
		targetArray[0] = 0;
		targetArray[1] = (byte) 0x7;
		targetArray[2] = (byte) 0xdb;
		targetArray[3] = (byte) 0x7;
		targetArray[4] = (byte) 0xdb;

		for (int i = 5; i < firstAnimationSize + 5; i += 3) {
			targetArray[i] = 0xA;
			if (i + 1 < firstAnimationSize - 2 + 5) {
				targetArray[i + 1] = 0xB;
			}
			if (i + 2 < firstAnimationSize - 1 + 5) {
				targetArray[i + 2] = 0xC;
			}
		}

		targetArray[firstAnimationSize + 5] = 1;
		targetArray[firstAnimationSize + 6] = 0;
		targetArray[firstAnimationSize + 7] = 0;
		targetArray[firstAnimationSize + 8] = 0;
		targetArray[firstAnimationSize + 9] = 0;
		targetArray[firstAnimationSize + 10] = 0xD;
		targetArray[firstAnimationSize + 11] = 0xE;
		targetArray[firstAnimationSize + 12] = 0xF;

		Assert.assertArrayEquals("Arrays are not equal", targetArray, sut.toBinary());
	}

	@Test
	public void testConcatFourAnimations() {
		Animation a1 = new AnimationMock(new byte[][]{//
						new byte[]{0xA, 0xB, 0xC} //
		});
		Animation a2 = new AnimationMock(new byte[][]{//
						new byte[]{0xD, 0xE, 0xF, 0xF} //
		});
		Animation a3 = new AnimationMock(new byte[][]{//
						new byte[]{0x1A, 0x1B, 0x1C} //
		});
		Animation a4 = new AnimationMock(new byte[][]{//
						new byte[]{0x1D, 0x1E, 0x1F} //
		});
		Memory sut = new Memory(Arrays.asList(a1, a2, a3, a4));
		Assert.assertArrayEquals(new byte[]{ //
										0, 0, 25, 0, 8, 0xA, 0xB, 0xC, //
										1, 0, 0, 0, 17, 0xD, 0xE, 0xF, 0xF, //
										2, 0, 8, 0, 25, 0x1A, 0x1B, 0x1C, //
										3, 0, 17, 0, 0, 0x1D, 0x1E, 0x1F //

						} //
						, //
						sut.toBinary());
	}

	class AnimationMock extends Animation {

		private byte[][] pics;

		public AnimationMock(byte[][] pics) {
			super(new ArrayList<Picture>());
			this.pics = pics;
		}

		@Override
		public byte[] toBinary() {
			return ArrayUtils.concatArrays(pics);
		}

	}

}
