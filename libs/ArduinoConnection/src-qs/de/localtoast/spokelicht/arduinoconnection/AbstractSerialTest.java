package de.localtoast.spokelicht.arduinoconnection;

import jssc.SerialPort;
import jssc.SerialPortException;

import static de.localtoast.spokelicht.utils.ByteHelper.b2h;
import static de.localtoast.spokelicht.utils.ByteHelper.b2s;

public class AbstractSerialTest {

	byte writeBytes(final SerialPort port, byte[] temp)
					throws SerialPortException, InterruptedException {
		System.out.print("Sende: ");
		for (int j = 0; j < temp.length; j++) {
			byte b = temp[j];
			System.out.print("[" + j + "] " + b2h(b) + ", ");
			port.writeByte(b);
			Thread.sleep(2);
			byte[] unexpectedAnswer = port.readBytes();
			for (int i = 0; unexpectedAnswer != null
							&& i < unexpectedAnswer.length; i++) {
				System.out.println("unexpected answer: "
								+ b2h(unexpectedAnswer[i]) + " / "
								+ b2s(unexpectedAnswer[i]) + " / "
								+ (char) unexpectedAnswer[i]);

			}
		}
		System.out.println();

		byte[] chkrepeat;
		while ((chkrepeat = port.readBytes()) == null) {
			Thread.sleep(1);
		}
		System.out.println("received " + b2h(chkrepeat[0]) + " / "
						+ b2s(chkrepeat[0]) + " / " + (char) chkrepeat[0]);

		if (chkrepeat.length > 1) {
			for (int i = 1; i < chkrepeat.length; i++) {
				System.out.println("received " + b2h(chkrepeat[i]) + " / "
								+ b2s(chkrepeat[i]) + " / " + (char) chkrepeat[i]);

			}
		}
		System.out.print(".");
		return chkrepeat[0];
	}
}
