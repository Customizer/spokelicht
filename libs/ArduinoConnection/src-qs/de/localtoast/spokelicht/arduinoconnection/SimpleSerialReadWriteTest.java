package de.localtoast.spokelicht.arduinoconnection;

import de.localtoast.spokelicht.arduinoconnection.DataSender;
import jssc.SerialPort;
import jssc.SerialPortException;
import junit.framework.Assert;
import org.junit.Test;

public class SimpleSerialReadWriteTest {
	/**
	 * expects the Arduino-Program SimpleSerialRepeater to be running. Transmits
	 * a short string to the Arduino and waits for it to be repeated
	 *
	 * @throws Exception
	 * @throws SerialPortException
	 * @throws InterruptedException
	 */
	@Test
	public void testReceive() throws InterruptedException, SerialPortException,
					Exception {
		DataSender sender = new DataSender();
		final SerialPort port = sender.openPort();
		// final byte[] receivedByte = new byte[1];
		// SerialPortEventListener listener = new SerialPortEventListener() {
		// @Override
		// public void serialEvent(SerialPortEvent evt) {
		//
		// if (evt.isRXCHAR()) {
		// try {
		//
		// byte[] bytes = port.readBytes();
		// if (bytes.length > 1) {
		//
		// Assert.fail("More than one byte received");
		// }
		// System.out.print((char) bytes[0]);
		// receivedByte[0] = bytes[0];
		// synchronized (port) {
		// port.notify();
		// }
		//
		// } catch (SerialPortException e) {
		// e.printStackTrace();
		// try {
		// port.removeEventListener();
		// port.closePort();
		// } catch (SerialPortException e1) {
		// e1.printStackTrace();
		// }
		// Assert.fail();
		//
		// }
		//
		// }
		//
		// }
		//
		// };

		// port.addEventListener(listener);
		String toRepeat = "Hallo Welt, hier spricht der Spoklicht Testtreiber. Wie geht es denn so?";
		byte[] bytesToSend = toRepeat.getBytes();
		Thread.sleep(3000);
		for (byte b : bytesToSend) {
			byte[] btemp = new byte[1];
			btemp[0] = b;
			port.writeBytes(btemp);

			/*
			 * synchronized (port) { port.wait(1000); }
			 */
			byte[] chkrepeat;
			while ((chkrepeat = port.readBytes()) == null) {
				Thread.sleep(1);
			}
			System.out.print((char) chkrepeat[0]);
			Assert.assertEquals(b, chkrepeat[0]);
			Assert.assertEquals(1, chkrepeat.length);
		}
		System.out.println();
		System.out.println("Everything is well");
		// port.removeEventListener();
		port.closePort();
	}
}
