package de.localtoast.spokelicht.arduinoconnection;

import de.localtoast.spokelicht.arduinoconnection.DataSender;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import org.junit.Assert;
import org.junit.Test;

public class SimpleSerialReceiver {
	/**
	 * expects the Arduino-Program SimpleSerialSender to be running. Waits for 8
	 * x's and then reports everything is ok.
	 *
	 * @throws Exception
	 * @throws SerialPortException
	 * @throws InterruptedException
	 */
	@Test
	public void testReceive() throws InterruptedException, SerialPortException,
					Exception {
		DataSender sender = new DataSender();
		final SerialPort port = sender.openPort();

		SerialPortEventListener listener = new SerialPortEventListener() {
			int i = 0;

			@Override
			public void serialEvent(SerialPortEvent evt) {

				if (evt.isRXCHAR()) {
					try {

						byte[] bytes = port.readBytes();
						for (byte b : bytes) {
							i++;
							System.out.println(Integer.toHexString(b) + " ");
							if (i >= 9) {
								synchronized (port) {
									port.notify();
								}

							}
							if (b != 'x') {
								Assert.fail("recived wrong char 0x"
												+ Integer.toHexString(b));
							}
						}

					} catch (SerialPortException e) {
						e.printStackTrace();
					}

				}

			}

		};
		port.addEventListener(listener);
		synchronized (port) {
			port.wait();
		}
		System.out.println("Everything is well");
		port.removeEventListener();
		port.closePort();
	}

}
