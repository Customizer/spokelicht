package de.localtoast.spokelicht.arduinoconnection;

import de.localtoast.spokelicht.utils.ExtendedBitSet;
import jssc.SerialPort;
import org.junit.Test;

import static de.localtoast.spokelicht.utils.ByteHelper.b2s;

public class StoreDataWithProtocol extends AbstractSerialTest {
	@Test
	public void store() throws Exception {
		ExtendedBitSet bitSet = new ExtendedBitSet();
		int bytes = 33;
		int bits = bytes * 8; // 3584;// 3840;
		for (int i = 0; i < bits; i += 7) {
			bitSet.set(i);
		}
		bitSet.set(bits - 1);

		DataSender sender = new DataSender();
		final SerialPort port = sender.openPort();

		Thread.sleep(3000);
		// flush receiver...
		port.readBytes();
		System.out.println("Request write...");

		byte[] command = {'w', (byte) (bytes >> 8), (byte) bytes, '$'};
		System.out.println("b1: " + Integer.toHexString(command[1]));
		System.out.println("b2: " + Integer.toHexString(command[2]));
		port.writeBytes(command);
		// wait for the answer
		byte[] ack = port.readBytes(2, 500);
		if (ack[0] != 'o') {
			throw new IllegalStateException("Expected OK, but got: "
							+ (char) ack[0] + (char) ack[1]);
		}

		byte[] dataToSend = bitSet.toByteArray();
		int chunkSize = 32;
		byte[] sendBuffer = new byte[chunkSize];
		byte chksum = 0;
		for (int i = 0; i < dataToSend.length; i++) {
			if (i >= chunkSize && i % chunkSize == 0) {
				byte chkrepeat = writeBytes(port, sendBuffer);
				if (chkrepeat != chksum) {
					System.out.println("Checksum failure at " + (i - 1) + " "
									+ b2s(chksum) + " vs " + b2s(chkrepeat));

				}

				chksum = 0;
				if (i + chunkSize > dataToSend.length) {
					sendBuffer = new byte[dataToSend.length - i];

				}

			}
			// adjust send buffer size for last chunk.
			sendBuffer[i % chunkSize] = dataToSend[i];

			// System.out.print("Adding " + b2s(byteArray[i]) + " to checksum "
			// + b2s(chksum));
			chksum ^= dataToSend[i];
			// System.out.println(" result " + b2s(chksum));

		}
		System.out.println("Raus mit dem Rest!!!");
		byte chkrepeat = writeBytes(port, sendBuffer);
		if (chkrepeat != chksum) {
			System.out.println("Checksum failure at last chunk " + b2s(chksum)
							+ " vs " + b2s(chkrepeat));

		}
		System.out.println();
		System.out.println("done sending.");
	}

}
