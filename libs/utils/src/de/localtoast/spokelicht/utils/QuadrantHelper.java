/*
 *  Spokelicht GUI for the project Spokelicht
 *  Copyright (C) 2014 Arne Augenstein
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.localtoast.spokelicht.utils;

public class QuadrantHelper {
	/**
	 * Determines, in which quadrant the passed coordinates are located.
	 *
	 * @return One of the four quadrants of a 2-dimensional coordinate system
	 */
	public static int getQuadrant(int x, int y) {
		if (x >= 0 && y >= 0) {
			return 1;
		} else if (x >= 0 && y < 0) {
			return 2;
		} else if (x < 0 && y < 0) {
			return 3;
		} else {
			return 4;
		}
	}

	/**
	 * Determines, in which quadrant we are, if we rotate the y-axes clockwise
	 * in a 2-dimensional coordinate system
	 *
	 * @return One of the four quadrants of a 2-dimensional coordinate system
	 */
	public static int getQuadrant(double angle) {
		if (angle < 0 || angle > 360) {
			throw new IllegalArgumentException("This angle is not allowed!");
		}
		if (0 <= angle && angle <= 90) {
			return 1;
		} else if (90 <= angle && angle < 180) {
			return 2;
		} else if (180 <= angle && angle < 270) {
			return 3;
		} else {
			return 4;
		}
	}
}
