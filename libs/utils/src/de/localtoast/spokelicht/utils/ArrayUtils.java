package de.localtoast.spokelicht.utils;

public class ArrayUtils {
	// /**
	// * Concats the arrays.
	// *
	// * @param t
	// * some arrays
	// * @return an array containing all the elements of all ts.
	// */
	// public static <T> T[] concatArrays(T[] ... t) {
	// int totalLength = t.length;
	// for (T[] ts : t) {
	// totalLength += ts.length;
	// }
	// final T[] result = (T[]) java.lang.reflect.Array.newInstance(t[0]
	// .getClass().getComponentType(), totalLength);
	// int idx = 0;
	// for (T[] ts : t) {
	// System.arraycopy(ts, 0, result, idx, ts.length);
	// idx += ts.length;
	// }
	// return result;
	// }

	/**
	 * Concats the byte arrays.
	 *
	 * @param t some arrays
	 * @return an array containing all the elements of all ts.
	 */
	public static byte[] concatArrays(byte[]... t) {
		int totalLength = 0;
		for (byte[] ts : t) {
			totalLength += ts.length;
		}
		final byte[] result = (byte[]) java.lang.reflect.Array.newInstance(t[0]
						.getClass().getComponentType(), totalLength);
		int idx = 0;
		for (byte[] ts : t) {
			System.arraycopy(ts, 0, result, idx, ts.length);
			idx += ts.length;
		}
		return result;
	}

}
