package de.localtoast.spokelicht.utils;

public class ByteHelper {

	public static String b2s(byte b) {
		String s = Integer.toBinaryString(b);
		s = "00000000" + s;
		if (s.length() > 8) {

			return s.substring(s.length() - 8);
		} else {
			return s;
		}
	}

	public static String b2h(byte b) {
		String s = Integer.toHexString(b);
		s = "00000000" + s;

		return "0x" + s.substring(s.length() - 2);

	}

}
