/*
 *  Spokelicht GUI for the project Spokelicht
 *  Copyright (C) 2014 Arne Augenstein
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.localtoast.spokelicht.utils;

import java.util.BitSet;

/**
 * Extension of Java's BitSet class. We need a BitSet, which saves the length,
 * independently of the value. Java's BitSet's length-method always returns the
 * index of the highest set bit plus one. This class also has some
 * BitSet-Operation methods, Java's BitSet does not provide.
 *
 * @author Arne Augenstein
 */
public class ExtendedBitSet {

	private BitSet bitSet;
	private int length = 0;

	public ExtendedBitSet() {
		bitSet = new BitSet();
	}

	/**
	 * This methods concatenates two BitSets
	 *
	 * @param bitSet2
	 */
	public void add(ExtendedBitSet bitSet2) {
		int length = length();
		for (int i = 0; i < bitSet2.length(); i++) {
			if (bitSet2.get(i)) {
				set(length + i);
			} else {
				clear(length + i);
			}
		}
	}

	public boolean get(int i) {
		if (i >= length()) {
			throw new IllegalArgumentException(
							"Index too high! Length of set: " + length() + ", index: "
											+ i);
		}
		return bitSet.get(i);
	}

	public void set(int i) {
		bitSet.set(i);
		if (i >= length) {
			length = i + 1;
		}
	}

	public void clear(int i) {
		bitSet.clear(i);
		if (i >= length) {
			length = i + 1;
		}
	}

	public int length() {
		return length;
	}

	/**
	 * Converts the value to a bit set and adds the result to the current bit
	 * set. The integer is not converted as a whole, but only the lowest bits
	 * are taken. How many bits are taken, is defined by numberOfBits.
	 */
	public void add(int value, int numberOfBits) {
		ExtendedBitSet set = new ExtendedBitSet();
		int index = 0;
		while (index < numberOfBits) {
			if (value % 2 != 0) {
				set.set(index);
			} else {
				set.clear(index);
			}
			index++;
			value = value >>> 1;
		}

		add(set);
	}

	/**
	 * Converts the values to bit sets and adds them to the current bit set.
	 *
	 * @param bytes
	 */
	public void add(byte[] bytes) {
		for (byte value : bytes) {
			add(value);
		}
	}

	private void add(byte value) {
		ExtendedBitSet set = new ExtendedBitSet();
		/*
		 * We have to use a trick here, because valueOf(byte[]) seems to cut off
		 * trailing zeroes.
		 */
		BitSet tempSet = BitSet.valueOf(new byte[]{value});
		int index = 0;
		while (index < 8) {
			if (tempSet.get(index)) {
				set.set(index);
			} else {
				set.clear(index);
			}
			index++;
		}

		add(set);
	}

	/**
	 * Attention! If the length of the BitSet is not divisible by 8, the rest of
	 * the bits of the array's last byte are filled with zeroes!
	 *
	 * @return
	 */
	public byte[] toByteArray() {
		byte[] byteArray = bitSet.toByteArray();

		// Java's BitSet class truncates empty bytes at the end of the BitSet
		int desiredLength = length() / 8;
		if (length() % 8 > 0) {
			desiredLength++;
		}

		if (byteArray.length < desiredLength) {
			byte[] biggerArray = new byte[desiredLength];
			for (int i = 0; i < byteArray.length; i++) {
				biggerArray[i] = byteArray[i];
			}
			return biggerArray;
		}
		return byteArray;
	}

	public boolean equals(ExtendedBitSet other) {
		if (length() != other.length()) {
			return false;
		}
		for (int i = 0; i < length(); i++) {
			if (get(i) != other.get(i)) {
				System.out.println(toString());
				System.out.println(other.toString());
				return false;
			}
		}

		return true;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < length(); i++) {
			if (get(i)) {
				buffer.append("1");
			} else {
				buffer.append("0");
			}
		}
		return buffer.toString();
	}
}
