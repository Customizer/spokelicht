/*
 *  Spokelicht GUI for the project Spokelicht
 *  Copyright (C) 2014 Arne Augenstein
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.localtoast.spokelicht.utils;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class Line {
	private List<Color> pixels = new ArrayList<>();

	public void addPixel(Color pixel) {
		pixels.add(pixel);
	}

	public Color getPixel(int index) {
		return pixels.get(index);
	}

	public int getPixelCount() {
		return pixels.size();
	}

}
