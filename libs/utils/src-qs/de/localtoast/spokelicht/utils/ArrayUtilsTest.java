package de.localtoast.spokelicht.utils;

import org.junit.Assert;
import org.junit.Test;

public class ArrayUtilsTest {
	@Test
	public void testTwoPrimitiveArrays() {
		byte[] erg = ArrayUtils.concatArrays(new byte[]{0x1, 0x2},
						new byte[]{0x3, 0x4});
		Assert.assertArrayEquals(new byte[]{0x1, 0x2, 0x3, 0x4}, erg);
	}

	@Test
	public void testThreePrimitiveArrays() {
		byte[] erg = ArrayUtils.concatArrays(//
						new byte[]{0x1, 0x2},//
						new byte[]{0x3, 0x4},//
						new byte[]{0x5, 0x6});
		Assert.assertArrayEquals(new byte[]{0x1, 0x2, 0x3, 0x4, 0x5, 0x6},
						erg);
	}

	@Test
	public void testTwoEmptyArrays() {
		byte[] erg = ArrayUtils.concatArrays(new byte[]{}, new byte[]{});
		Assert.assertArrayEquals(new byte[]{}, erg);
	}

	@Test
	public void testOneArray() {
		byte[] erg = ArrayUtils.concatArrays(new byte[]{}, new byte[]{0x1,
						0x2});
		Assert.assertArrayEquals(new byte[]{0x1, 0x2}, erg);
	}

}
