package de.localtoast.spokelicht.utils;

import org.junit.Assert;
import org.junit.Test;

public class ExtendedBitSetTest {

	@Test
	public void concat() {
		ExtendedBitSet bitSet = new ExtendedBitSet();
		bitSet.set(2);
		bitSet.set(3);
		bitSet.set(5);

		ExtendedBitSet bitSet2 = new ExtendedBitSet();
		bitSet2.set(1);
		bitSet2.set(3);

		bitSet.add(bitSet2);

		Assert.assertEquals(false, bitSet.get(0));
		Assert.assertEquals(false, bitSet.get(1));
		Assert.assertEquals(true, bitSet.get(2));
		Assert.assertEquals(true, bitSet.get(3));
		Assert.assertEquals(false, bitSet.get(4));
		Assert.assertEquals(true, bitSet.get(5));
		Assert.assertEquals(false, bitSet.get(6));
		Assert.assertEquals(true, bitSet.get(7));
		Assert.assertEquals(false, bitSet.get(8));
		Assert.assertEquals(true, bitSet.get(9));
		try {
			bitSet.get(10);
			Assert.fail("The index should be too high.");
		} catch (IllegalArgumentException e) {
			// Expected behavior
		}

	}

	@Test
	public void addInteger() {
		ExtendedBitSet set = new ExtendedBitSet();
		set.add(5, 3);

		Assert.assertTrue(set.get(0));
		Assert.assertFalse(set.get(1));
		Assert.assertTrue(set.get(2));
		try {
			set.get(3);
			Assert.fail("The index should be too high.");
		} catch (IllegalArgumentException e) {
			// Expected behavior
		}

		set.add(2, 3);

		Assert.assertTrue(set.get(0));
		Assert.assertFalse(set.get(1));
		Assert.assertTrue(set.get(2));
		Assert.assertFalse(set.get(3));
		Assert.assertTrue(set.get(4));
		Assert.assertFalse(set.get(5));

		try {
			set.get(6);
			Assert.fail("The index should be too high.");
		} catch (IllegalArgumentException e) {
			// Expected behavior
		}

	}

}
