package de.localtoast.spokelicht.animations;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementMap;

import java.util.HashMap;
import java.util.Map;

@Element
public class Animation {
	@Element
	private int xMove = 0;
	@Element
	private int yMove = 0;
	@Element
	private double xScale = 1;
	@Element
	private double yScale = 1;

	// Mapping frame index to FrameSettings
	@ElementMap
	Map<Integer, Frame> frameSettings = new HashMap<>();

	@Element
	private int rotationInDegrees = 0;

	@Element
	private String imageFileName;

	public Animation() {
		// empty constructor for deserialization
	}

	public Animation(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public int getxMove() {
		return xMove;
	}

	public void setxMove(int xMove) {
		this.xMove = xMove;
	}

	public int getyMove() {
		return yMove;
	}

	public void setyMove(int yMove) {
		this.yMove = yMove;
	}

	public double getxScale() {
		return xScale;
	}

	public void setxScale(double xScale) {
		this.xScale = xScale;
	}

	public double getyScale() {
		return yScale;
	}

	public void setyScale(double yScale) {
		this.yScale = yScale;
	}

	public int getRotationInDegrees() {
		return rotationInDegrees;
	}

	public void setRotationInDegrees(int rotationInDegrees) {
		this.rotationInDegrees = rotationInDegrees;
	}

	public Map<Integer, Frame> getFrameSettings() {
		return frameSettings;
	}

	public void setFrameSettings(Map<Integer, Frame> frameSettings) {
		this.frameSettings = frameSettings;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}
}
