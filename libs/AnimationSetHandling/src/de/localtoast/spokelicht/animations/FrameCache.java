package de.localtoast.spokelicht.animations;

import de.localtoast.spokelicht.imagemanipulation.ImageHelper;
import javafx.scene.image.WritableImage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This cache is implemented as a singleton, hence the enum-type and the INSTANCE-Field
 * Created by arne on 8/6/15.
 */
public enum FrameCache {
	INSTANCE;

	/**
	 * Stores every frame of every animation. The key of the map is the animation index
	 */
	// TODO make map with animation instead of index
	private Map<Integer, List<WritableImage>> originalFrames = new HashMap<>();

	void addAnimation(int i, List<File> files) throws IOException {
		List<WritableImage> writableImages = ImageHelper.loadImages(files);
		originalFrames.put(i, writableImages);
	}

	List<WritableImage> getOriginalFrames(int animationIndex) {
		List<WritableImage> frames = originalFrames.get(animationIndex);
		if (frames == null) {
			return new ArrayList<>();
		}
		return frames;
	}


}
