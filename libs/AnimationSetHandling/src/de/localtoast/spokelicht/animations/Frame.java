package de.localtoast.spokelicht.animations;

import javafx.scene.paint.Color;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementMap;

import java.util.HashMap;
import java.util.Map;

@Element
public class Frame {

	@ElementMap
	private Map<String, String> replacementColors = new HashMap<>();

	public Map<String, String> getReplacementColors() {
		return replacementColors;
	}

	public void setReplacementColors(Map<String, String> replacementColors) {
		this.replacementColors = replacementColors;
	}

	public void setReplacementColor(Color original, Color replacement) {
		String originalHex = toWebHexCode(original);
		String replacementHex = toWebHexCode(replacement);

		for (String key : replacementColors.keySet()) {
			String temp = replacementColors.get(key);
			if (temp != null && temp.equals(originalHex)) {
		/*
		 * If the original color is the result of a replacement itself, switch only the replacement
		 * color
		 */
				replacementColors.put(key, replacementHex);
				return;
			}
		}

		replacementColors.put(toWebHexCode(original), toWebHexCode(replacement));
	}

	private String toWebHexCode(Color color) {
		return String.format("#%02X%02X%02X",
						(int) (color.getRed() * 255),
						(int) (color.getGreen() * 255),
						(int) (color.getBlue() * 255));
	}
}
