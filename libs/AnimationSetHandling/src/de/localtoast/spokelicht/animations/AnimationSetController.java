package de.localtoast.spokelicht.animations;

import de.localtoast.spokelicht.LineCreator;
import de.localtoast.spokelicht.arduinoconnection.Memory;
import de.localtoast.spokelicht.arduinoconnection.Picture;
import de.localtoast.spokelicht.imagemanipulation.ImageManipulationHelper;
import de.localtoast.spokelicht.utils.Line;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import org.apache.commons.io.FilenameUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Every action, which can be performed on an animation set or animation is handled through this
 * class.
 */
public class AnimationSetController {
	private static final String FILE_EXTENSION_SXML_WITHOUT_EXTENSION = "sxml";
	public static final String FILE_EXTENSION_SXML = "." + FILE_EXTENSION_SXML_WITHOUT_EXTENSION;

	private AnimationSet animationSet;
	private File animationSetFileName;

	private File createProperFilename(File destinationFile) throws IOException {
		if (!FILE_EXTENSION_SXML_WITHOUT_EXTENSION.equals(FilenameUtils.getExtension
						(destinationFile.getCanonicalPath()))) {
			destinationFile = new File(FilenameUtils.removeExtension(destinationFile.getCanonicalPath
							()) + FILE_EXTENSION_SXML);
		}
		return destinationFile;
	}

	public void saveAnimationSet() {
		saveAnimationSetAs(animationSetFileName);
	}

	public void saveAnimationSetAs(File destinationFile) {
		try {
			animationSetFileName = createProperFilename(destinationFile);
			createImagePath();

			Serializer serializer = new Persister();
			serializer.write(animationSet, animationSetFileName);
		} catch (Exception e) {
			// TODO add proper error handling
			e.printStackTrace();
		}
	}

	private void createImagePath() {
		Path imagePath = getImageDirectory();
		File imagePathFile = imagePath.toFile();
		if (!imagePathFile.exists()) {
			imagePathFile.mkdir();
		}
	}

	private Path getImageDirectory() {
		String imagePathName = FilenameUtils.getFullPath(animationSetFileName.getAbsolutePath());
		return Paths.get(imagePathName, FilenameUtils.getBaseName(
						animationSetFileName.getAbsolutePath()));
	}

	public void createNewAnimationSet(File destinationFile) {
		animationSet = new AnimationSet();
		saveAnimationSetAs(destinationFile);
	}

	public AnimationSet loadAnimationSet(File file) {
		Serializer serializer = new Persister();
		try {
			animationSet = serializer.read(AnimationSet.class, file);
			animationSetFileName = file;

			List<Animation> animations = animationSet.getAnimations();
			for (int i = 0; i < animations.size(); i++) {
				FrameCache.INSTANCE.addAnimation(i, getImageFiles(i));
			}

			return animationSet;
		} catch (Exception e) {
			throw new IllegalArgumentException("Animation set could not be loaded.", e);
		}
	}

	public Animation getAnimation(int index) {
		return animationSet.getAnimations().get(index);
	}

	public int addAnimation(File imageFile) throws IOException {
		String imageName = FilenameUtils.getName(imageFile.getAbsolutePath());
		File destinationFile = new File(FilenameUtils.concat(getImageDirectory().toString(),
						imageName));
		Files.copy(imageFile.toPath(), destinationFile.toPath());
		Animation animation = new Animation(imageName);
		int animationIndex = animationSet.addAnimation(animation);
		FrameCache.INSTANCE.addAnimation(animationIndex, getImageFiles(animationIndex));
		return animationIndex;
	}

	public File getAnimationSetFileName() {
		return animationSetFileName;
	}

	public File getFullImagePath(int animationIndex) {
		return new File(FilenameUtils.concat(getImageDirectory().toString(), getAnimation
						(animationIndex)
						.getImageFileName()));
	}

	private List<File> getImageFiles(int animationIndex) throws IOException {
		String imageFileName = animationSet.getAnimation(animationIndex).getImageFileName();
		String imageFilePath = FilenameUtils.concat(getImageDirectory().toString(), imageFileName);

		File imageFile = new File(imageFilePath);
		List<File> imageFiles = new ArrayList<>();

		// Get all image files of a sequence
		String fileName = FilenameUtils.getBaseName(imageFile.getCanonicalPath());
		if (!"".equals(fileName)) {
			int lastCharacterIndex = -1;
			for (int i = fileName.length() - 1; i >= 0; i--) {
				if (fileName.substring(i, i + 1).matches("[0-9]")) {
					lastCharacterIndex = i;
				} else {
					break;
				}
			}
			if (lastCharacterIndex >= 0) {
				int padding = fileName.length() - lastCharacterIndex;
				String paddingFormatString = "%0" + padding + "d";
				double maxIndex = Math.pow(10, padding);
				for (int i = 0; i < maxIndex; i++) {
					String newFileName = constructNumberedFileName(imageFile, fileName, lastCharacterIndex,
									paddingFormatString, i);
					File newFile = new File(newFileName);
					if (newFile.exists()) {
						imageFiles.add(newFile);

						if (i == maxIndex - 1) {
							/*
							 * If there are no leading zeroes, we have to adjust the pattern to be able to fetch
							 * the next number.
							 */
							padding++;
							maxIndex = Math.pow(10, padding);
						}
					} else {
						if (i == 0) {
							// search beginning index
							while (i < maxIndex && !newFile.exists()) {
								i++;
								newFile = new File(constructNumberedFileName(imageFile, fileName,
												lastCharacterIndex, paddingFormatString, i));
							}
							if (newFile.exists()) {
								imageFiles.add(newFile);
							} else {
								break;
							}
						}
					}
				}
			} else {
				imageFiles.add(imageFile);
			}
		}
		return imageFiles;
	}


	private String constructNumberedFileName(File imageFile, String fileName, int
					lastCharacterIndex, String paddingFormatString, int i) {
		try {
			return FilenameUtils.getFullPath(imageFile.getCanonicalPath()) + fileName
							.substring(0, lastCharacterIndex) + String.format(paddingFormatString, i)
							+ "." + FilenameUtils.getExtension(imageFile.getCanonicalPath());
		} catch (IOException e) {
			// TODO add proper error handling
		}
		return "";
	}

	public List<WritableImage> getOriginalFrames(int animationIndex) {
		return FrameCache.INSTANCE.getOriginalFrames(animationIndex);
	}

	public WritableImage getManipulatedFrame(int animationIndex, int frameIndex) {
		List<WritableImage> frames = FrameCache.INSTANCE.getOriginalFrames(animationIndex);

		if (frameIndex >= frames.size()) {
			throw new IllegalArgumentException("Frame index too high");
		}

		Animation animation = animationSet.getAnimation(animationIndex);
		WritableImage originalImage = frames.get(frameIndex);

		Map<String, String> replacementColors = new HashMap<>();
		Frame frame = animation.getFrameSettings().get(frameIndex);
		if (frame != null) {
			replacementColors = frame.getReplacementColors();
		}
		ImageManipulationHelper helper = new ImageManipulationHelper(
						SwingFXUtils.fromFXImage(originalImage, null),
						animation.getxMove(),
						animation.getyMove(),
						animation.getxScale(),
						animation.getyScale(),
						animation.getRotationInDegrees(),
						(int) originalImage.getWidth(),
						(int) originalImage.getHeight(),
						replacementColors);

		return SwingFXUtils.toFXImage(helper.runManipulations(), null);
	}

	public Memory getMemoryObject() {
		List<de.localtoast.spokelicht.arduinoconnection.Animation> memAnimations = new ArrayList<>();
		for (int animIndex = 0; animIndex < animationSet.getAnimations().size(); animIndex++) {
			List<Picture> pictures = new ArrayList<>();
			for (int frameIndex = 0; frameIndex < getOriginalFrames(animIndex).size(); frameIndex++) {
				WritableImage manipulatedFrame = getManipulatedFrame(animIndex, frameIndex);
				List<Line> lines = new LineCreator(manipulatedFrame).createLines();
				// TODO make duration configurable
				pictures.add(new Picture(lines, 50));
			}
			memAnimations.add(new de.localtoast.spokelicht.arduinoconnection.Animation(pictures));
		}

		return new Memory(memAnimations);
	}

	public void removeAnimation(int animationIndex) {
		animationSet.getAnimations().remove(animationIndex);
	}
}