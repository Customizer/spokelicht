package de.localtoast.spokelicht.animations;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a set of animations, which can be transferred to the
 * Arduino. These animations can be saved to an XML file, which also includes the
 * modifications of these images.
 */
@Root
public class AnimationSet {

	@ElementList
	private List<Animation> animations;

	AnimationSet() {
		animations = new ArrayList<>();
	}

	/**
	 * Adds a new animation to the set
	 *
	 * @return The index of the newly added animation
	 */
	public int addAnimation(Animation animation) {
		animations.add(animation);
		return animations.size() - 1;
	}

	// TODO method for removing animation

	public void setAnimations(List<Animation> animations) {
		this.animations = animations;
	}

	public Animation getAnimation(int animationIndex) {
		return animations.get(animationIndex);
	}

	public List<Animation> getAnimations() {
		return animations;
	}
}
