/*
 *  Spokelicht GUI for the project Spokelicht
 *  Copyright (C) 2014 Arne Augenstein
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.localtoast.spokelicht;

import org.junit.Assert;
import org.junit.Test;

public class PixelPositionCalculatorTest {

	@Test
	public void testGetETest() {
		String message = "getETest liefert falschen Wert.";

		Assert.assertEquals(message, getETest(1, 1), 1.414, 0.001);
		Assert.assertEquals(message, getETest(3, 2), 3.606, 0.001);
		Assert.assertEquals(message, getETest(5, 1), 5.099, 0.001);

		// assertEquals(message, getETest(3, 1), 5.099, 0.001);
	}

	private double getETest(int x, int y) {
		return new PixelPositionCalculator(x, y).getETest();
	}

	@Test
	public void testGetAlphaTest() {
		try {
			String message = "getAlphaTest liefert falschen Wert.";

			Assert.assertEquals(message, getAlphaTest(0, 3), 0, 0);
			Assert.assertEquals(message, getAlphaTest(4, 4), 45, 0);
			Assert.assertEquals(message, getAlphaTest(5, 0), 90, 0);
			Assert.assertEquals(message, getAlphaTest(3, -3), 135, 0);
			Assert.assertEquals(message, getAlphaTest(0, -3), 180, 0);
			Assert.assertEquals(message, getAlphaTest(-2, -2), 225, 0);
			Assert.assertEquals(message, getAlphaTest(-4, 0), 270, 0);
			Assert.assertEquals(message, getAlphaTest(-3, 3), 315, 0);

			Assert.assertEquals(message, getAlphaTest(4, 3), 53.1301, 0.001);
			Assert.assertEquals(message, getAlphaTest(2, -3), 146.3099, 0.001);
			Assert.assertEquals(message, getAlphaTest(-2, -80), 181.4321, 0.001);
			Assert.assertEquals(message, getAlphaTest(-45, 32), 305.4171, 0.001);

		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}

	private double getAlphaTest(int x, int y) throws Exception {
		return new PixelPositionCalculator(x, y).getAlphaTest();
	}

}
