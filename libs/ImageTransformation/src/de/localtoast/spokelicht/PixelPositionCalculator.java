/*
 *  Spokelicht GUI for the project Spokelicht
 *  Copyright (C) 2014 Arne Augenstein
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.localtoast.spokelicht;

import de.localtoast.spokelicht.utils.QuadrantHelper;

public class PixelPositionCalculator {
	private int x;
	private int y;
	private double xAbs;
	private double yAbs;

	public PixelPositionCalculator(int x, int y) {
		if (x == 0 && y == 0) {
			throw new IllegalArgumentException(
							"It is not allowed to set x and y both to zero!");
		}

		this.x = x;
		this.y = y;

		xAbs = Math.abs(x);
		yAbs = Math.abs(y);
	}

	public double getETest() {
		return Math.sqrt(x * x + y * y);
	}

	public double getAlphaTest() throws Exception {

		switch (QuadrantHelper.getQuadrant(x, y)) {
			case 1:
				if (yAbs == 0) {
					return 90;
				}
				return Math.toDegrees(Math.atan(xAbs / yAbs));
			case 2:
				if (xAbs == 0) {
					return 180;
				}
				return Math.toDegrees(Math.atan(yAbs / xAbs)) + 90;
			case 3:
				if (yAbs == 0) {
					return 270;
				}
				return Math.toDegrees(Math.atan(xAbs / yAbs)) + 180;
			case 4:
				if (xAbs == 0) {
					return 0;
				}
				return Math.toDegrees(Math.atan(yAbs / xAbs)) + 270;
			default:
				throw new Exception("Error while calculating alpha test.");
		}
	}

}
