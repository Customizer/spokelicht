/*
 *  Spokelicht GUI for the project Spokelicht
 *  Copyright (C) 2014 Arne Augenstein
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.localtoast.spokelicht;

import de.localtoast.spokelicht.utils.QuadrantHelper;
import javafx.scene.image.Image;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

public class PixelFetcher {
	private PixelReader pixelReader;
	private double width;
	private double height;

	public PixelFetcher(Image image) {
		this.pixelReader = image.getPixelReader();
		width = image.getWidth();
		height = image.getHeight();
	}

	public Color getPixel(double eMin, double eMax, double alphaMin) {

		double eMean = eMin + ((eMax - eMin) / 2);
		double alpha = alphaMin;

		int quadrant = QuadrantHelper.getQuadrant(alpha);

		/*
		 * For simplicity's sake, we only want to use one trigonometric formula
		 * for each quadrant. In that case, we have to use the correct angle,
		 * depending on the quadrant our line is located in.
		 */
		switch (quadrant) {
			case 1:
				alpha = 90 - alpha;
				break;
			case 2:
				alpha = alpha - 90;
				break;
			case 3:
				alpha = 270 - alpha;
				break;
			case 4:
				alpha = alpha - 270;
				break;
			default:
				break;
		}

		/*
		 * Calculating the position of the pixel, we want to use for current
		 * LED.
		 */
		alpha = Math.toRadians(alpha);

		double xPos, yPos;
		xPos = eMean * Math.cos(alpha);
		yPos = eMean * Math.sin(alpha);

		/*
		 * Set the correct sign.
		 */
		switch (quadrant) {
			case 1:
				// values are already positive
				break;
			case 2:
				yPos = -yPos;
				break;
			case 3:
				xPos = -xPos;
				yPos = -yPos;
				break;
			case 4:
				xPos = -xPos;
				break;
			default:
				break;
		}

		double halfWidth = width / 2;
		double halfHeight = height / 2;
		Color pixel = pixelReader.getColor(
						(int) (Math.round(xPos + halfWidth)),
						(int) (Math.round(yPos + halfHeight)));

		return pixel;
	}
}
