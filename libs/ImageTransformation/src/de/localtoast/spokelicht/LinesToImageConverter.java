package de.localtoast.spokelicht;

import de.localtoast.spokelicht.utils.Line;
import de.localtoast.spokelicht.utils.QuadrantHelper;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.List;

public class LinesToImageConverter {
	public static final int TARGET_WIDTH_AND_HEIGHT = 800;

	public void convert(List<Line> lines, Canvas canvas, int ledCount) {
		GraphicsContext graphicsContext = canvas.getGraphicsContext2D();
		graphicsContext.setFill(Color.BLACK);
		graphicsContext.fillRect(0, 0, TARGET_WIDTH_AND_HEIGHT,
						TARGET_WIDTH_AND_HEIGHT);

		double angleAlpha = (double) 360 / lines.size();
		double relationOuterInner = (double) LineCreator.OUTER_RADIUS / LineCreator.INNER_RADIUS;

		long innerPixelRadius = Math
						.round(((double) TARGET_WIDTH_AND_HEIGHT / 2)
										/ relationOuterInner);

		long pixelPerLine = Math.round((double) TARGET_WIDTH_AND_HEIGHT / 2)
						- innerPixelRadius;
		long pixelOffsetPerLed = Math.round(pixelPerLine / ledCount);

		for (int i = 0; i < lines.size(); i++) {
			Line line = lines.get(i);
			double angle = i * angleAlpha;

			for (int j = 0; j < ledCount; j++) {
				int x, y;
				long hypotenuse = innerPixelRadius + j * pixelOffsetPerLed;

				int x2, y2;
				int offset = (int) Math
								.round((double) TARGET_WIDTH_AND_HEIGHT / 2);

				switch (QuadrantHelper.getQuadrant(angle)) {
					case 1:
						x = (int) Math.round(Math.sin(Math.toRadians(angle))
										* hypotenuse);
						y = (int) Math.round(Math.cos(Math.toRadians(angle))
										* hypotenuse);
						x2 = x + offset;
						y2 = y + offset;
						break;
					case 2:
						x = (int) Math.round(Math.cos(Math.toRadians(angle - 90))
										* hypotenuse);
						y = (int) Math.round(Math.sin(Math.toRadians(angle - 90))
										* hypotenuse);
						x2 = x + offset;
						y2 = -y + offset;
						break;
					case 3:
						x = (int) Math.round(Math.sin(Math.toRadians(angle - 180))
										* hypotenuse);
						y = (int) Math.round(Math.cos(Math.toRadians(angle - 180))
										* hypotenuse);
						x2 = -x + offset;
						y2 = -y + offset;
						break;
					case 4:
						x = (int) Math.round(Math.cos(Math.toRadians(angle - 270))
										* hypotenuse);
						y = (int) Math.round(Math.sin(Math.toRadians(angle - 270))
										* hypotenuse);
						x2 = -x + offset;
						y2 = y + offset;
						break;
					default:
						// Don't paint the image, something important went wrong
						return;
				}

				paintLed(graphicsContext, line, j, x2, y2);
			}
		}
	}

	private void paintLed(GraphicsContext graphicsContext, Line line, int led,
												int x, int y) {
		int ledPixelSize = 5;

		graphicsContext.setFill(line.getPixel(led));
		graphicsContext.fillOval(x, y, ledPixelSize, ledPixelSize);
	}
}
