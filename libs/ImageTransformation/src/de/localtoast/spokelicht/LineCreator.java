/*
 *  Spokelicht GUI for the project Spokelicht
 *  Copyright (C) 2014 Arne Augenstein
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.localtoast.spokelicht;

import de.localtoast.spokelicht.utils.Line;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class LineCreator {
	/**
	 * Number of LEDs on the board
	 */
	public static final int LEDS = 20;
	private static final int LINES = 200;
	private Image image;

	// TODO make these values configurable
	public static int OUTER_RADIUS = 30;
	public static int INNER_RADIUS = 5;

	public LineCreator(Image image) {
		this.image = image;

		if (image.getWidth() != image.getHeight()) {
			throw new IllegalArgumentException(
							"The image width and height have to be equal!");
		}
	}

	public List<Line> createLines() {
		List<Line> lines = new ArrayList<>();
		for (int line = 0; line < LINES; line++) {
			lines.add(createLine(line));
		}
		return lines;
	}

	private Line createLine(int lineNr) {
		Line line = new Line();
		for (int ledNr = 0; ledNr < LEDS; ledNr++) {
			addPixelToLine(line, lineNr, ledNr);
		}

		return line;
	}

	private void addPixelToLine(Line line, int lineNr, int ledNr) {
		double eMin;
		double eMax;
		double alphaMin;

		double pixelPerCm = image.getWidth() / (OUTER_RADIUS * 2);

		eMin = (INNER_RADIUS + ledNr * ((OUTER_RADIUS - INNER_RADIUS) / LEDS))
						* pixelPerCm;
		eMax = (INNER_RADIUS + (ledNr + 1)
						* ((OUTER_RADIUS - INNER_RADIUS) / LEDS))
						* pixelPerCm;

		alphaMin = (double) (lineNr * 360) / LINES;

		Color pixel = new PixelFetcher(image).getPixel(eMin, eMax, alphaMin);

		line.addPixel(pixel);
	}
}
