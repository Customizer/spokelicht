/*
 *  Spokelicht GUI for the project Spokelicht
 *  Copyright (C) 2014 Arne Augenstein
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.localtoast.spokelicht.gui;

import de.localtoast.spokelicht.LineCreator;
import de.localtoast.spokelicht.LinesToImageConverter;
import de.localtoast.spokelicht.arduinoconnection.*;
import de.localtoast.spokelicht.imagemanipulation.ImageHelper;
import de.localtoast.spokelicht.javafx.JavaFxJUnit4ClassRunner;
import de.localtoast.spokelicht.utils.Line;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(JavaFxJUnit4ClassRunner.class)
public class ConverterTest {

	public static final String RELATIVE_IMAGE_PATH = "convertable-images/";

	/**
	 * This test converts an image to lines and transforms the lines back to a
	 * new image in the attempt to visualize the result of the transformation.
	 */
	@Test
	public void processImageAndOutputResult() {

		String[] images = {"pacman_r1.png", "pacman_r2.png", "pacman_r3.png",
						"pacman_r4.png", "pacman_r5.png", "pacman_r6.png",
						"pacman_r7.png", "pacman_r8.png", "pacman_r9.png",
						"pacman_r10.png"};
		String[] images1 = {"pacman1.png", "pacman2.png"};
		try {
			Animation a0 = createAnimation(images, 1);
			Animation a1 = createAnimation(images1, 5);
			Animation a2 = createAnimation(
							new String[]{"der_kleine_elefant_16_colors.png"}, 50);
			Animation a3 = createAnimation(new String[]{"testimage2.png"},
							50);
			Animation a4 = createAnimation(new String[]{"testimage3.png"},
							50);
			Animation a5 = createAnimation(new String[]{"ente.png"}, 50);
			// Animation a6 = createAnimation(new String[] { "die_maus.png" },
			// 50);
			// Animation a7 = createAnimation(new String[] { "homer.png" }, 50);
			Animation a8 = createAnimation(new String[]{"cm-logo.png"}, 50);
			Animation a9 = createAnimation(new String[]{"cm-logo2.png"}, 50);
			Animation a10 = createAnimation(new String[]{"schild.png"}, 50);

			Memory mem = new Memory(Arrays.asList(a0, a2, a4, a5, a8, a9, a10));

			DataSender dataSender = new DataSender();
			try {
				dataSender.updateArduinoMemory(mem);
			} catch (SerialConnectionException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	private Animation createAnimation(String[] images, int duration)
					throws IOException, Exception {
		ArrayList<Picture> allPictures = new ArrayList<>();
		for (String imageFilename : images) {
			imageFilename = RELATIVE_IMAGE_PATH + "/" + imageFilename;
			List<File> imageFiles = new ArrayList<>();
			imageFiles.add(new File(imageFilename));
			Picture p = new Picture(createLinesAndImage(ImageHelper.loadImages(imageFiles).get(0),
							imageFilename.substring(0, imageFilename.length() - 4)
											+ "_convert.png"), duration);
			allPictures.add(p);
		}
		return new Animation(allPictures);
	}

	// @Test
	// public void processImageWithTextAndOutputResult() {
	// try {
	// String imageFilename = "testimage_text.png";
	// startTest(imageFilename, "output_text.png");
	// } catch (Exception e) {
	// e.printStackTrace();
	// Assert.fail(e.getMessage());
	// }
	//
	// }

	/**
	 * This test converts an image to lines and transforms the lines back to a
	 * new image in the attempt to visualize the result of the transformation.
	 */
	private List<Line> createLinesAndImage(Image source,
																				 String outputFilename) throws IOException, Exception {
		List<Line> lines = new LineCreator(source)
						.createLines();

		Canvas canvas = new Canvas(
						LinesToImageConverter.TARGET_WIDTH_AND_HEIGHT,
						LinesToImageConverter.TARGET_WIDTH_AND_HEIGHT);
		new LinesToImageConverter().convert(lines, canvas, LineCreator.LEDS);

		ImageIO.write(
						SwingFXUtils.fromFXImage(canvas.snapshot(null, null), null),
						"png", new File(outputFilename));
		return lines;
	}
}
