#include "TLCController.h"

TLCController::TLCController() {
  pictureSource = NULL;
  switchedOff = false;
  setPictureSource(OnlyLogging);
}

TLCController::~TLCController() {
  delete pictureSource;
  pictureSource = NULL;
}

void TLCController::setPictureSource(PictureSourceType type) {
  LOGGER(F("Setting picture source"), DEBUG);

  delete pictureSource;
  pictureSource = NULL;
  LOGGER(F("Old picture source deleted"), DEBUG);

  this->sourceType = type;

  switch(type) {
  case OnlyLogging:
    pictureSource = new OnlyLoggingPictureSource();
    LOGGER(F("Picture source set to OnlyLogging"), DEBUG);
    break;

  case StoredInEEPROM:
    pictureSource = new EEPROMPictureSource();
    LOGGER(F("Picture source set to EEPROM"), DEBUG);
    break;

  case KnightRider:
    // TODO
    LOGGER(F("Picture source set to Knight Rider"), DEBUG);
    break;

  }
}

void TLCController::updateTLCLine() {
  uint8_t offset = 0;

  for (uint8_t lineNr = 0; lineNr < 2; lineNr++) {
    for (uint8_t ledNr = 0; ledNr < settings.numLeds; ledNr++) {
      if (ledNr == 0 && lineNr == 1) {
        // Offset for the mapping between bit-positions and outputs, as not all outputs are used.
        offset = 64;
      }
      if (ledNr == 10) {
        if (lineNr == 1) {
          offset += 2;
        } else {
          // output 30 and 31 are not connected (Board-Split-Area)
          offset = 2;
        }
      }

      if (switchedOff) {
        Tlc.set((ledNr * 3) + offset, 0);
        Tlc.set((ledNr * 3) + 1 + offset, 0);
        Tlc.set((ledNr * 3) + 2 + offset, 0);
      } else {
        Tlc.set((ledNr * 3) + offset, (pictureSource->getBlue(lineNr, ledNr) << settings.brightness));
        Tlc.set((ledNr * 3) + 1 + offset, (pictureSource->getRed(lineNr, ledNr) << settings.brightness));
        Tlc.set((ledNr * 3) + 2 + offset, (pictureSource->getGreen(lineNr, ledNr) << settings.brightness));
      }
    }
  }

  // Asynchronously load line 1, if the pictureSource supports it
  pictureSource->loadLine1NonBlocking();
}

void TLCController::waitForTlcAndLoadNextLines() {
  // when the TLC is not yet ready to be updated 1 is returned. So... Wait!
  while (Tlc.update()) {
  }

  pictureSource->loadNextLines();
}

void TLCController::preLoadData() {
  pictureSource->preLoadData();
}

void TLCController::magnetDetected() {
  pictureSource->magnetDetected();
}

void TLCController::next() {
  pictureSource->next();
}

void TLCController::previous() {
  pictureSource->previous();
}

void TLCController::switchOff(bool value) {
  switchedOff = value;
}
