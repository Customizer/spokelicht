#ifndef SERIALCOMMUNICATOR_H
#define SERIALCOMMUNICATOR_H

#include <avr/wdt.h>
#include <EEPROM.h>
#include <EEPROMNB.h>
#include "SerialInterface.h"
#include "Settings.h"
#include "TLCController.h"
#include "Logger.h"


extern Settings settings;
extern SerialInterface serial;
extern TLCController tlcController;

/**
 * This class handles the serial communication with the PC or
 * the Android Smartphone.
 */
class SerialCommunicator {
public:
  SerialCommunicator();
  ~SerialCommunicator();

  void serialDataAvailable();

private:
  // some error codes for the bluetooth communication
  struct ErrorCode {
    // the payload did not contain the required amount of data
    uint8_t ERR_NOT_ENOUGH_DATA;

    // the payload's length exceeded the required amount of data
    uint8_t ERR_TOO_MUCH_DATA;

    // a number within the payload was too big
    uint8_t ERR_NUMBER_TOO_BIG;

    // the requested command is not supported.
    uint8_t ERR_UNSUPPORTED_COMMAND;

    // the cheksum of the payload was wrong.
    uint8_t ERR_CHECKSUM_FAIL;

    // the received data did not arrive within the timeout.
    uint8_t ERR_TIMEOUT;
  } errorCode;


  EEPROMInterface* eeprom;

  // the command last received
  uint8_t serialCommand;

  // the serial command's payload
  uint8_t serialCommandBufferSize;
  uint8_t* serialCommandBuffer;

  // pointer for that buffer
  uint8_t serialCommandBufferLength;


  void writeDataToEEPROM();

  void declineSerialRequest(uint8_t b);

  void waitForData(int ms);

  void calibrateZeroLine();

  void resetArduino();
  void brighter();
  void darker();

  static void noop();

};

#endif // SERIALCOMMUNICATOR_H
