#ifndef ONLYLOGGINGPICTURESOURCE_H
#define ONLYLOGGINGPICTURESOURCE_H

#include "PictureSource.h"
#include "Settings.h"

extern Settings settings;
#ifdef LOGGING
extern Logger logger;
#endif //LOGGING

/**
 * This class does not display any picture, but is only used
 * for logging purposes.
 */
class OnlyLoggingPictureSource : public PictureSource {
public:
  OnlyLoggingPictureSource();
  virtual ~OnlyLoggingPictureSource();

  /* virtual */ void preLoadData();

  /* virtual */ void loadNextLines();

  /* virtual */ void loadLine1NonBlocking();

  /* virtual */ uint8_t getRed(uint8_t lineNr, uint8_t ledNr);

  /* virtual */ uint8_t getGreen(uint8_t lineNr, uint8_t ledNr);

  /* virtual */ uint8_t getBlue(uint8_t lineNr, uint8_t ledNr);

  /* virtual */ void magnetDetected();

  /* virtual */ void next();

  /* virtual */ void previous();

};

#endif // ONLYLOGGINGPICTURESOURCE_H
