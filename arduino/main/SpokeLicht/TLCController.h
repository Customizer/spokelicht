#ifndef TLCCONTROLLER_H
#define TLCCONTROLLER_H

#include "inttypes.h"
#include "Tlc5940.h"
#include "PictureSource.h"
#include "EEPROMPictureSource.h"
#include "OnlyLoggingPictureSource.h"
#include "Logger.h"

#ifdef LOGGING
extern Logger logger;
#endif //LOGGING

class TLCController {
public:
  enum PictureSourceType {
    OnlyLogging, StoredInEEPROM, KnightRider
  };

  TLCController();
  ~TLCController();

  void setPictureSource(PictureSourceType type);

  void updateTLCLine();

  void waitForTlcAndLoadNextLines();

  void preLoadData();

  void magnetDetected();

  void next();

  void previous();

  void switchOff(bool value);

private:
  PictureSourceType sourceType;
  PictureSource* pictureSource;
  bool switchedOff;
};

#endif // TLCCONTROLLER_H
