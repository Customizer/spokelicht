#include <SoftwareSerial.h>
#include "WireNB.h"
#include "Arduino.h"
#include "inttypes.h"
#include <EEPROM.h>
#include "EEPROMNB.h"
#include "Tlc5940.h"


#define SPOKELICHT
#define W__RITE_TEST_DATA
// define this if data written to the EEPROM should be verified after writing.
#define WRITE_DATA_READ_CHECK


#include "Settings.h"
#include "Logger.h"
#include "SerialInterface.h"
#include "SerialCommunicator.h"
#include "TLCController.h"


/*

 Data Format

 Animation-Header
 ----------------
 Byte
 0        Animation-ID
 1-2      Begin of previous animation: Absolute Adress of the previous animation
 3-4      Begin of next animation: Absolute Adress of the next animation
 5        Number of pictures in this animation

 Picture-Header
 --------------
 0              Color count: How many colors are actually used by this image? Maximum: 0x0F (16)
 1              Display time: How long should this picture be displayed (0->unlimited)
 2-(3*bit0)     Support for 2^4=16 Colors with 24-Bit depth each per picture, so maximum: 0x30 (48)

 */


// Duration of the last rotation in micros
volatile uint32_t sl_rotation_duration=1000000;
// Duration of one line
volatile uint32_t sl_line_duration=100000;
// Timestamp of the last rotation's start, micros.
volatile long sl_last_rotation = micros();

volatile uint8_t sl_last_hall_state = HIGH;

// was the magnet detected in this cycle?
volatile uint8_t sl_magnet_detected = 0;

TLCController tlcController;
SerialCommunicator serialCommunicator;
Settings settings;
SerialInterface serial;

#ifdef LOGGING
Logger logger;
#endif //LOGGING

// I don't know, if this method calculates correct values
uint16_t freeRam () {
  extern uint16_t __heap_start, *__brkval;
  uint16_t v;
  return (uint16_t) &v - (__brkval == 0 ? (uint16_t) &__heap_start : (uint16_t) __brkval);
}

void setup() {
  // Set the following parameter to true, if your bluetooth module
  // is connected via software serial
  serial.init(57600, false);

#ifdef LOGGING
  logger.setLoggingLevel(DEBUG);
#endif //LOGGING

  LOGGER(F("Starting setup"), DEBUG);

  LOGGER(F("free ram"), freeRam(), DEBUG);

  tlcController.setPictureSource(tlcController.StoredInEEPROM);

  LOGGER(F("Zeroline"), settings.zeroLine, INFO);

  delay(1000);
  // Start the i2c communications
  WireNB.begin();
  pinMode(HALL_PIN, INPUT);

  Tlc.init();
  LOGGER(F("TLC initialized."), INFO);

  interrupts();
  // Pin Change mask Register: Bit 2 is PCINT10, which is A2 which is connected to the halleffect sensor.
  PCMSK1 |= _BV(PCINT10);
  //  // Enable Button interrupts
  //  PCMSK2 |= _BV(PCINT22);
  //  PCMSK2 |= _BV(PCINT23);
  // enable the intrrupt PCINT14:8
  PCICR |= _BV(PCIE1);
  //  // enable the intrrupt PCINT23:6
  //  PCICR |= _BV(PCIE2);
  LOGGER(F("Interrupts set up"), INFO);

  tlcController.preLoadData();

  LOGGER(F("Initial Data loaded."), INFO);
  LOGGER(F("Setup done."), INFO);

}


ISR(PCINT1_vect) {
  // TODO: find the atmel Constant for 0x04
  uint8_t state = (PINC & 0x04) != 0;
  long current_micros = micros();
  // LOW->Magnet present;
  // HIGH->Magnet not present
  // act on the falling edge.
  if (sl_last_hall_state == LOW && state == HIGH) {
    sl_rotation_duration = current_micros-sl_last_rotation;
    sl_line_duration = sl_rotation_duration / settings.lines;
    sl_last_rotation = current_micros;
    sl_magnet_detected = 1;
  }
  sl_last_hall_state = state;
}


volatile uint8_t magnetDetectionCount = 0;

volatile uint8_t tlcwait = 0;


void loop() {
  uint8_t magnet_detected = sl_magnet_detected;

  sl_magnet_detected = 0;
  if (magnet_detected) {
    tlcController.magnetDetected();
    magnetDetectionCount++;
    LOGGER("magnet detected", INSANE_DEBUG);
  }

  if (magnetDetectionCount == 10) {
    LOGGER("tlcwait", tlcwait, HEAVY_DEBUG);
    LOGGER("r", sl_rotation_duration, HEAVY_DEBUG);
    tlcwait = 0;
    magnetDetectionCount = 0;
  }

  // safe the time this loop started.
  long lineStartTime = micros();

  // measure cycle duration --> done in the interrupt handler.

  tlcController.updateTLCLine();
  tlcController.waitForTlcAndLoadNextLines();

  // -------------------------------------------------------------------------------------
  // wait for cycletime to be over
  // -------------------------------------------------------------------------------------
  // calculate the waiting duration.
  // TODO: if connected load the data for the slave-board here.

  long toWait = sl_line_duration - (micros() - lineStartTime);


  // delayMicroseconds is only precise for values up to 16383
  // using delay() here is actually pointless as if toWait is 16383 we have a total waitingtime of 1.638.300
  // which means far less than one rotation per second - POV will not work
  // TODO in future Versions a cool I cannot work effect (like the knight rider-style scanner) should kick in
  // in this situation.
  if (toWait > 16383) {
    delay(toWait/1000);
  } else if (toWait > 0) {
    delayMicroseconds(toWait);
  } else {
    if (magnetDetectionCount != 0 && magnet_detected) {
      LOGGER("TF", DEBUG);
      LOGGER("toWait", toWait, HEAVY_DEBUG);
      LOGGER("sl_line_duration", sl_line_duration, HEAVY_DEBUG);
    }
    // what now? Most likely we are too slow... maybe reduce the resolution?
  }


  // switch the LEDs off, when the wheel is not turning or
  // turning slowly
  if (micros() - sl_last_rotation > 800000) {
    tlcController.switchOff(true);
  } else {
    tlcController.switchOff(false);
  }

  serialCommunicator.serialDataAvailable();
  LOGGER("free ram: ", freeRam(), INSANE_DEBUG);

}
