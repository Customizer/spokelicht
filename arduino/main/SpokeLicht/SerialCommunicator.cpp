#include "SerialCommunicator.h"

SerialCommunicator::SerialCommunicator() {
  serialCommandBufferSize = 8;
  serialCommandBuffer = new uint8_t[serialCommandBufferSize];

  errorCode.ERR_NOT_ENOUGH_DATA = 'l';
  errorCode.ERR_TOO_MUCH_DATA ='s';
  errorCode. ERR_NUMBER_TOO_BIG = 'n';
  errorCode. ERR_UNSUPPORTED_COMMAND = 'k';
  errorCode. ERR_CHECKSUM_FAIL = 'c';
  errorCode. ERR_TIMEOUT = 't';

  serialCommandBufferLength = 0;
  eeprom = new EEPROMInterface(SerialCommunicator::noop /* no callback at this point */);
}

SerialCommunicator::~SerialCommunicator() {
  delete eeprom;
}


// reads as many bytes as specified in the current serial command buffer and stores it in the external eeprom.
void SerialCommunicator::writeDataToEEPROM() {
  if (serialCommandBufferLength == 0) {
    //no payload - we do not know how many bytes to receive/store
    declineSerialRequest('i');
    return;
  }
  // fetch the length of the incoming packages from the buffer into count...
  long count = 0;
  for (int i = 0 ; i < serialCommandBufferLength ; i++) {
    count |= serialCommandBuffer[i];
    if (i < serialCommandBufferLength - 1) {
      count = count << 8;
    }
  }
  // reply, ok!
  serial.write('o');
  serial.write('$');

  // Store data in the EEPROM.

  int LENGTH=15;
  byte buffer[LENGTH];

  byte chksum =0;
  // the byte just read;
  byte in  = 0;
  // pointer within the EEPROM
  int eepromWritePointer=0;
  // pointer within the buffer
  int bufferWritePointer=0;
  // bytes read since last EEPROM write
  int bytesRead = 0;
  for (long i = 0; i<count; i++) {
    if (i >= LENGTH && i % LENGTH == 0) {
      // no data - must not happen
      if (serial.read() != -1) {
        declineSerialRequest(0x42);
      }
      eeprom->writeBytesTo(eepromWritePointer, buffer, bytesRead);



#ifdef WRITE_DATA_READ_CHECK
      eeprom->readBytesAt(eepromWritePointer, bytesRead);
      for (int d = 0; d<bytesRead; d++) {

        if (eeprom->getData() != buffer[d]) {
          declineSerialRequest((byte)bytesRead);
        }
      }
#endif


      eepromWritePointer += bytesRead;
      bufferWritePointer = 0;
      serial.write(chksum);

      chksum = 0;
      bytesRead = 0;
    }

    waitForData(1000);
    in = serial.read();
    buffer[bufferWritePointer] = in;
    chksum ^= in;
    bytesRead++;
    bufferWritePointer++;

  }
  eeprom->writeBytesTo(eepromWritePointer, buffer, bytesRead);
  serial.write(chksum);

}

//
// Writes the 'x' command with byte b as payload after flushing any incoming data.
//
void SerialCommunicator::declineSerialRequest(uint8_t b) {
  while(serial.available()) {
    serial.read();
  }
  serial.write('x');
  serial.write(b);
  serial.write('$');
  serialCommandBufferLength = 0;
}


// waits ms for data to be available. if no data is received within ms, blinks the LED and waits forever.
void SerialCommunicator::waitForData(int ms) {
  int wait = 0;
  // wait for data...
  for (wait = 0; wait < ms && !serial.available(); wait++) {
    delay (1);
  }
  if (wait == ms) {
    // receive timeout
    while (true) {
      digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
      delay(1000);              // wait for a second
      digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
      delay(1000);
    }
  }
}

void SerialCommunicator::calibrateZeroLine() {
  if (serialCommandBufferLength < 1) {
    declineSerialRequest(errorCode.ERR_NOT_ENOUGH_DATA);
  } else if (serialCommandBufferLength > 1) {
    declineSerialRequest(errorCode.ERR_TOO_MUCH_DATA);
  } else if (serialCommandBuffer[0] > settings.lines) {
    declineSerialRequest(errorCode.ERR_NUMBER_TOO_BIG);
  } else {
    settings.zeroLine = serialCommandBuffer[0];

    LOGGER(F("ok, set zero to"), settings.zeroLine, DEBUG);

    // store the address in the atmega's (!) EERPROM
    EEPROM.write(CONFIG_ZERO_LINE, settings.zeroLine);
  }
}

void SerialCommunicator::serialDataAvailable() {
  if (!serial.available()) {
    return;
  }

  if (serialCommandBufferLength != 0) {
    declineSerialRequest('j');
  } else {
    // first byte is always the command.
    byte commandByte = serial.read();

    // Check if a valid command was received.
    // w --> Write Bytes.
    if (commandByte != 'w'
        && commandByte != 'r'
        && commandByte != 'n'
        && commandByte != 'p'
        && commandByte != 'c'
        && commandByte != 'b'
        && commandByte != 'd') {
      declineSerialRequest(errorCode.ERR_UNSUPPORTED_COMMAND);

      LOGGER(F("Received invalid request "), (char) commandByte, DEBUG);

      // flush buffer
      while(serial.available()) {
        Serial.read();
      }

      return;
    }

    boolean commandComplete = false;
    uint8_t inByte=0;
    // read the payload and store it in the global buffer
    uint16_t MAX_WAIT = 300;
    uint16_t wait = 0 ;
    for (; wait < MAX_WAIT &&!commandComplete && serialCommandBufferLength < serialCommandBufferSize ;) {
      if (serial.available()) {
        inByte = serial.read();
        if (inByte == '$') {
          commandComplete = true;
        } else {

          serialCommandBuffer[serialCommandBufferLength] = inByte;
          serialCommandBufferLength++;
        }
      } else {
        delay(100);
        wait++;
      }

    }
    if (commandComplete) {
      // store the ommand globally
      serialCommand = commandByte;

      // and process the command.
      switch (serialCommand) {
      case 'w':
        writeDataToEEPROM();
        break;

      case 'n':
        tlcController.next();
        break;
      case 'p':
        tlcController.previous();
        break;
      case 'c':
        calibrateZeroLine();
        break;
      case 'b':
        brighter();
        break;
      case 'd':
        darker();
        break;
      case 'r':
        resetArduino();
        break;
      }

    } else if (wait == MAX_WAIT) {
      declineSerialRequest(errorCode.ERR_TIMEOUT);
    }

    // else nothing to do, an invalid command was received.
    serialCommandBufferLength = 0;

  }
}

void SerialCommunicator::resetArduino() {
  // Initiate watchdog reset
  wdt_enable(WDTO_15MS);
}

void SerialCommunicator::brighter() {
  if (settings.brightness <= 4) {
    settings.brightness++;
  }

  LOGGER(F("new Brightness"), settings.brightness, DEBUG);
}

void SerialCommunicator::darker() {
  if (settings.brightness > 0) {
    settings.brightness--;
  }

  LOGGER(F("new Brightness"), settings.brightness, DEBUG);
}


void SerialCommunicator::noop() {
  // This is just a dummy method, needed for the initialization of
  // the EEPROM interface
}
