#include "OnlyLoggingPictureSource.h"

OnlyLoggingPictureSource::OnlyLoggingPictureSource()
  : PictureSource() {
}

OnlyLoggingPictureSource::~OnlyLoggingPictureSource() {
}

void OnlyLoggingPictureSource::preLoadData() {
  LOGGER(F("pre-loading data"), DEBUG);
}

void OnlyLoggingPictureSource::loadNextLines() {
  LOGGER(F("loadNextLines"), INSANE_DEBUG);
}

void OnlyLoggingPictureSource::loadLine1NonBlocking() {
  LOGGER(F("loadLine1NonBlocking"), INSANE_DEBUG);
}

uint8_t OnlyLoggingPictureSource::getRed(uint8_t lineNr, uint8_t ledNr) {
  LOGGER(F("getRed lineNr"), lineNr, INSANE_DEBUG);
  LOGGER(F("getRed ledNr"), ledNr, INSANE_DEBUG);
  return 0;
}

uint8_t OnlyLoggingPictureSource::getGreen(uint8_t lineNr, uint8_t ledNr) {
  LOGGER(F("getGreen lineNr"), lineNr, INSANE_DEBUG);
  LOGGER(F("getGreen ledNr"), ledNr, INSANE_DEBUG);
  return 0;
}

uint8_t OnlyLoggingPictureSource::getBlue(uint8_t lineNr, uint8_t ledNr) {
  LOGGER(F("getBlue lineNr"), lineNr, INSANE_DEBUG);
  LOGGER(F("getBlue ledNr"), ledNr, INSANE_DEBUG);
  return 0;
}

void OnlyLoggingPictureSource::magnetDetected() {
  LOGGER(F("magnet detected"), DEBUG);
}

void OnlyLoggingPictureSource::next() {
  LOGGER(F("next"), DEBUG);
}

void OnlyLoggingPictureSource::previous() {
  LOGGER(F("previous"), DEBUG);
}
