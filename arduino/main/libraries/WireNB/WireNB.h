/*
  TwoWireNB.h - TWI/I2C library for Arduino & Wiring
  Copyright (c) 2006 Nicholas Zambetti.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef TwoWireNB_h
#define TwoWireNB_h

#include <inttypes.h>

#define BUFFER_LENGTH 32

#define RET_REQUEST_TOO_LONG 2
#define RET_TRANSMISSION_IN_PROGRESS 0xFF
#define RET_OK 1
class TwoWireNB 
{
  static uint8_t initialized ;
  
  private:
     uint8_t rxBuffer[BUFFER_LENGTH];
     uint8_t rxBufferIndex;
     uint8_t rxBufferLength;

     uint8_t txAddress;
     uint8_t txBuffer[BUFFER_LENGTH];
     uint8_t txBufferIndex;
     uint8_t txBufferLength;

     uint8_t requestedByteCount;
    

     void (*user_onRequestComplete)(void);
    
  public:
    //  void transmissionComplete(void);
    void transmissionCompleteCallback();
    TwoWireNB();
    void begin();
    void begin(uint8_t);
    void begin(int);
    void beginTransmission(uint8_t);
    uint8_t endTransmission(void);
    uint8_t requestFrom(uint8_t, uint8_t);
    uint8_t requestFromNonBlocking(uint8_t , uint8_t );
    uint8_t write(uint8_t);
    uint8_t write(const uint8_t *, uint8_t);
    uint8_t available(void);
    uint8_t read(void);
    uint8_t isRequestComplete();
    void onRequestComplete( void (*function)(void) );
    
};

extern TwoWireNB WireNB;

#endif

