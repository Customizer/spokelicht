#include "Settings.h"

Settings::Settings() {
  lines = 200;
  lineLength = 10;
  numLeds = 20;
  zeroLine = EEPROM.read(CONFIG_ZERO_LINE);
  brightness = 1;
}

Settings::~Settings() {
}
