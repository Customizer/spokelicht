#include "EEPROMAnimation.h"


EEPROMAnimation::EEPROMAnimation() {
  LOGGER(F("Animation: constructor"), DEBUG);
  current_animation = 0;
  previousAnimationAddress = 0;
  nextAnimationAddress = 0;
  numberOfPictures = 0;
  firstPictureAddress = 0;
  currentPicture = 0;
  currentPictureDisplayTime = 0;
  currentPictureAddress = 0;
  pictureDisplayDuration = 4;

  eeprom1 = new EEPROMInterface(EEPROMAnimation::noop /* no callback at this point */);
  eeprom2 = new EEPROMInterface(EEPROMAnimation::noop /* no callback at this point */);
}

EEPROMAnimation::~EEPROMAnimation() {
  delete eeprom1;
  delete eeprom2;
}

void EEPROMAnimation::loadAnimation(uint16_t address) {
  eeprom1->readBytesAt(address,ANIMATION_HEADER_LENGTH);

  current_animation = eeprom1->getData();
  // previous animation-address
  uint8_t prevByte1 = eeprom1->getData();
  uint8_t prevByte2 = eeprom1->getData();
  previousAnimationAddress = bytesToUint16(prevByte1, prevByte2);
  // next animation-address
  uint8_t nextByte1 = eeprom1->getData();
  uint8_t nextByte2 = eeprom1->getData();
  nextAnimationAddress = bytesToUint16(nextByte1, nextByte2);
  // # of pics
  numberOfPictures = eeprom1->getData();
  // first picture (for looping)
  firstPictureAddress = address + ANIMATION_HEADER_LENGTH;

  loadPicture(firstPictureAddress);
}


void EEPROMAnimation::loadAnimationNr(uint8_t animationNr) {
  if (animationNr < 0) {
    // illegal argument
    return;
  }

  // Start with loading the first animation, as we don't know,
  // how many animations are currently saved to the EEPROM
  // and we don't want to run out of bounds.
  for (int i = 0; i < animationNr; i++) {
    loadNextAnimation();
  }
}

void EEPROMAnimation::loadNextAnimation() {
  LOGGER(F("address of next animation: "), nextAnimationAddress, DEBUG);

  loadAnimation(nextAnimationAddress);
  EEPROM.write(CONFIG_LAST_USED_ANIMATION, current_animation);
}

void EEPROMAnimation::loadPreviousAnimation() {
  loadAnimation(previousAnimationAddress);
  EEPROM.write(CONFIG_LAST_USED_ANIMATION, current_animation);
}

void EEPROMAnimation::loadLine1(uint16_t lineAddress) {
  eeprom1->readBytesAt(calculateEEPROMAddress(lineAddress), settings.lineLength);
}

void EEPROMAnimation::loadLine2(uint16_t lineAddress) {
  eeprom2->readBytesAt(calculateEEPROMAddress(lineAddress), settings.lineLength);
}

void EEPROMAnimation::loadLine1NonBlocking(uint16_t lineAddress) {
  eeprom1->readBytesAtNonBlocking(calculateEEPROMAddress(lineAddress), settings.lineLength);
}

bool EEPROMAnimation::readingLine1Finished() {
  return eeprom1->readingFinished();
}

uint8_t EEPROMAnimation::getData1() {
  return eeprom1->getData();
}

uint8_t EEPROMAnimation::getData2() {
  return eeprom2->getData();
}

void EEPROMAnimation::magnetDetected() {
  currentPictureDisplayTime++;

  if (currentPictureDisplayTime >= pictureDisplayDuration) {
    loadNextPicture();
    currentPictureDisplayTime = 0;
  }
}


void EEPROMAnimation::loadPicture(uint16_t address) {
  eeprom1->readBytesAt(address, 2);
  uint8_t colorCount = eeprom1->getData();
  // time how long the current picture should be displayed
  pictureDisplayDuration = eeprom1->getData();

  // We cannot request more than 32 at once, therefore
  // we make several calls to readBytesAt
  int colorIndexMax = colorCount * 3;
  for (int offsetIndex = 0; offsetIndex < colorIndexMax / 32 + 1; offsetIndex++) {
    int offset = offsetIndex * 32;
    int fetchLength = colorIndexMax - offset;
    eeprom1->readBytesAt(address + 2 + offset, fetchLength);
    for (int i = offset; eeprom1->dataAvailable() && i < (fetchLength + offset); i++) {
      colorPalette[i] = eeprom1->getData();
    }
  }

  currentPictureAddress = address + 2 + (colorCount*3);

}

void EEPROMAnimation::loadNextPicture() {

  currentPicture++;

  uint16_t adress;
  if (currentPicture >= numberOfPictures) {
    currentPicture = 0;
    adress = firstPictureAddress;

  } else {
    adress = currentPictureAddress + (settings.lineLength * settings.lines);
  }

  loadPicture(adress);
}

uint16_t EEPROMAnimation::calculateEEPROMAddress(uint16_t lineAddress) {
  return currentPictureAddress + lineAddress;
}

uint16_t EEPROMAnimation::bytesToUint16(uint8_t b1, uint8_t b2) {
  uint16_t retval = b1;
  retval = retval << 8;
  retval |= b2;
  return retval;
}

void EEPROMAnimation::log(uint16_t currentLineAddress, uint16_t currentLowerLineAddress) {
  LOGGER(F("ca"), current_animation, HEAVY_DEBUG);
  LOGGER(F("paadr"), previousAnimationAddress, HEAVY_DEBUG);
  LOGGER(F("naadr"), nextAnimationAddress, HEAVY_DEBUG);
  LOGGER(F("curLiAd"), calculateEEPROMAddress(currentLineAddress), HEAVY_DEBUG);
  LOGGER(F("curLoLiAd"), calculateEEPROMAddress(currentLowerLineAddress), HEAVY_DEBUG);
  LOGGER(F("cpa"), currentPictureAddress, HEAVY_DEBUG);
}

void EEPROMAnimation::noop() {
  // This is just a dummy method, needed for the initialization of
  // the EEPROM interface
}
