#ifndef LOGGER_H
#define LOGGER_H

#include "inttypes.h"
#include "Arduino.h"
#include "SerialInterface.h"

extern SerialInterface serial;

enum DEBUG_LEVEL { INFO, DEBUG, HEAVY_DEBUG, INSANE_DEBUG };

class Logger {
public:
  Logger();
  ~Logger();

  void setLoggingLevel(DEBUG_LEVEL debugLevel);

  void log(const char message[], DEBUG_LEVEL level);

  void log(const __FlashStringHelper* message, DEBUG_LEVEL level);

  void log(const char name[], uint8_t value, DEBUG_LEVEL level);

  void log(const __FlashStringHelper* name, uint8_t value, DEBUG_LEVEL level);

  void log(const char name[], uint16_t value, DEBUG_LEVEL level);

  void log(const __FlashStringHelper* name, uint16_t value, DEBUG_LEVEL level);

  void log(const char name[], uint32_t value, DEBUG_LEVEL level);

  void log(const __FlashStringHelper* name, uint32_t value, DEBUG_LEVEL level);

  void log(const char name[], long value, DEBUG_LEVEL level);

  void log(const __FlashStringHelper* name, long value, DEBUG_LEVEL level);

  void log(const char name[], char value, DEBUG_LEVEL level);

  void log(const __FlashStringHelper* name, char value, DEBUG_LEVEL level);

private:
  DEBUG_LEVEL currentLevel;

  bool isLoggingLevelSufficient(DEBUG_LEVEL level);
};

#endif // LOGGER_H
