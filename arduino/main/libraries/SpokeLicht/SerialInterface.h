#ifndef SERIALINTERFACE_H
#define SERIALINTERFACE_H

#include <Stream.h>
#include <Arduino.h>
#include <SoftwareSerial.h>

class SerialInterface : public Stream {
public:
  SerialInterface();
  ~SerialInterface();

  void init(unsigned long baud, bool useSoftwareSerial);

  size_t write(uint8_t data);
  int available();
  int read();
  int peek();
  void flush();

  // overriding default implementation of write method
  size_t write(const uint8_t *buffer, size_t size);


private:
  bool useSoftwareSerial;

  SoftwareSerial* softwareSerial;
};

#endif // SERIALINTERFACE_H
