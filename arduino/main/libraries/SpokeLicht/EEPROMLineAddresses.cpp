#include "EEPROMLineAddresses.h"

EEPROMLineAddresses::EEPROMLineAddresses() {
  reset();
}

EEPROMLineAddresses::~EEPROMLineAddresses() {
}

void EEPROMLineAddresses::nextLines() {
  currentLine += 2;

  if (currentLine == settings.lines) {
    currentLine = 1;
  } else if (currentLine > settings.lines) {
    currentLine = 0;
  }
}

void EEPROMLineAddresses::reset() {
  currentLine = settings.zeroLine;
}

uint16_t EEPROMLineAddresses::getCurrentLineAddress() {
  uint16_t address = calculateLineAddress(currentLine);
  return address;
}

uint16_t EEPROMLineAddresses::getCurrentLowerLineAddress() {
  if (currentLine % 2 == 1) {
    return calculateLineAddress(currentLine - 1);
  }
  uint16_t address = calculateLineAddress(currentLine + 1);
  return address;
}

uint16_t EEPROMLineAddresses::calculateLineAddress(uint8_t line) {
  return (line * settings.lineLength);
}
