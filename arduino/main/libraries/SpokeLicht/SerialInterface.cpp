#include "SerialInterface.h"

SerialInterface::SerialInterface() {
}

SerialInterface::~SerialInterface() {
  if (useSoftwareSerial) {
    softwareSerial->end();
    delete softwareSerial;
  }
}

void SerialInterface::init(unsigned long baud, bool useSoftwareSerial) {
  this->useSoftwareSerial = useSoftwareSerial;
  if (useSoftwareSerial) {
    softwareSerial = new SoftwareSerial(8,A0);
  }

  if (useSoftwareSerial) {
    softwareSerial->begin(baud);
  } else {
    Serial.begin(baud);
  }
}

size_t SerialInterface::write(uint8_t data) {
  if (useSoftwareSerial) {
    return softwareSerial->write(data);
  }
  return Serial.write(data);
}

int SerialInterface::available() {
  if (useSoftwareSerial) {
    return softwareSerial->available();
  }
  return Serial.available();
}

int SerialInterface::read() {
  if (useSoftwareSerial) {
    return softwareSerial->read();
  }
  return Serial.read();
}

int SerialInterface::peek() {
  if (useSoftwareSerial) {
    return softwareSerial->peek();
  }
  return Serial.peek();
}

void SerialInterface::flush() {
  if (useSoftwareSerial) {
    softwareSerial->flush();
  } else {
    Serial.flush();
  }
}

size_t SerialInterface::write(const uint8_t *buffer, size_t size) {
  if (useSoftwareSerial) {
    softwareSerial->write(buffer, size);
  }
  return Serial.write(buffer, size);
}
