#ifndef EEPROMANIMATION_H
#define EEPROMANIMATION_H

#include "inttypes.h"
#include <EEPROMNB.h>
#include <EEPROM.h>
#include "Settings.h"
#include "Logger.h"


extern Settings settings;
#ifdef LOGGING
extern Logger logger;
#endif //LOGGING

class EEPROMAnimation {

public:
  EEPROMAnimation();
  ~EEPROMAnimation();

  /*
   * color-map with n*3+0 -> R; n*3+1 -> G; n*3+2 -> B,
   * where n is the color index from the pixel
   */
  uint8_t colorPalette[16*3];


  void loadAnimation(uint16_t address);

  void loadAnimationNr(uint8_t animationNr);

  void loadNextAnimation();

  void loadPreviousAnimation();

  void loadNextPicture();

  /**
   * Loads the line with the passed number of the currently
   * loaded animation.
   */
  void loadLine1(uint16_t lineAddress);

  /**
   * Loads the line with the passed number of the currently
   * loaded animation.
   */
  void loadLine2(uint16_t lineAddress);

  /**
   * Loads the line with the passed address of the currently
   * loaded animation asynchronously.
   */
  void loadLine1NonBlocking(uint16_t lineAddress);

  /**
   * Check, if the non blocking reading has finished.
   */
  bool readingLine1Finished();

  uint8_t getData1();

  uint8_t getData2();

  void magnetDetected();

  void log(uint16_t currentLineAddress, uint16_t currentLowerLineAddress);

private:

  EEPROMInterface * eeprom1;
  EEPROMInterface * eeprom2;

  // current animation-Id displayed
  uint8_t current_animation;

  // previous animation-address
  uint16_t previousAnimationAddress;

  // next animation-address
  uint16_t nextAnimationAddress;

  // number of pics in current animaiton
  uint16_t numberOfPictures;

  // address of the first picture in the animation. NOT the first databyte!!
  uint16_t firstPictureAddress;

  // current picture in the animation to be displayed
  uint8_t currentPicture;

  // time how long the current picture should be displayed
  uint8_t pictureDisplayDuration;

  // time how long the current picture is displayed so far
  uint8_t currentPictureDisplayTime;

  // address _of the first databyte_ of the currently displayed picture
  uint16_t currentPictureAddress;

  void loadPicture(uint16_t address);

  uint16_t calculateEEPROMAddress(uint16_t lineAddress);

  uint16_t bytesToUint16(uint8_t b1, uint8_t b2);

  static void noop();
};

#endif // EEPROMANIMATION_H
