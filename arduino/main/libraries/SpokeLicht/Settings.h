#ifndef SETTINGS_H
#define SETTINGS_H

#include <inttypes.h>
#include <EEPROM.h>

#define HALL_PIN A2

const uint8_t EXTERNAL_EEPROM_ADRESS = 0x50 ;

// Adress of the ZERO_LINE within the atmega's EEPROM
#define CONFIG_ZERO_LINE 0

// Adress of the last used animation within the atmega's EEPROM
#define CONFIG_LAST_USED_ANIMATION 1

#define ANIMATION_HEADER_LENGTH 6

// comment the following define statement to disable logging completely
#define LOGGING

#ifdef LOGGING
#define LOGGER(...) logger.log(__VA_ARGS__)
#else
#define LOGGER(...) ((void)0)
#endif



class Settings {
public:
  Settings();
  ~Settings();

  // How many lines to display in one rotation
  uint8_t lines;

  uint8_t zeroLine;

  // number of bytes for one line.
  uint8_t lineLength;

  // number of leds on one arm
  uint8_t numLeds;

  uint8_t brightness;

};

#endif // SETTINGS_H
