#include "EEPROMPictureSource.h"

EEPROMPictureSource::EEPROMPictureSource()
  : PictureSource() {
  dataFetched = false;
  line1 = new uint8_t[settings.numLeds / 2];
  line2 = new uint8_t[settings.numLeds / 2];
}

EEPROMPictureSource::~EEPROMPictureSource() {
  delete[] line1;
  delete[] line2;
}

void EEPROMPictureSource::preLoadData() {
  // load first animation as fail save, if no suitable value has been stored in EEPROM

  animation.loadAnimation(0);
  LOGGER(F("Animation 0 loaded"), DEBUG);

  animation.loadAnimationNr(EEPROM.read(CONFIG_LAST_USED_ANIMATION));

  animation.loadLine1(lineAddresses.getCurrentLineAddress());
  animation.loadLine2(lineAddresses.getCurrentLowerLineAddress());
  LOGGER(F("first two lines pre-loaded"), DEBUG);
}

void EEPROMPictureSource::loadNextLines() {
  LOGGER(F("new line address"), lineAddresses.getCurrentLineAddress(), HEAVY_DEBUG);
  LOGGER(F("new lower line address"), lineAddresses.getCurrentLowerLineAddress(), HEAVY_DEBUG);

  dataFetched = false;

  while (!animation.readingLine1Finished()) {
    // We have already started loading the first line, so
    // wait for the first loading operation to be finished
  }

  animation.loadLine2(lineAddresses.getCurrentLowerLineAddress());

  // Loading is finished, jump to next line addresses
  lineAddresses.nextLines();
}

void EEPROMPictureSource::loadLine1NonBlocking() {
  animation.loadLine1NonBlocking(lineAddresses.getCurrentLineAddress());
}

uint8_t EEPROMPictureSource::getRed(uint8_t lineNr, uint8_t ledNr) {
  fetchDataIfNecessary();
  return getColor(lineNr, ledNr, 0);
}

uint8_t EEPROMPictureSource::getGreen(uint8_t lineNr, uint8_t ledNr) {
  fetchDataIfNecessary();
  return getColor(lineNr, ledNr, 1);
}

uint8_t EEPROMPictureSource::getBlue(uint8_t lineNr, uint8_t ledNr) {
  fetchDataIfNecessary();
  return getColor(lineNr, ledNr, 2);
}

void EEPROMPictureSource::fetchDataIfNecessary() {
  if(!dataFetched) {
    // TODO: Error handling in case there is not enough
    // data in the buffer? available() is not called...

    for (uint8_t i = 0; i < (settings.numLeds / 2); i++) {
      line1[i] = animation.getData1();
      line2[i] = animation.getData2();
    }
    dataFetched = true;
  }
}

uint8_t EEPROMPictureSource::getColor(uint8_t lineNr, uint8_t ledNr, uint8_t offset) {
  if (lineNr == 0) {
    return getColor(line1, ledNr, offset);
  } else {
    return getColor(line2, ledNr, offset);
  }
}

uint8_t EEPROMPictureSource::getColor(uint8_t* line, uint8_t ledNr, uint8_t offset) {
  uint8_t address = ledNr / 2;
  uint8_t color = (line[(address)] & 0x0F) * 3;

  if (ledNr % 2 != 0) {
    color = (line[address] >> 4) * 3;;
  }

  return animation.colorPalette[color + offset];
}

void EEPROMPictureSource::magnetDetected() {
  lineAddresses.reset();
  animation.magnetDetected();
}

void EEPROMPictureSource::next() {
  animation.loadNextAnimation();
}

void EEPROMPictureSource::previous() {
  animation.loadPreviousAnimation();
}
