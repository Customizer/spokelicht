#include "Logger.h"

Logger::Logger() {
  this->currentLevel = INFO;
}

Logger::~Logger() {
}

void Logger::setLoggingLevel(DEBUG_LEVEL debugLevel) {
  this->currentLevel = debugLevel;
}


void Logger::log(const char message[], DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.println(message);
  }
}

void Logger::log(const __FlashStringHelper* message, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.println(message);
  }
}

void Logger::log(const char name[], uint8_t value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}

void Logger::log(const __FlashStringHelper* name, uint8_t value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}

void Logger::log(const char name[], uint16_t value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}


void Logger::log(const __FlashStringHelper* name, uint16_t value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}

void Logger::log(const char name[], uint32_t value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}

void Logger::log(const __FlashStringHelper* name, uint32_t value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}

void Logger::log(const char name[], long value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}

void Logger::log(const __FlashStringHelper* name, long value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}

void Logger::log(const char name[], char value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}

void Logger::log(const __FlashStringHelper* name, char value, DEBUG_LEVEL level) {
  if (isLoggingLevelSufficient(level)) {
    serial.print(name);
    serial.print(": ");
    serial.println(value);
  }
}

bool Logger::isLoggingLevelSufficient(DEBUG_LEVEL level) {
  if (currentLevel == INFO) {
    return level == INFO;
  } else if(currentLevel == DEBUG) {
    return level == INFO || level == DEBUG;
  } else if(currentLevel == HEAVY_DEBUG) {
    return level == INFO || level == DEBUG || level == HEAVY_DEBUG;
  } else if(currentLevel == INSANE_DEBUG) {
    return level == INFO
           || level == DEBUG
           || level == HEAVY_DEBUG
           || level == INSANE_DEBUG;
  }

  return false;
}
