#ifndef PICTURESOURCE_H
#define PICTURESOURCE_H

#include "Settings.h"
#include "Logger.h"


/**
 * The color information, which is passed to the TLC, has
 * to be delivered by an inheritor of this class. By using
 * the strategy pattern, we enable easily interchangeable ways
 * of displaying different kind of pictures.
 */
class PictureSource {
public:
  PictureSource();
  virtual ~PictureSource();

  /*
   * This method can be used to preLoad and cache data, if this
   * is helpful.
   */
  virtual void preLoadData() = 0;

  /*
   * Load the next lines (if necessary)
   */
  virtual void loadNextLines() = 0;

  /*
   * This method can be used to asynchronously load the next line
   * (with number 1)
   */
  virtual void loadLine1NonBlocking() = 0;

  /*
   * Get the color information for the given line and led
   */
  virtual uint8_t getRed(uint8_t lineNr, uint8_t ledNr) = 0;
  virtual uint8_t getGreen(uint8_t lineNr, uint8_t ledNr) = 0;
  virtual uint8_t getBlue(uint8_t lineNr, uint8_t ledNr) = 0;

  /*
   * This method gets called, when the hall sensor passes the
   * magnet. After this method has been called, the next line
   * the PictureSource delivers, has to be the first line of the
   * image, again.
   */
  virtual void magnetDetected() = 0;

  /*
   * Jump to the next picture or animation (depending on the kind of
   * picture source).
   */
  virtual void next() = 0;

  /*
   * Jump to the previous picture or animation (depending on the kind
   * of picture source).
   */
  virtual void previous() = 0;

};

#endif // PICTURESOURCE_H
