#ifndef EEPROMPICTURESOURCE_H
#define EEPROMPICTURESOURCE_H

#include "PictureSource.h"
#include "Settings.h"
#include "EEPROMAnimation.h"
#include "EEPROMLineAddresses.h"

#ifdef LOGGING
extern Logger logger;
#endif //LOGGING
extern Settings settings;

/**
 * Loads the picture or the animation from the EEPROM.
 */
class EEPROMPictureSource : public PictureSource {
public:
  EEPROMPictureSource();
  virtual  ~EEPROMPictureSource();

  /* virtual */ void preLoadData();

  /* virtual */ void loadNextLines();

  /* virtual */ void loadLine1NonBlocking();

  /* virtual */ uint8_t getRed(uint8_t lineNr, uint8_t ledNr);

  /* virtual */ uint8_t getGreen(uint8_t lineNr, uint8_t ledNr);

  /* virtual */ uint8_t getBlue(uint8_t lineNr, uint8_t ledNr);

  /* virtual */ void magnetDetected();

  /* virtual */ void next();

  /* virtual */ void previous();

private:

  bool dataFetched;
  EEPROMAnimation animation;
  EEPROMLineAddresses lineAddresses;

  uint8_t *line1;
  uint8_t *line2;

  void fetchDataIfNecessary();

  uint8_t getColor(uint8_t lineNr, uint8_t ledNr, uint8_t offset);
  uint8_t getColor(uint8_t* line, uint8_t ledNr, uint8_t offset);

};

#endif // EEPROMPICTURESOURCE_H
