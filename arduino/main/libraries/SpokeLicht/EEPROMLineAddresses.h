#ifndef EEPROMLINEADDRESSES_H
#define EEPROMLINEADDRESSES_H

#include <inttypes.h>
#include "Settings.h"
#include "Logger.h"

extern Settings settings;
#ifdef LOGGING
extern Logger logger;
#endif //LOGGING

/**
 * The lines are stored in the EEPROM in an order, which
 * is optimized for fast reading. This class calculates
 * the address offsets of the currently active lines to
 * the start address of the picture.
 */
class EEPROMLineAddresses {
public:
  EEPROMLineAddresses();
  ~EEPROMLineAddresses();

  void nextLines();

  void reset();

  uint16_t getCurrentLineAddress();

  uint16_t getCurrentLowerLineAddress();

private:

  // Number of the current line of the first arm
  uint8_t currentLine;

  uint16_t calculateLineAddress(uint8_t line);
};

#endif // EEPROMLINEADDRESSES_H
