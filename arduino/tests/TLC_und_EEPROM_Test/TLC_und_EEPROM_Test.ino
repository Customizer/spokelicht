//writes a single byte to the eeprom an reads it afterwards.
#include "Tlc5940.h"

#include <Wire.h>
// PIN 1 is connected to ground. otherwise it woud be 0x50
const byte EEPROM = 0x50 ;
// enable or disable debugging via serial
boolean debug = false;

boolean readBytesAt(uint16_t addr, byte *buffer, int length) {
  if (debug) {
    Serial.print("Lese ");
    Serial.print("Addresse [");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print((int)((addr) & 0xFF), HEX);
    Serial.print("]: ");
  }
  openEEPROMAdress(addr);
  Wire.endTransmission();
  // einzelbyte lesen:
  Wire.requestFrom((int)EEPROM, length);
  for (int i=0; i< length; i++) {
    if (Wire.available()) {
      byte in = Wire.read();
      if (debug) {
        Serial.print(in);
        Serial.print(", ");
      }      
      buffer[i] = in;
    } 
    else {
      return false; 
    }
  }
  if (debug) {
    Serial.println("");
  }
  return true;
}

void openEEPROMAdress(uint16_t addr) {
  Wire.beginTransmission(EEPROM);
  // Adresse schreiben
  Wire.write((int)((addr >> 8) & 0xFF));
  Wire.write((int)(addr & 0xFF));

}

void writeBytesTo(uint16_t addr, byte * b, byte length) {

  openEEPROMAdress(addr );
  delay(10);
  if (debug) {
    Serial.print("Addresse [");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print((int)((addr) & 0xFF), HEX);
    Serial.print("]: ");

  }
  /*  if (debug) {
   Serial.print("Addresse 2:");
   Serial.println((int)((addr) & 0xFF), HEX);
   }*/

  uint16_t i = addr;
  uint16_t page=addr>>7;
  do {
    if (i%8==0) {
      if (i>addr || addr>>7 > page) {
        Wire.endTransmission();
        delay(10);
        openEEPROMAdress(i);
        if (addr>>7 > page){
          if (debug) {

            Serial.println("open next page");
          }
        } 
        else {
          //Serial.println("flush"); 
        }
        delay(10);
      } 

    }
    // einzelbyte schreiben:
    Wire.write(b[i-addr]);
    delay(1);
    if   (debug) {
      Serial.print(b[i-addr]);
      Serial.print(", ");
    }
    i++;
    page=addr>>7;
  } 
  while (i<addr+length);
  if (debug) {

    Serial.println();
  }
  Wire.endTransmission();

}


void reportError(uint16_t i, int j, byte expected, byte actual) {
  Serial.print("Fehler bei: ");
  Serial.print(i);
  Serial.print(", ");
  Serial.print(j);
  Serial.print(" soll: ");
  Serial.print(expected);
  Serial.print(" is: ");
  Serial.println(actual);

}

const uint16_t dataSize = 15;
byte inData[dataSize];

uint16_t brightnessValues [4] = {
  0,333,666,1000};

void setup() {
  byte data[dataSize];
  int lines=200;
  Wire.begin(); 
  int byteAddress = 0x0;
  Serial.begin(115200);


  Tlc.init();
  

  
  const byte startValue=0x40;
  byte b=startValue;
  boolean write = false;
  
  if (write) {
    
    Serial.println("schreibe...");
    for (uint16_t i = 0; i<lines; i++) {
      for (int j = 0; j<dataSize; j++) {
        data[j] = b;
        b++;
      }
  
      writeBytesTo(i*dataSize, data, dataSize);
  
    }
    Serial.println("Schreiben fertig.");
  }

  delay(10);
  Serial.println("jetzt lesen...");
  long tlcUpdateTime = 0;
  long tlcProgramTime = 0;
  long readTime = 0;
  int tlcUpdateCount = 0;
  long readStart = micros();
  long temp = 0;
  for (int rot = 0; rot<5; rot++) {
    b=startValue;
    for (uint16_t line = 0; line<lines; line++) {
      temp=micros();
      boolean ok = readBytesAt(line*dataSize,inData, dataSize);
      readTime+=micros() - temp;
      for (int j = 0; j<dataSize; j++) {
        for (int shift =0; shift < 4; shift++) {
          byte reqBright = inData[j] >> shift & 0x03;
          temp = micros();
          Tlc.set((j*4)+shift,brightnessValues[reqBright]);
          tlcProgramTime+=micros() - temp;
        }


        if(inData[j] != b) {
          reportError(line,j,b,inData[j]);
        } 
        b++;
      }
      if(!ok) {
      /*  Serial.println();
        Serial.print("Uebertragungsfehler: ");
        Serial.print(line);
        Serial.println();*/
      }
      
      long tlcUpdateStart = micros();
      Tlc.update(); 
      tlcUpdateTime += micros() - tlcUpdateStart;
    }
  }
  long time = micros() - readStart;
  Serial.println();
  Serial.print("fertig nach ");
  Serial.print(time);
  Serial.print(" micros. TLC update time");
  Serial.print(tlcUpdateTime);
  Serial.print(" micros. TLC program time");
  Serial.print(tlcProgramTime);
  Serial.print(" micros. Read time");
  Serial.print(readTime);
  Serial.print(" micros");
  delay(1000);

}



void loop() {
  delay(10000); 
}















