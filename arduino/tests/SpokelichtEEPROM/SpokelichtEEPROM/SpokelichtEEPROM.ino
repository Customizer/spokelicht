#include "WireNB.h"
#include "inttypes.h"
#include "EEPROMNB.h"


// PIN 1 is connected to ground. otherwise it woud be 0x50
const uint8_t EEPROM = 0x50 ;

const uint16_t et_dataSize = 15;
const uint16_t et_lines=2000;
const uint16_t et_middleLine = et_lines / 2;


void reportError(uint16_t i, int j, byte expected, byte actual) {
  Serial.print("Fehler bei: ");
  Serial.print(i);
  Serial.print(", ");
  Serial.print(j);
  Serial.print(" soll: ");
  Serial.print(expected);
  Serial.print(" is: ");
  Serial.println(actual);

}


volatile uint16_t et_currentLine = 0;
volatile uint16_t et_lowerLine = et_middleLine;
void readFinishedUpperLine();
void readFinishedLowerLine();

EEPROMInterface * et_if1 = new EEPROMInterface(readFinishedUpperLine);
EEPROMInterface * et_if2 = new EEPROMInterface(readFinishedLowerLine);

void readFinishedUpperLine() {
  /* This callback function does not seem to work. That's why we do this stuff
   * in the doTest method instead
  if (et_currentLine < et_middleLine) {
    et_lowerLine = et_currentLine + et_middleLine;
  } else {
    et_lowerLine = et_currentLine - et_middleLine;
  }

  */
  // The following does not work so far because the following triggers twi_write_to(). Since this code is
  // executed in an ISR-Routine no interrupts can be caught, so the TWI code gets stuck
  // TODO: make the following really non blocking by making write_to() non blocking.

//  et_if2 -> readBytesAtNonBlocking(et_lowerLine*et_dataSize, et_dataSize);
  //warReadingFertig = 3;
}
void readFinishedLowerLine() {
  // erstmal nix.
}

/*
 * The default value for expectedValue is only there, because
 * the Arduino build process generates prototypes for every function
 * and places these prototypes above the includes, which, in this
 * case, results in a compile error, because of the missing
 * definition of EEPROMInterface. To prevent the automatic
 * prototype generation, it suffices to set one default value of a
 * parameter…
 */
void verify(EEPROMInterface * interface, uint8_t which, uint8_t expectedValue = 0) {

  for (int j = 0; j<et_dataSize; j++) {
    if(!interface->dataAvailable()) {
      Serial.print("Fehler bei: ");
      Serial.print(et_currentLine);
      Serial.println(" daten fehlen!");

    }
    uint8_t read = interface -> getData();
    if(read != expectedValue) {
      Serial.print("Fehler bei Buffer ");
      Serial.print(which);
      Serial.print(" line: ");
      Serial.print(et_currentLine);
      Serial.print(", ");
      Serial.print(j);
      Serial.print(" soll: ");
      Serial.print(expectedValue);
      Serial.print(" is: ");
      Serial.println(read);

    }
    expectedValue++;
  }
}



void doTest(byte et_startValue) {
  // this breaks the test when working with a ATMEL CAT24B512
  // why ever.
  //Tlc.init();


  Serial.println("Los gehts");

  uint8_t b=et_startValue;

  //  write();

  Serial.println("schreiben........");

  uint8_t writeData[et_dataSize];
  for (uint16_t i = 0; i<et_lines; i++) {
    for (int j = 0; j<et_dataSize; j++) {

      writeData[j] = b;
      b++;
    }

    et_if1 -> writeBytesTo(i*et_dataSize, writeData, et_dataSize);
    // Serial.print(".");

  }
  Serial.println("fertig");

  delay(10);
  Serial.println("jetzt lesen...");
  delay(100);
  b=et_startValue;
  for (et_currentLine = 0; et_currentLine < et_lines; et_currentLine++) {
    et_if1->readBytesAt(et_currentLine*et_dataSize, et_dataSize);

    // in real world: do fancy stuff here!
    Serial.print("Warten auf 1.");

    while (!et_if1 -> readingFinished() ) {
      Serial.print(".");
    }

    if (et_currentLine < et_middleLine) {
      et_lowerLine = et_currentLine + et_middleLine;
    } else {
      et_lowerLine = et_currentLine - et_middleLine;
    }

    et_if2 -> readBytesAtNonBlocking(et_lowerLine*et_dataSize, et_dataSize);
    Serial.print("Warten auf 2.");
    while (!et_if2 -> readingFinished() ) {
      Serial.print(".");
    }

    Serial.println("");

    Serial.print("Reading line ");
    Serial.print(et_currentLine);
    Serial.print(" of ");
    Serial.print(et_lines);
    Serial.println(" lines");
    Serial.print("lower line: ");
    Serial.println(et_lowerLine);
    //Serial.println("Verify von buffer 1");
    verify(et_if1, 1, et_startValue + (et_currentLine * et_dataSize));
    //Serial.println("Verify von buffer 2");
    verify(et_if2, 2, et_startValue + (et_lowerLine * et_dataSize));

    Serial.println();
  }


  Serial.println("durch");


}


void eeprom_test() {
  doTest(0x42);
  doTest(0x00);
}


void eeprom_test();

void setup() {
  WireNB.begin();
  Serial.begin(57600);

  eeprom_test();
}

void loop() {
  delay(100000);
}
