// Performance measuring: How long does it take to perform n color changes
#include "Tlc5940.h"

int numleds=64;
long j=0;

void setup() {
  Tlc.init();
  Serial.begin(9600);
}

const int MODE_ALL_ON = 1;
const int MODE_ALL_SELECTIVE = 2;


int mode = MODE_ALL_SELECTIVE;


void loop() {
  if (mode == MODE_ALL_ON) {

    Serial.print("Loop");
    Serial.println(j);
    Serial.println("Aus");
    
    
    setAllLEDs(0);
    Tlc.update();
    delay(700);
    Serial.println("An");
    setAllLEDs(100);
    Tlc.update();
    delay(25000);
  //  mode = MODE_ALL_SELECTIVE;
    j=0;
  } else {
    Serial.print("Loop");
    Serial.println(j);
    Serial.println("Alles aus");

//    setAllLEDs(0);
//    Tlc.update();
    Serial.print("LED ");
    Serial.print(j%numleds);
    Serial.print(" auf an ");
    int color = 0;
    for (int i=0; i<numleds; i++) {
      if (i==j) {
         color = 700; 
      } else {
         color = 0; 
      }
      Tlc.set(i,color);
    }  
 //   Tlc.set(j,500);
    Tlc.update();
    Serial.println("Warten...");
    delay(500);
    
  }
  j++;
  if (j==numleds) {
     j=0; 
     mode = MODE_ALL_ON;
  }

}

void setAllLEDs(int color) {
  for (int i=0; i<numleds; i++) {

    Tlc.set(i,color);
  }  
}



