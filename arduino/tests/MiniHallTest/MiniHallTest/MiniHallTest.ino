// Pin on which the hall-effect sesor is connected
const int HALL_PIN = A2;


void setup() {
  pinMode(HALL_PIN, INPUT); 
  pinMode(13, OUTPUT);
  Serial.begin(115200);
}

void loop() {
  int hallState = digitalRead(HALL_PIN);
  if (hallState == LOW) { 
    Serial.println("low");
    digitalWrite(13, LOW);
  } 
  else {
    Serial.println("high");
    digitalWrite(13, HIGH);
  } 
  delay(100);
}


