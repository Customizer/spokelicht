#include "inttypes.h"
#include <EEPROM.h>
#include <SoftwareSerial.h>
#include "EEPROMNB.h"

#include "EEPROMPictureSource.h"
#include "Settings.h"
#include "EEPROMLineAddresses.h"

Settings settings;
EEPROMPictureSource pictureSource;

void noop() {
  // This is just a dummy method, needed for the initialization of
  // the EEPROM interface
}

void setup() {
  Serial.begin(57600);
  WireNB.begin();
  pictureSource.preLoadData();
}

void loop() {

  EEPROMLineAddresses lineAddresses;
  EEPROMInterface eeprom1(noop /* no callback at this point */);
  EEPROMInterface eeprom2(noop /* no callback at this point */);

  long startTime = micros();

  for (int i = 0; i < settings.lines; i++) {
    pictureSource.loadLine1NonBlocking();

    pictureSource.loadNextLines();
    pictureSource.getRed(0, 0);
  }
  long runtime = micros() - startTime;
  Serial.print("runtime: ");
  Serial.println(runtime);
  delay(300);
}
