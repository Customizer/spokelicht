//writes a single byte to the eeprom an reads it afterwards.
#include "Tlc5940.h"

//#include <Wire.h>
#include "Wire.h"
// PIN 1 is connected to ground. otherwise it woud be 0x50
const uint8_t EEPROM = 0x50 ;
// enable or disable debugging via serial
boolean debug = false;
volatile uint8_t readInProgress = 0;
volatile uint16_t line = 0;

volatile uint8_t loadingUpperLine = 0;

const uint16_t dataSize = 15;

byte dataUpperLine[dataSize];
byte dataLowerLine[dataSize];
byte dataNextUpperLine[dataSize];
byte dataNextLowerLine[dataSize];
const byte startValue=0x40;
const int lines=100;



boolean readBytesAt(uint16_t addr, byte *buffer, int length) {
  if (debug) {
    Serial.print("Lese ");
    Serial.print("Addresse [");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print((int)((addr) & 0xFF), HEX);
    Serial.print("]: ");
  }
  openEEPROMAdress(addr);
  Wire.endTransmission();
  // einzelbyte lesen:
  Wire.requestFrom((int)EEPROM, length);
  for (int i=0; i< length; i++) {
    if (Wire.available()) {
      byte in = Wire.read();
      if (debug) {
        Serial.print(in);
        Serial.print(", ");
      }      
      buffer[i] = in;
    } 
    else {
      return false; 
    }
  }
  if (debug) {
    Serial.println("");
  }
  return true;
}

void readFinished() {
  if (loadingUpperLine) {
    loadingUpperLine = 0;
    uint16_t addr;
    if (line < 50) {
      addr = 50+line;  
    } else {
      addr = 50 - line;
    }
    for (int i=0; i< dataSize; i++) {

        byte in = Wire.read();
        dataNextUpperLine[i] = in;
      }
    
    readBytesAtNonBlocking(EEPROM, dataSize);
  } 
  else {
    loadingUpperLine = 1;
    readInProgress = 0;    
  }
}

void readBytesAtNonBlocking(uint16_t addr, uint8_t length) {
  if (debug) {
    Serial.print("Lese ");
    Serial.print("Addresse [");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print((int)((addr) & 0xFF), HEX);
    Serial.print("]: ");
  }
  openEEPROMAdress(addr);
  Wire.endTransmission();
  // einzelbyte lesen:
  readInProgress=1;
  Wire.requestFromNonBlocking(EEPROM, length, readFinished);

}



void openEEPROMAdress(uint16_t addr) {
  Wire.beginTransmission(EEPROM);
  // Adresse schreiben
  Wire.write((int)((addr >> 8) & 0xFF));
  Wire.write((int)(addr & 0xFF));

}

void writeBytesTo(uint16_t addr, byte * b, byte length) {

  openEEPROMAdress(addr );
  delay(10);
  if (debug) {
    Serial.print("Addresse [");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print((int)((addr) & 0xFF), HEX);
    Serial.print("]: ");

  }
  

  uint16_t i = addr;
  uint16_t page=addr>>7;
  do {
    if (i%8==0) {
      if (i>addr || addr>>7 > page) {
        Wire.endTransmission();
        delay(10);
        openEEPROMAdress(i);
        if (addr>>7 > page){
          if (debug) {

            Serial.println("open next page");
          }
        } 
        else {
          //Serial.println("flush"); 
        }
        delay(10);
      } 

    }
    // einzelbyte schreiben:
    Wire.write(b[i-addr]);
    delay(1);
    if   (debug) {
      Serial.print(b[i-addr]);
      Serial.print(", ");
    }
    i++;
    page=addr>>7;
  } 
  while (i<addr+length);
  if (debug) {

    Serial.println();
  }
  Wire.endTransmission();

}


void reportError(uint16_t i, int j, byte expected, byte actual) {
  Serial.print("Fehler bei: ");
  Serial.print(i);
  Serial.print(", ");
  Serial.print(j);
  Serial.print(" soll: ");
  Serial.print(expected);
  Serial.print(" is: ");
  Serial.println(actual);

}


uint16_t brightnessValues [4] = {
  0,333,666,1000};


void write() {
  byte data[dataSize];
  byte b=startValue;
  Serial.println("schreibe...");
  for (uint16_t i = 0; i<lines; i++) {
    for (int j = 0; j<dataSize; j++) {
      data[j] = b;
      b++;
    }

    writeBytesTo(i*dataSize, data, dataSize);

  }
  Serial.println("Schreiben fertig.");  
}

void setup() {
  
  Wire.begin(); 
  int byteAddress = 0x0;
  Serial.begin(115200);


  Tlc.init();



  
  byte b=startValue;
  //  write();

  delay(10);
  Serial.println("jetzt lesen...");
  long tlcUpdateTime = 0;
  long tlcProgramTime = 0;
  long readTime = 0;
  int tlcUpdateCount = 0;
  long readStart = micros();
  long temp = 0;
  // den ersten blockierend lesen...
  readBytesAt(0,dataUpperLine, dataSize);
  readBytesAt(50,dataLowerLine, dataSize);
  
  for (int rot = 0; rot<5; rot++) {
    b=startValue;
    for (line = 0; line<lines; line++) {
      //perf temp=micros();
      //boolean ok = readBytesAt(line*dataSize,inData, dataSize);
      // Naechste Zeile schon mal lesen.
      uint16_t nextLine = line +1 < lines ? line+1:0;
      readBytesAtNonBlocking(nextLine*dataSize, dataSize);

      //perf readTime+=micros() - temp;
      for (int j = 0; j<dataSize; j++) {
        for (int shift =0; shift < 4; shift++) {
          byte reqBrightUpper = dataUpperLine[j] >> shift & 0x03;
          Tlc.set((j*4)+shift,brightnessValues[reqBrightUpper]);

          byte reqBrightLower = dataLowerLine[j] >> shift & 0x03;
          Tlc.set(32+(j*4)+shift,brightnessValues[reqBrightLower]);

        }





        if(dataUpperLine[j] != b) {
          Serial.println("fehler!");
          reportError(line,j,b,inData[j]);
        } 


        b++;
      }
      

      //perf long tlcUpdateStart = micros();
      Tlc.update(); 
      //perf tlcUpdateTime += micros() - tlcUpdateStart;
      int gewartet = 0;
      while (readInProgress != 0) {
        gewartet =1;
      }
      if (1 == gewartet) {
        Serial.println("EEPROM war zu lahm"); 
      }

      for (int i=0; i< dataSize; i++) {

        byte in = Wire.read();
        dataNextLowerLine[i] = in;
      }

    }
  }
  long time = micros() - readStart;
  Serial.println();
  Serial.print("fertig nach ");
  Serial.print(time);
  Serial.print(" micros. TLC update time");
  Serial.print(tlcUpdateTime);
  Serial.print(" micros. TLC program time");
  Serial.print(tlcProgramTime);
  Serial.print(" micros. Read time");
  Serial.print(readTime);
  Serial.print(" micros");
  delay(1000);

}



void loop() {
  delay(10000); 
}

















