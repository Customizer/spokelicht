// Cycling all LED colors
#include "Tlc5940.h"



int counter = -1;

void setup() {
  Tlc.init();
  Serial.begin(57600);
}

void loop() {

  Tlc.clear();
  Tlc.update();
  delay(10);

  setLEDs(counter);
  Tlc.update();

  counter++;
  if (counter == 3)
  {
    counter = 0;
  }

  delay(1000);
}

int numleds=40;

// The startIndex defines the color of the LED's light.
// Valid values are 0, 1 and 2 (representing red, green and blue;
// not necessarily in that order)
void setLEDs(int color) {
  int offset = 0;
  int color0 = 0;
  int color1 = 0;
  int color2 = 0;

  switch (color)
  {
    case 0:
      color0 = 1000;
      break;
    case 1:
      color1 = 1000;
      break;
    case 2:
      color2 = 1000;
      break;
    default:
      break;
  }

  for (int i = 0; i < numleds; i++) {
    if (i == 10 || i == 20 || i == 30)
    {
      offset += 2;
    }
    Tlc.set(i * 3 + offset, color0);
    Tlc.set(i * 3 + offset + 1, color1);
    Tlc.set(i * 3 + offset + 2, color2);
  }
}




