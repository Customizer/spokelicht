// This is the counterpart for the JUnit Test SerialConnectionTest of the
// ImageConverter.

#include <Wire.h>

const byte EEPROM_SLAVE_ADDRESS = 0x50;
const int EEPROM_CHILLOUT_TIME = 3;

void openEEPROMAdress(uint16_t addr) {
  Wire.beginTransmission(EEPROM_SLAVE_ADDRESS);
  Wire.write((int)((addr >> 8) & 0xFF));
  Wire.write((int)(addr & 0xFF));
}

void writeBytesTo(uint16_t addr, byte * b, byte length) {
  openEEPROMAdress(addr);

  uint16_t i = addr;
  uint16_t page=addr>>7;
  do {
    if (i%30==0) {
      if (i>addr || addr>>7 > page) {
        Wire.endTransmission();
        delay(EEPROM_CHILLOUT_TIME);
        openEEPROMAdress(i);
      } 
    }

    Wire.write(b[i-addr]);
    i++;
    page=addr>>7;
  } 
  while (i<addr+length);

  Wire.endTransmission();
  delay(EEPROM_CHILLOUT_TIME);
}

boolean readBytesAt(uint16_t addr, byte *buffer, int length) {
  openEEPROMAdress(addr);
  Wire.endTransmission();

  // einzelbyte lesen:
  Wire.requestFrom((int)EEPROM_SLAVE_ADDRESS, length);
  for (int i=0; i< length; i++) {
    if (Wire.available()) {
      byte in = Wire.read();
      buffer[i] = in;
    } 
    else {
      return false; 
    }
  }
  return true;
}


int startAddress = 0;
int addressPointer = 0;
int counter = startAddress;
int arrayCounter = 0;
int byteCount = 32000;//472;
int pageCounter = 0;

const int LENGTH = 32;

uint8_t writeBuffer[LENGTH];
uint8_t readBuffer[LENGTH];

int readAddress = 0;

// waits ms for data to be available. if no data is received within ms, blinks the LED and waits forever. 
void waitForData(int ms) {
   int wait = 0;
     // wait for data...
    for (wait = 0; wait < ms && !Serial.available(); wait++) {
      delay (1);
    }
    if (wait == ms) {
      // receive timeout
      while (true) {
        digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
        delay(1000);              // wait for a second
        digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
        delay(1000); 
      }
    }  
 
}

void setup() {
  pinMode(13, OUTPUT);
  Wire.begin(); 
  Serial.begin(9600);

  byte buffer[LENGTH];
  int limit = startAddress + byteCount;
   

  waitForData(10000);
  byte chksum =0;
  byte in  = 0;
  int i = 0;
  int writePointer=0;
  int bytesRead = 0;
  for (i = 0; i<byteCount; i++) {
    if (i >= LENGTH && i % LENGTH == 0)
    {
      writeBytesTo(writePointer, writeBuffer, bytesRead);
      writePointer += bytesRead;
      Serial.write(chksum);
      
      chksum = 0;
      bytesRead = 0;
    }
    waitForData(1000);
    in = Serial.read();
    writeBuffer[i%LENGTH] = in;
    chksum ^= in;
    bytesRead++;
   
  }
  writeBytesTo(writePointer, writeBuffer, bytesRead);
  Serial.write(chksum);

  
  
  
  delay(1000);
  for (int i = 0; i < byteCount; i+= LENGTH)
  { // Last chunk may not be divisable by 32.
    int readLength = byteCount - i < LENGTH ? byteCount - i : LENGTH;
    readBytesAt(i, buffer, readLength);
    Serial.write(buffer, readLength);
  }




}

void loop() {
  delay(1000);
}


