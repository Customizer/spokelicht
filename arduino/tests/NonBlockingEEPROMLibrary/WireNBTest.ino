#ifdef WIRE_NB_TEST
#include "WireNB.h"
uint8_t wnbt_debug = 0;


uint8_t EEPROM =0x50;
void openEEPROMAdress(uint16_t addr) {
  WireNB.beginTransmission(EEPROM);
  // Adresse schreiben
  uint8_t ret = WireNB.write((int)((addr >> 8) & 0xFF));
  if (ret != RET_OK) {
     Serial.print("write 1 failed "); 
     Serial.println(ret);
  }
  ret = WireNB.write((int)(addr & 0xFF)); 
 if (ret != RET_OK) {
     Serial.print("Write 2 failed "); 
     Serial.println(ret);
  }
}
void writeBytesTo(uint16_t addr, byte * b, byte length) {
  openEEPROMAdress(addr );
  if (wnbt_debug) {
    Serial.print("Addresse [");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print((int)((addr) & 0xFF), HEX);
    Serial.print("]: ");

  }
  /*  if (wnbt_debug) {
   Serial.print("Addresse 2:");
   Serial.println((int)((addr) & 0xFF), HEX);
   }*/

  uint16_t i = addr;
  uint16_t page=addr>>7;
  do {
    if (i%32==0) {
      if (i>addr || addr>>7 > page) {
        WireNB.endTransmission();
        delay(3);
        openEEPROMAdress(i);
        if (addr>>7 > page){
          if (wnbt_debug) {

            Serial.println("open next page");
          }
        } 
        else {
          //Serial.println("flush"); 
        }
        
      } 

    }
    // einzelbyte schreiben:
    WireNB.write(b[i-addr]);
    if   (wnbt_debug) {
      Serial.print(b[i-addr]);
      Serial.print(", ");
    }
    i++;
    page=addr>>7;
  } 
  while (i<addr+length);
  if (wnbt_debug) {

    Serial.println();
  }
  WireNB.endTransmission();
  delay(3);
}



boolean readBytesAt(uint16_t addr, byte *buffer, int length) {
  if (wnbt_debug) {
    Serial.print("Lese ");
    Serial.print("Addresse [");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print((int)((addr) & 0xFF), HEX);
    Serial.print("]: ");
  }
  openEEPROMAdress(addr);
  uint8_t ret = WireNB.endTransmission();
  if (ret != RET_OK) {
     Serial.print("Transmission failed "); 
     Serial.println(ret);
  }
  //delay(1);
  // einzelbyte lesen:
  ret = WireNB.requestFrom((int)EEPROM, length);
  if (ret != length) {
     Serial.print("Request failed "); 
     Serial.println(ret);
  }
  
    Serial.println();
  
  for (int i=0; i< length; i++) {
    if (WireNB.available()) {
      byte in = WireNB.read();
      if (wnbt_debug) {
        Serial.print(in);
        Serial.print(", ");
      }      
      buffer[i] = in;
      //  delay(1);    
    } 
    else {
      return false; 
    }
    // delay(1);
  }
  if (wnbt_debug) {

    Serial.println("");
  }
  return true;
}

void wirenb_test() {
 uint16_t dataSize = 15;



 byte startValue=0x00;
 int lines=100;
  WireNB.begin(); 
  int byteAddress = 0x0;
  Serial.begin(115200);


  Serial.println("Los gehts");

  uint8_t b=startValue;
  //  write();
  
  Serial.println("schreiben...");
  
  uint8_t writeData[dataSize]; 
  for (uint16_t i = 0; i<lines; i++) {
    for (int j = 0; j<dataSize; j++) {

      writeData[j] = b;
      b++;
    }

    writeBytesTo(i*dataSize, writeData, dataSize);

  }
  Serial.println("fertig");


  byte inData[dataSize];
    b=startValue;
    for (uint16_t i = 0; i<lines; i++) {
      
      boolean ok = readBytesAt(i*dataSize,inData, dataSize);

      for (int j = 0; j<dataSize; j++) {
        Serial.print(inData[j]);
        Serial.print(", ");
        
        if(inData[j] != b) {
          Serial.print("Fehler bei: ");
          Serial.print(i);
          Serial.print(", ");
          Serial.print(j);
          Serial.print(" soll: ");
          Serial.print(b);
          Serial.print(" is: ");
          Serial.println(inData[j]);

        } 
        b++;
      }
      Serial.println();
      if(!ok) {
        Serial.println();
        Serial.print("Uebertragungsfehler: ");
        Serial.print(i);
        Serial.println();
      } 
      else {
        //Serial.print("."); 
      }
    }

}



#endif
