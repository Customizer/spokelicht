      #include "WireNB.h"
#include "Arduino.h"
#include "EEPROMNB.h"
#include"inttypes.h"

#define RET_NO_DATA 0;
#define RET_OK 1;
#define RET_READ_INCOMPLETE 3;

const uint8_t EEPROMAddress = 0x50 ;


// ===============================================================
// Constructor 
// ===============================================================

//
// para onRequestComplete must be a pointer to a function. This function will be executed inside an intrerupt handler and must
// therefore not trigger any wire activity!
//
EEPROMInterface::EEPROMInterface(void (*onRequestComplete) (void))
{
  user_onRequestComplete=onRequestComplete;
  lineBuffer = new TwoWireNB();
  lineBuffer -> onRequestComplete(onRequestComplete);
  
}


// ===============================================================
// private functions 
// ===============================================================
//

//
void EEPROMInterface::openEEPROMAddress(uint16_t addr) {
  lineBuffer -> beginTransmission(EEPROMAddress);
  // Adresse schreiben
  lineBuffer -> write((int)((addr >> 8) & 0xFF));
  lineBuffer -> write((int)(addr & 0xFF));

}

// ===============================================================
// public functions 
// ===============================================================

/**
 * returns RET_OK if an unread byte in buffer whichBuffer exists. If no byte is available RET_NO_DATA is returned. 
 * 
 */
uint8_t EEPROMInterface::dataAvailable() {
  return lineBuffer -> available();
  
}
//
// ca be called after read. Gives the next byte read, but not yet consumed.
//
uint8_t EEPROMInterface::getData() {
   return lineBuffer -> read(); 
}

/**
 * Performs a blocking read in the buffer whichBuffer. 
 */
uint8_t EEPROMInterface::readBytesAt(uint16_t addr, uint8_t length) {
  openEEPROMAddress(addr);
  lineBuffer -> endTransmission();
  // request the bytes. This will trigger the communication:
  uint8_t read = lineBuffer -> requestFrom(EEPROMAddress, length);
  if (read != length) {
     return RET_READ_INCOMPLETE; 
  }
  return RET_OK;
}


//
// reads length bytes from adress addr. After transmission onRequestComplete is invoked.
//
uint8_t EEPROMInterface::readBytesAtNonBlocking(uint16_t addr, uint8_t length) {
  openEEPROMAddress(addr);
  lineBuffer -> endTransmission();
  return lineBuffer -> requestFromNonBlocking(EEPROMAddress, length);

}

//
// returns 1 if reading is done and new commands can be accepted.
//
uint8_t EEPROMInterface::readingFinished() {
  return lineBuffer -> isRequestComplete(); 
}

/**
 * Writes lengh bytes from b to EEPROM-adress addr.
 */
void EEPROMInterface::writeBytesTo(uint16_t addr, byte * b, byte length) {

  openEEPROMAddress(addr );
 

  uint16_t i = addr;
  uint16_t page=addr>>7;
  do {
    if (i%32==0) {
      if (i>addr || addr>>7 > page) {
        lineBuffer -> endTransmission();
        delay(3);
        openEEPROMAddress(i);
      } 

    }
    // einzelbyte schreiben:
    lineBuffer -> write(b[i-addr]);
    delay(1);
    i++;
    page=addr>>7;
  } 
  while (i<addr+length);
  lineBuffer -> endTransmission();
  delay(3);
}



