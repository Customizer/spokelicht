
/*
  TwoWireNB.cpp - TWI/I2C library for Wiring & Arduino
 Copyright (c) 2006 Nicholas Zambetti.  All right reserved.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

extern "C" {
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include "twinb.h"
}


#include "WireNB.h"
// Initialize Class Variables //////////////////////////////////////////////////
uint8_t TwoWireNB::initialized = 0;
TwoWireNB * transmitting;


// Constructors ////////////////////////////////////////////////////////////////

TwoWireNB::TwoWireNB()
{
  // txBuffer = new uint8_t[BUFFER_LENGTH]
  for (uint8_t i = 0; i< BUFFER_LENGTH; i++) {
    txBuffer[i]=0;
    rxBuffer[i]=0;

  }

}


// Private Methods /////////////////////////////////////////////////////////////

//
// See description at transmissionCompleteCallback
//
void transmissionComplete() {
  transmitting -> transmissionCompleteCallback();

}

// Public Methods //////////////////////////////////////////////////////////////
//
// Must not be called from the outside. At this point I do not know how to pass a funtion pointer to an instance method to C code. So I wrote a plain C function which
// invokes this once Reading is done.
//
void TwoWireNB::transmissionCompleteCallback() {
  rxBufferIndex = 0;
  rxBufferLength = twi_getReceivedBytesCount();
  transmitting = 0; 

  if (user_onRequestComplete!=0 ) {
    user_onRequestComplete();
  }
    
}

void TwoWireNB::begin(void)
{
  rxBufferIndex = 0;
  rxBufferLength = 0;

  txBufferIndex = 0;
  txBufferLength = 0;

  twi_init();
  initialized = 1;
}

uint8_t TwoWireNB::isRequestComplete() {
   return transmitting == 0; 
}

void TwoWireNB::begin(uint8_t address)
{
  twi_setAddress(address);
  begin();
}

void TwoWireNB::begin(int address)
{
  begin((uint8_t)address);
}

//
// requests quantity bytes from the device at address. this is a blocking call. Returns the number of bytes read.
//

uint8_t TwoWireNB::requestFrom(uint8_t address, uint8_t quantity)
{

  if (transmitting) {
    return 0; 
  }

  // clamp to buffer length
  if(quantity > BUFFER_LENGTH){
    quantity = BUFFER_LENGTH;
  }
  transmitting = this;
  // perform blocking read into buffer

  uint8_t read = twi_readFrom(address, rxBuffer, quantity);

  transmitting = 0;

  // set rx buffer iterator vars
  rxBufferIndex = 0;
  rxBufferLength = read;

  return read;
}

//
// requests quantity bytes from the device at address. Once finished the onRequestComplete listener function is called.
// returns a RET_* status.
//
uint8_t TwoWireNB::requestFromNonBlocking(uint8_t address, uint8_t quantity )
{
  // return an error if length is too long
  if(quantity > BUFFER_LENGTH){
    return RET_REQUEST_TOO_LONG;
  }
  if (transmitting) {
    return RET_TRANSMISSION_IN_PROGRESS; 
  }

  rxBufferIndex = 0;
  // indicate that we are transmitting
  transmitting = this;

  // perform non blocking read into buffer
  twi_readFromNonBlocking(address,rxBuffer,quantity, transmissionComplete);
  return RET_OK;

}



void TwoWireNB::beginTransmission(uint8_t address)
{
  // set address of targeted slave
  txAddress = address;
  // reset tx buffer iterator vars
  txBufferIndex = 0;
  txBufferLength = 0;
}


//
// Actually writes the collected data from the writeBuffer to the wire.
//
uint8_t TwoWireNB::endTransmission(void)
{
  // if a transmission is ongoing, return 0;
  if (transmitting) {
    return RET_TRANSMISSION_IN_PROGRESS; 
  }

  // indicate that we are transmitting
  transmitting = this;
  // transmit buffer (blocking)
  int8_t ret = twi_writeTo(txAddress, txBuffer, txBufferLength, 1);
  // indicate that we are done transmitting
  transmitting = 0;
  // reset tx buffer iterator vars
  txBufferIndex = 0;
  txBufferLength = 0;

  // : ret ist nicht so gut. konstanten...
  return ret == 0 ? RET_OK : ret;
}

// must be called in:
// slave tx event callback
// or after beginTransmission(address)
uint8_t TwoWireNB::write(uint8_t data)
{
  if(txBufferLength >= BUFFER_LENGTH){
    // TODO setWriteError();
    return 0;
  }
  // put byte in tx buffer
  txBuffer[txBufferIndex] = data;
  ++txBufferIndex;
  // update amount in buffer   
  txBufferLength = txBufferIndex;
  return RET_OK;
}

// must be called in:
// slave tx event callback
// or after beginTransmission(address)
uint8_t TwoWireNB::write(const uint8_t *data, uint8_t quantity)
{
  // in master transmitter mode
  for(size_t i = 0; i < quantity; ++i){
    write(data[i]);
  }
  return quantity;
}

// must be called in:
// slave rx event callback
// or after requestFrom(address, numBytes)
uint8_t TwoWireNB::available(void)
{
  return rxBufferLength - rxBufferIndex;
}


// sets function called after a successful read
// This function _must not_ trigger new TWI activity. This will most likely fail!
void TwoWireNB::onRequestComplete( void (*function)(void) )
{
  user_onRequestComplete = function;
}

/**
 * Returns a Byte from the buffer. If the buffer is empty 0 is returned. 
 */
uint8_t TwoWireNB::read(void)
{
  uint8_t value = 0;

  // get each successive byte on each call
  if(rxBufferIndex < rxBufferLength){
    value = rxBuffer[rxBufferIndex];
    ++rxBufferIndex;
  }

  return value;
}


// Preinstantiate Objects //////////////////////////////////////////////////////

TwoWireNB WireNB = TwoWireNB();


