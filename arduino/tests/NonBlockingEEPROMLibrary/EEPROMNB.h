#include "WireNB.h"
#include "inttypes.h"
#ifndef EEPROMNB 
#define EEPROMNB

class EEPROMInterface 
{
  // Members
  TwoWireNB * lineBuffer;
  void (*user_onRequestComplete)(void);
 

  private:
    void openEEPROMAddress(uint16_t addr);
  public:
    uint8_t readingFinished();
    EEPROMInterface(void (*onRequestComplete) (void) );
    uint8_t dataAvailable();
    uint8_t getData();
    uint8_t readBytesAt(uint16_t , uint8_t );
    uint8_t readBytesAtNonBlocking(uint16_t , uint8_t );
    void writeBytesTo(uint16_t , uint8_t * , uint8_t );
};
#endif
