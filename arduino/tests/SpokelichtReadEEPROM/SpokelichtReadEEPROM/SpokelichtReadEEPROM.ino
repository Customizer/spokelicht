#include "WireNB.h"
#include "EEPROMNB.h"

void ret_nop() {
}
EEPROMInterface * ret_if1 = new EEPROMInterface(ret_nop);
int ret_byteCount = 472;

void read_eeprom_test() {
  WireNB.begin();
  Serial.begin(57600);
  Serial.println("los gehts");
  for (int i = 0; i<472; i+=32) {

    ret_if1->readBytesAt(i, 32);
    while(ret_if1 -> dataAvailable()) {
      uint8_t inByte = ret_if1 -> getData();
      Serial.print(inByte,BIN);
    }
  }
}

void setup() {
  read_eeprom_test();
}
void loop() {
  delay(100);
}
