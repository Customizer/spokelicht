#include "Tlc5940.h"
void setup() {
   Tlc.init();
   Serial.begin(9600);
   
}

uint16_t r = 0;
uint16_t g = 0;
uint16_t b = 0;
int add = 10;
void loop() {
  if (Serial.available()) {
     byte in = Serial.read();
     if ('+' == in) {
       add = 10;
       
     } else if ('-' == in) {
       add = -10;
     } else if ('r' == in) {
       r+=add;
    } else if ('g' == in) {
       g+=add;
    } else if ('b' == in) {
       b+=add;
    }
    Serial.print("r ");
    Serial.print(r);
    Serial.print(" g ");
    Serial.print(g);
    Serial.print(" b ");
    Serial.print(b);
    Serial.print(" add ");
    Serial.println(add, DEC);

  }
  Tlc.set(0, b);
  Tlc.set(1, 0);
  Tlc.set(2, 0);
  Tlc.set(3, 0);
  Tlc.set(4, r);
  Tlc.set(5, 0);
  Tlc.set(6, 0);
  Tlc.set(7, 0);
  Tlc.set(8, g);
  Tlc.set(9, b);
  Tlc.set(10, r);
  Tlc.set(11, g);

  Tlc.update();
  delay(10);
  
}
