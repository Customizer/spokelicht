// Pin on which the hall-effect sesor is connected
#include "Tlc5940.h"
#define INTERRUPT
const int HALL_PIN = A2;

void customSetup();

void setup() {
  Tlc.init();

  pinMode(HALL_PIN, INPUT);     
  Serial.begin(115200);
  customSetup();
}

#ifdef INTERRUPT
volatile uint8_t state = 0;
volatile uint8_t state_b1 = 0;
volatile uint8_t state_b2 = 0;
volatile int intCount = 0;
volatile int intCount_b1 = 0;
volatile int intCount_b2 = 0;
volatile uint8_t change = 0;

ISR(PCINT1_vect) {
 // state = digitalRead(HALL_PIN);
  state = (PINC & 0x04) != 0;
  intCount++;
  change++;
}
/*ISR(PCINT0_vect) {
  state = digitalRead(HALL_PIN);
  intCount++;
}*/
ISR(PCINT2_vect) {
  noInterrupts();
  uint8_t stateTemp = digitalRead(6);
  
  state_b1 = digitalRead(6);
  intCount_b1++;
  change++;
  interrupts();
}

void customSetup() {
  interrupts();
  // Pin Change mask Register: Bit 2 is PCINT10, which is A2 which is connected to the halleffect sensor.
  PCMSK1 |= _BV(PCINT10);
  // Enable Button interrupts 
  PCMSK2 |= _BV(PCINT22);
  PCMSK2 |= _BV(PCINT23);
  // enable the intrrupt PCINT14:8
  PCICR |= _BV(PCIE1);
  // enable the intrrupt PCINT23:6
  PCICR |= _BV(PCIE2);

}
volatile uint8_t lastChange = 0;;
void loop() {
  
 if (lastChange != change) {
   
  
  Serial.print("State: ");
  Serial.print(state);
  Serial.print("port B: ");
  Serial.print(PORTB);
  Serial.print("port C: ");
  Serial.print(PINC);
  Serial.print("port D: ");
  Serial.print(PORTD);


  Serial.print(" intCount ");
  Serial.print(intCount);
  Serial.print(" State b1: ");
  Serial.print(state_b1);

  Serial.print(" intCount ");
  Serial.print(intCount_b1);

  Serial.print(" change ");
  Serial.print( change);
  Serial.print(" lastchange ");
  Serial.println( lastChange);
   lastChange = change;
 } 
}

#endif

#ifdef POLLING

void customSetup() {

}

void loop() {

  int hallState = digitalRead(HALL_PIN);
  if (hallState == LOW) { 
    Tlc.set(0,0);
    Tlc.update();
    Serial.println("low");
  } 
  else {
    Serial.println("high");
    Tlc.set(0,2000);
    Tlc.update();

  } 
  delay(100);
}


#endif


