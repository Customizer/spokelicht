
#define TLC 
#if defined(TLC)
#include "Tlc5940.h"
#endif


#include "SPI.h"
#include "LPD8806.h"

// how many LEDs are connected?
const int LED_COUNT = 60;

#if defined(HARDWARE_SPI) 
  // You can optionally use hardware SPI for faster writes, just leave out
  // the data and clock pin parameters.
  LPD8806 strip = LPD8806(LED_COUNT);
#else
  // for software SPI only:
  int clockPin = 7;
  int dataPin = 8;

  LPD8806 strip = LPD8806(LED_COUNT, dataPin,clockPin);
#endif




// Pin on which the hall-effect sesor is connected
int HALL_PIN = A2;
// how many measures until a reported HIGH value from the hall-effect sensor is considered to be 
// a real hit (to prevent noise)
int HALL_THRESHOLD = 50;

// total number of lines to be displayed
const int LINES=12;

// symbolic array of line-values. 
int data[LINES][LED_COUNT];
/*= 
 {
 {
 4000,0,0,0  }
 ,
 {
 0,4000,0,0  }
 ,
 {
 0,0,4000,0  }
 ,
 {
 0,0,0,4000  }
 ,
 
 };*/

void setup() {
  pinMode(HALL_PIN, INPUT);     

  setupLEDs();

  Serial.begin(115200);

  for (int i = 0; i<LED_COUNT;i++) {
    for (int j=0; j <LED_COUNT;j++) {
      int value = 0;
      if (j==i) {
        value =500;
      }
      data[i][j] = value;
    } 
  }

}

#if defined(TLC)
void setupLEDs() {
    Tlc.init();
}

void displayLine(int linedata[]) {
  for (int i=0; i< LED_COUNT; i++ ) {      
      Tlc.set(i+1,linedata[i]); 
    }
    //Serial.println();
    Tlc.update(); 
}
#endif

#if defined(LPD)
void setupLEDs() {
  
   strip.begin();
   strip.show();
}

void displayLine(int linedata[]) {
    for (int i=0; i< LED_COUNT; i++ ) { 
       if (linedata[] >0) {     
          strip.setPixelColor(i,120,120,120); 
       }
    }
    strip.show(); 
}
#endif


// Duration of the last Rotation. Initially set to 1
long rotationDuration = 1;
// The loop counter
long loopCount = 0; 
// the pointer to the line currently to be displayed
int line = 0;
// how long would one duration be?
int durationPerLine = rotationDuration/LINES;
// how often was the magnet reported to be high? 
// see below...
int hallCount = 0;
// state of the hall-sensor
int hallState = 0;

// how often did we (presumably) miss the data from the magnet?
int misses = 0;
int lastLine = -1;
void loop() {
  if (loopCount %10 ==0) {
    debug();

  }
  if (lastLine != line) {
    displayLine(data[line]);
//    for (int i=0; i< LED_COUNT; i++ ) {      
 //     Tlc.set(i+1,data[line]); 
  //  }
    //Serial.println();
    Tlc.update(); 
    lastLine=line;
  }
  // if the loop-count exceeds the duration of current line time duration per line the next
  // line is the one to choose.
  if ((loopCount % rotationDuration) > (durationPerLine * (line+1))) { 
    line++;
  }  
  // since a tick from the sensor coud be missed, after having exceeded the duration of the last 
  // rotation, start over from the beginning.
  if ((loopCount % rotationDuration ) == 0 || line == LINES) {
    // Serial.println("langsamer geworden");
    line=0;
  } 
  // write data to the Tlc.


  hallState = digitalRead(5);
  if (hallState == LOW) { 
    // increase the counter how often we measured the magnet to be close to the sensor.
    hallCount ++;
    //Serial.println("high");
  }  
  else {
    // if the sensor does not report the magnet to be present and we exceeded the threshold,
    // we consider this as a 'hit'  
    if (hallCount > HALL_THRESHOLD) {
      // if this is the first time we hit the sensor or if the loopCount is this loop was not longer
      // than twice the length of the loop before, reset the rotationDuration.
      // That is keep the speed stable in case a rotation is missed.
      // In order not to get stuck after the 5th miss the duration is reset anyway.  
      if (rotationDuration == 1 || loopCount < rotationDuration*2 || misses == 5) {
        rotationDuration=loopCount;
        misses == 0;
      } 
      else {
        // Serial.println("Oooops missed one rotation??"); 
        // debug();
        misses++;
      }    
      durationPerLine = rotationDuration  / LINES;
      loopCount = 0;  
      line = 0; 
      /*  Serial.print("mag detected new Duration: ");
       Serial.print(rotationDuration);
       Serial.print(" mag duration ");
       Serial.println(hallCount);
       */
    } 

    hallCount=0; 
  }
  //  delay(1); 

  loopCount++;
  //Serial.println(line);
}

void debug () {
  Serial.print("Line: ");
  Serial.print( line );
  Serial.print( " hallCount: ");
  Serial.print( hallCount );
  Serial.print( " loopCount: ");
  Serial.print(loopCount);
  Serial.print("modulo " );
  Serial.print((loopCount % rotationDuration ));
  Serial.print(" thresh ");
  Serial.print(durationPerLine * (line+1));
  Serial.print(" rotDur ");
  Serial.println(rotationDuration); 
}




