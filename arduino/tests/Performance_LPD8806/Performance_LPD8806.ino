#include "LPD8806.h"
#include "SPI.h" 

"
// Performance measuring: How long does it take to perform n color changes with
// hardware of with software SPI

/*****************************************************************************/

// Number of RGB LEDs in strand:
int nLEDs = 40;

//#define HARDWARE_SPI true

// defined: yellow cabel/Clock on 7 ; blue / Data on 8
// not defined: yellow cable/Clock on 13 ; blue cable / Data on 11
// dont't get confused: The strip will light up constantly if  
// HARDWARE_SPI is not defined but the cables are connected to 
// hardware-spi ports!


// First parameter is the number of LEDs in the strand.  The LED strips
// are 32 LEDs per meter but you can extend or cut the strip.  Next two
// parameters are SPI data and clock pins:
#if defined(HARDWARE_SPI) 
  // You can optionally use hardware SPI for faster writes, just leave out
  // the data and clock pin parameters.
  LPD8806 strip = LPD8806(nLEDs);
#else
  // for software SPI only:
  int clockPin = 7;
  int dataPin = 8;

  LPD8806 strip = LPD8806(nLEDs, dataPin,clockPin);
#endif

// Number of LEDs on the strip
int numleds=40;

void setAllLedsTo(int r,int g,int b) {
  for (int i=0; i<numleds; i++) {

    strip.setPixelColor(i,r,g,b);
  }
}


void setup() {
  // Start up the LED strip
  strip.begin();
  // Update the strip, to start they are all 'off'
  strip.show();

}

int j=0;

void loop() {
  if(j<10000){
    setAllLedsTo(j%255,j%255,j%255);
    j++;
  } 
  else {
    setAllLedsTo(0,0,0);

  }
  strip.show();
}






