#include <avr/wdt.h>

void setup() {
  Serial.begin(57600); // Match the speed expected by the bootloader
  Serial.println("rebooted");
}

int counter = 0;

void loop() {
  Serial.println(counter);
  counter++;
  delay(1000);
}


void serialEvent() {
  if (!Serial.available()) {
    return;
  }
  char cmd = Serial.read();
  if (cmd == 'r') {
    wdt_enable(WDTO_15MS);
  }

}
