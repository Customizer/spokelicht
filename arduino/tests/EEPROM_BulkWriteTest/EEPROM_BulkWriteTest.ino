// This is the counterpart for the JUnit Test SerialConnectionTest of the
// ImageConverter.

#include <Wire.h>

const byte EEPROM_SLAVE_ADDRESS = 0x50;
const int EEPROM_CHILLOUT_TIME = 5;

void writeBytesTo(int addr, uint8_t* bytes, int length) {
	Wire.beginTransmission(EEPROM_SLAVE_ADDRESS);

	// write address
	Wire.write((addr >> 8) & 0xFF);
	Wire.write((int)((addr) & 0xFF));

	// write bytes
	Wire.write(bytes, length);
	Wire.endTransmission();
	
	// give the EEPROM some time to relax
	delay(EEPROM_CHILLOUT_TIME);
}

byte readByteAt(int addr) {
	Wire.beginTransmission(EEPROM_SLAVE_ADDRESS);
	// write address
	Wire.write((int)((addr >> 8) & 0xFF));
	Wire.write((int)(addr & 0xFF));
	Wire.endTransmission();

	// read one byte
	Wire.requestFrom(EEPROM_SLAVE_ADDRESS, (byte)1);
	if (Wire.available()){
		byte readByte = Wire.read();
		return readByte;
	} 
	else {
		return 0x00;
	}
}

boolean readBytesAt(unsigned int addr, uint8_t *buffer, int length) {
  Wire.beginTransmission(EEPROM_SLAVE_ADDRESS);
  // write address
  Wire.write((int)((addr >> 8) & 0xFF));
  Wire.write((int)(addr & 0xFF));
  Wire.endTransmission();

  // read bytes
  Wire.requestFrom((int)EEPROM_SLAVE_ADDRESS, length+1);
  for (int i=0; i< length; i++) {
    if (Wire.available()) {
      buffer[i] = Wire.read();    
    } else {
       return false; 
    }
	
	// give the EEPROM some time to relax
	//delay(EEPROM_CHILLOUT_TIME);
  }
  return true;
}

void setup() {
	Wire.begin(); 
	Serial.begin(9600);
}

int startAddress = 0;
int addressPointer = 0;
int counter = startAddress;
int arrayCounter = 0;
int byteCount = 200;
int pageCounter = 0;

const int LENGTH = 30;

uint8_t writeBuffer[LENGTH];
uint8_t readBuffer[LENGTH];

int readAddress = 0;

int calcPageAddress(int pageNr)
{
	if (pageNr == 0)
	{
		return 0;
	}
	int sum = 0;
	for (int i = 0; i <= pageNr - 1; i++)
	{
		sum += pow(2, i);
	}
	return pow(2, 7) * sum;
}

void writeByteTo(int addr, byte b) {
  Wire.beginTransmission(EEPROM_SLAVE_ADDRESS);

  // Adresse schreiben
  Wire.write((addr >> 8) & 0xFF);

  Wire.write((int)((addr) & 0xFF));

  // einzelbyte schreiben:
  Wire.write(b);
  Wire.endTransmission();
	// give the EEPROM some time to relax
	delay(EEPROM_CHILLOUT_TIME);
}

int seed = 5;

void loop() {
	int limit = startAddress + byteCount;
	if (counter < limit)
	{
		int value = 0;
		for (int i = 0; i < limit; i++)
		{		
			writeBuffer[arrayCounter] = (uint8_t) value + seed;
			arrayCounter++;
			counter++;

			if (counter % LENGTH == 0)
			{
				writeBytesTo(calcPageAddress(pageCounter), writeBuffer, LENGTH);
				for (int j = 0; j < LENGTH; j++)
				{
					int address = calcPageAddress(pageCounter) + j;
				}
				arrayCounter = 0;
				pageCounter++;
			}
			value++;
		}
	}
	if (counter == limit)
	{
		Serial.println("Limit reached");
		
		int expectedValue = 0 + seed;
		
		for (int i = 0; i < pageCounter; i++)
		{
			int pageAddress = calcPageAddress(i);
			readBytesAt(pageAddress, readBuffer, LENGTH);
			for (int j = 0; j < LENGTH; j++)
			{
				byte readByte = readBuffer[j];
				int address = pageAddress + j;
				if (expectedValue == readByte)
				{
					Serial.print("match at address: ");
					Serial.print(address);
					Serial.print("    value: ");
					Serial.println(readByte);
				}
				else
				{
					Serial.print("mismatch at address: ");
					Serial.print(address);
					Serial.print("   expected:   ");
					Serial.print(expectedValue);
					Serial.print("    read:   ");
					Serial.println(readByte);


					counter++;
					return;
				}

				expectedValue++;					
			}
		}
		counter++;
	}
}