//writes a single byte to the eeprom an reads it afterwards.

#include <Wire.h>
// PIN 1 is connected to ground. otherwise it woud be 0x50
const byte EEPROM = 0x50;
// enable or disable debugging via serial
boolean debug = false;

void openEEPROMAdress(uint16_t addr) {
  Wire.beginTransmission(EEPROM);
  // Adresse schreiben
  Wire.write((int)((addr >> 8) & 0xFF));
  Wire.write((int)(addr & 0xFF)); 
 
}

void writeByteTo(int addr, byte b) {
  Wire.beginTransmission(EEPROM);

  // Adresse schreiben
  Wire.write((addr >> 8) & 0xFF);
  if (debug) {
    Serial.print("Addresse 1:");
    Serial.println((addr >> 8) & 0xFF, HEX);
  }
  Wire.write((int)((addr) & 0xFF));
  if (debug) {
    Serial.print("Addresse 2:");
    Serial.println((int)((addr) & 0xFF), HEX);
  }
  // einzelbyte schreiben:
  Wire.write(b);
  Wire.endTransmission();

}

byte readByteAt(int addr) {
  if (debug) {
    Serial.print("Lese ");
    Serial.println(addr,HEX);
  }
  Wire.beginTransmission(EEPROM);
  // Adresse schreiben
  Wire.write((int)((addr >> 8) & 0xFF));
  Wire.write((int)(addr & 0xFF));
  Wire.endTransmission();

  // einzelbyte lesen:
  Wire.requestFrom(EEPROM, (byte)1);
  if (Wire.available()){
    byte readByte = Wire.read();
    if (debug) {
      Serial.print("Read byte: " );
      Serial.println(readByte, HEX);
    }
    return readByte;
  } 
  else {
    return 0x00; 
  }
}
boolean readBytesAt(uint16_t addr, byte *buffer, int length) {
  if (debug) {
    Serial.print("Lese ");
    Serial.print("Addresse [");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print((int)((addr) & 0xFF), HEX);
    Serial.print("]: ");
  }
  openEEPROMAdress(addr);
  Wire.endTransmission();
  //delay(1);
  // einzelbyte lesen:
  Wire.requestFrom((int)EEPROM, length);
  for (int i=0; i< length; i++) {
    if (Wire.available()) {
      byte in = Wire.read();
      if (debug) {
        Serial.print(in);
        Serial.print(", ");
      }      
      buffer[i] = in;
      //  delay(1);    
    } 
    else {
      return false; 
    }
    // delay(1);
  }
  if (debug) {

    Serial.println("");
  }
  return true;
}

void writeBytesTo(uint16_t addr, byte * b, byte length) {
  openEEPROMAdress(addr );
  if (debug) {
    Serial.print("Addresse [");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print((int)((addr) & 0xFF), HEX);
    Serial.print("]: ");

  }
  /*  if (debug) {
   Serial.print("Addresse 2:");
   Serial.println((int)((addr) & 0xFF), HEX);
   }*/

  uint16_t i = addr;
  uint16_t page=addr>>7;
  do {
    if (i%32==0) {
      if (i>addr || addr>>7 > page) {
        Wire.endTransmission();
        delay(3);
        openEEPROMAdress(i);
        if (addr>>7 > page){
          if (debug) {

            Serial.println("open next page");
          }
        } 
        else {
          //Serial.println("flush"); 
        }
        
      } 

    }
    // einzelbyte schreiben:
    Wire.write(b[i-addr]);
    if   (debug) {
      Serial.print(b[i-addr]);
      Serial.print(", ");
    }
    i++;
    page=addr>>7;
  } 
  while (i<addr+length);
  if (debug) {

    Serial.println();
  }
  Wire.endTransmission();
  delay(3);
}

const uint16_t dataSize = 15;

void setup() {
  //this will breake the test with the ATMEL 512kbit EEPROM. :-(
  //Tlc.init();
  const byte startValue=0x42;
  byte data[dataSize];
  long     lines=4300;
  Wire.begin(); 
  int byteAddress = 0x0;
  Serial.begin(115200);
  delay(3000);
  Serial.println("Schreibe mit arduino Wire...: ");
  byte b = startValue;
  long writeStart = micros();
  for (uint16_t i = 0; i<lines; i++) {
    for (int j = 0; j<dataSize; j++) {
      data[j] = b;
      b++;
    }

    writeBytesTo(i*dataSize, data, dataSize);
    Serial.print(".");
  }
  Serial.println(micros()-writeStart);
  Serial.print(" us for writing");

  Serial.println("");
  Serial.println("Moment warten");
  delay(10);
  Serial.println("Und noch mal lesen");
  byte inData[dataSize];
  long readStart = micros();
  for (int rot = 0; rot<5; rot++) {
    b=startValue;
    for (uint16_t i = 0; i<lines; i++) {
      boolean ok = readBytesAt(i*dataSize,inData, dataSize);

      for (int j = 0; j<dataSize; j++) {
        if(inData[j] != b) {
          Serial.print("Fehler bei: ");
          Serial.print(i);
          Serial.print(", ");
          Serial.print(j);
          Serial.print(" soll: ");
          Serial.print(b);
          Serial.print(" is: ");
          Serial.println(inData[j]);

        } 
        b++;
      }
      if(!ok) {
        Serial.println();
        Serial.print("Uebertragungsfehler: ");
        Serial.print(i);
        Serial.println();
      } 
      else {
        //Serial.print("."); 
      }
    }
  }
  long time = micros() - readStart;
  Serial.println();
  Serial.print("fertig nach ");
  Serial.print(time);
  Serial.print(" micros");
  delay(1000);

}



void loop() {
  delay(10000); 
}













