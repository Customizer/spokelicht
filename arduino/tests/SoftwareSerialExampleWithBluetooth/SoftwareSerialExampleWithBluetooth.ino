#include <SoftwareSerial.h>
#include <Wire.h>

/**
  Test program that reads data from the bluetooth module. Every read byte is stored in the EEPROM. If a ! is received
  it is not stored, but the content of the EEPROM is written to Serial (not soft serial!). If a $ is received the 
  internal data-end pointer is cleard and thereby the written text is ignored. (So start with sending a $ if you use the 
  programm for the first time)
**/
// PIN 1 is connected to ground. otherwise it woud be 0x50
const byte EEPROM = 0x51;
// enable or disable debugging via serial
boolean debug = false;
// RX and TX Pin _of the bluetooth module_. Connect RX and TX of the module to these pins. 
#define RX_PIN 5
#define TX_PIN 8

SoftwareSerial mySerial(TX_PIN, RX_PIN); // mySerial(RX, TX) -> Note that the pins are crossed.
// RX of the bluetooth board needs to be connected to software serial's TX Pin and vice versa.

void setup()  
{
  Serial.begin(9600);
  Serial.println("Goodnight moon!");

  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  mySerial.println("Hello, world?");
  Wire.begin(); 

}

int writeCount = 0;
boolean readMode = true;
void loop() // run over and over
{
  if (readMode) {
    byte endOfData=readByteAt(0);
    Serial.print("End of Data is at:");
    Serial.println(endOfData);

    Serial.println("EEProm Inhalt:");
    
    for (int i = 1; i<= endOfData; i++) {
      Serial.print((char)readByteAt(i));
    }    
    readMode = false;
  }
  if (mySerial.available()) {
    byte inByte = mySerial.read();
    if (inByte == '!') {
       readMode = true; 
    } else if (inByte == '$') {
      // reset End-Of-Data Pointer.
       writeByteTo(0,0); 
       Serial.println("Pointer zurueckgesetzt");
    } else {
      
      Serial.print("Speichere: ");
      Serial.println((char)inByte);
      byte endOfData=readByteAt(0)+1;
      //Serial.print("End of Data: " );
      //Serial.println(endOfData);
      delay(10);
      writeByteTo(endOfData,inByte);
      Serial.print("Speichere neues ende:");
      Serial.println(endOfData);
      delay(10);
      writeByteTo(0,endOfData);
      
    }
  }
  if (Serial.available()) {
    byte inByte = Serial.read();
    mySerial.write(inByte);
  }



}

void writeByteTo(int addr, byte b) {
  Wire.beginTransmission(EEPROM);
  // Adresse schreiben
  Wire.write((addr >> 8) & 0xFF);
  Wire.write((int)((addr) & 0xFF));
  if (debug) {
    Serial.print("Addresse 1 / 2 :");
    Serial.print((addr >> 8) & 0xFF, HEX);
    Serial.print(" / ");
    Serial.println((int)((addr) & 0xFF), HEX);
  }
  // einzelbyte schreiben:
  Wire.write(b);
  Wire.endTransmission();
  writeCount++;
  if (writeCount > 1000) {
     Serial.println("hier stimmt was nicht. Stopp!") ;
     
     while (true) {}
  }
}

byte readByteAt(int addr) {
/*  if (debug) {
    Serial.print("Lese ");
    Serial.println(addr,HEX);
  }*/
  Wire.beginTransmission(EEPROM);
  // Adresse schreiben
  Wire.write((int)((addr >> 8) & 0xFF));
  Wire.write((int)(addr & 0xFF));
  Wire.endTransmission();

  // einzelbyte lesen:
  Wire.requestFrom(EEPROM, (byte)1);
  if (Wire.available()){
    byte readByte = Wire.read();
    if (debug) {
      Serial.print("Read byte: " );
      Serial.println(readByte, HEX);
    }
    return readByte;
  } 
  else {
    return 0x00; 
  }
}

