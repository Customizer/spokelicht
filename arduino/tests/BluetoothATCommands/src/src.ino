/*
The posible baudrates are:
1-------1200
2-------2400
3------4800
4------9600
5------19200
6-------38400
7------57600
8-----115200
9------230400
A-----460800
B-----921600
C-----1382400


Important. If you re-set the BT baud rate, you must change
the myserial baud to match what was just set.

Assuming the BT module baud default is 9600 Baud, set mySerial
to 9600 and you want to change it to 57600:
1)	leave the line:  //mySerial.begin(9600); as you have
	to been at the current baud setting to be able to send the
	command to the BT module.
2) 	uncomment the line:  //mySerial.print("AT+BAUD7");
3)	run the sketch.  This will set the baud to 57600. However,
	you may not see the response OK.
4) 	uncomment the line //mySerial.begin(57600); and download
	the sketch again - this sets myserial to the new setting.
5) 	now that myserial is set to 57600 open the serial monitor -
	this will restart the sketch.  You should see the BT module
	responding to the change.


*/
#include <SoftwareSerial.h>

SoftwareSerial mySerial(7, 6); // TX, RX

void sendATCommand(String command) {
  delay(1000);
  Serial.print(command);
}

void setup() {
//	Serial.begin(9600);

  Serial.println("Waiting 1 second before starting!");


//	mySerial.begin(115200);
  Serial.begin(115200);

  sendATCommand("AT");

  sendATCommand("AT+VERSION");
  sendATCommand("AT+VERSION");

//	sendATCommand("AT+NAMESpokelichtArne");

  /* Attention: When setting Baud rate to 115200, the AT
   * command responses are not readable any more. But changing
   * settings is still possible.
   */
  sendATCommand("AT+BAUD7");

}

void loop() {
  if (mySerial.available()) {
    Serial.write(mySerial.read());
  }
  if (Serial.available())
    mySerial.write(Serial.read());

}
