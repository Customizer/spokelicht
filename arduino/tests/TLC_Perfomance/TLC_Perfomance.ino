// Performance measuring: How long does it take to perform n color changes
#include "Tlc5940.h"

int numleds=60;

#define REP 10000
void setup() {
  Tlc.init();
  delay(1000);
  setAllLEDs(0);
  delay(1000);
  long wait = 0;
  long start = micros();
  for (long j = 0 ; j<REP; j++){
    Tlc.setAll((int)j);
    while(Tlc.update()) {
      wait++;
    }
  } 
  long end = micros();
  Serial.begin(9600);
  Serial.print("done time: ");
  Serial.print(end - start);
  Serial.print(" us,");
  Serial.print(" rate: ");
  Serial.print((end -start) / REP);
  Serial.print(" wait-count: ");
  Serial.println(wait);

}
void loop() {
  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);              // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);              // wait for a second
}

void setAllLEDs(int color) {
  for (int i=0; i<numleds; i++) {

    Tlc.set(i,color);
  }  
}



