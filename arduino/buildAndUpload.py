#!/usr/bin/python

# ensure compatibility between Pythons print functions in V2 and V3
from __future__ import print_function

# This script is used to compile and upload sketches to the Arduino
# via USB or Bluetooth.
# Usage:
#   python upload usb
#   python upload bluetooth

import platform
import sys
import serial
import threading
import time
from subprocess import call

def resetArduino():
  print("Waitng to reset Arduino");
  time.sleep(0)
  print("Resetting Arduino");  
  ser = serial.Serial(port = device, baudrate=baud)
   
  ser.close()
  ser.open()
  ser.write("r$".encode())
  ser.close()
  return
  print("done")

if len(sys.argv) == 1:
  print("Please add parameter 'usb' or 'bluetooth'")
  sys.exit()


# build the project
call(["platformio", "run"])

# upload the project
deviceType = sys.argv[1]
usbCommand = "usb"
bluetoothCommand = "bluetooth"
system = platform.system()
if system == "Linux":
  print("Penguin detected. Using Linux settings...")
  if deviceType == usbCommand:
    device = "/dev/ttyUSB0"
  elif deviceType == bluetoothCommand:
    device = "/dev/rfcomm0"
elif system == "Darwin":
  print("Mac detected. Setting properties accordingly...")
  if deviceType == usbCommand:
    device = ""
  elif deviceType == bluetoothCommand:
    device = "/dev/tty.spokelichtb-DevB"
#    device = "/dev/tty.SPOKELICHT-DevB"
elif system == "Windows":
  print("Windows detected. Setting properties accordingly...")
  if deviceType == usbCommand:
    device = "COM3"
  elif deviceType == bluetoothCommand:
    device = "COM6"

baud = 57600

print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
print("   Build done. Now uploading")
print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
print("")
print("")

if deviceType == bluetoothCommand:
  # asynchronously call reset to resolve timing problems
  print("Starting Thread")
  t1 = threading.Thread(target=resetArduino)
  t1.start();

if device == "":
  call(["platformio","run", "-t", "uploadlazy"]);
else:
  call(["platformio","run", "-t", "uploadlazy", "--upload-port", device]);

