#!/usr/bin/python

# ensure compatibility between Pythons print functions in V2 and V3
from __future__ import print_function


# The following steps only work with the AVR ISP MKII programmer and 
# the ATmega328 chip.

from subprocess import call
import platform
import time


system = platform.system()
if system == "Linux":
  print("Penguin detected. Using Linux settings...")
  # unlock bootloader and set fuses
  call(["sudo", "avrdude", "-v", "-v", "-cstk500v2", "-p", "atmega328p", "-P", "usb",
    "-e", "-Ulock:w:0x3F:m", "-Uefuse:w:0x05:m", "-Uhfuse:w:0xDA:m",
    "-Ulfuse:w:0xFF:m"])

  #flash bootloader
  call(["sudo", "avrdude", "-v", "-v", "-cstk500v2", "-p", "atmega328p", "-P", "usb",
    "-U", "flash:w:ATmegaBOOT_168_atmega328.hex"])

  # lock bootloader
  call(["sudo", "avrdude", "-v", "-v", "-cstk500v2", "-p", "atmega328p", "-P", "usb",
    "-Ulock:w:0x0F:m"])
elif system == "Windows":
  print("Windows detected. Settings properties accordingly...")
  # unlock bootloader and set fuses
  call(["avrdude", "-v", "-v", "-cstk500v2", "-p", "atmega328p", "-P", "usb",
    "-e", "-Ulock:w:0x3F:m", "-Uefuse:w:0x05:m", "-Uhfuse:w:0xDA:m",
    "-Ulfuse:w:0xFF:m"])
  time.sleep(3)
  #flash bootloader
  call(["avrdude", "-v", "-v", "-cstk500v2", "-p", "atmega328p", "-P", "usb",
    "-U", "flash:w:ATmegaBOOT_168_atmega328.hex"])
  time.sleep(3)
  # lock bootloader
  call(["avrdude", "-v", "-v", "-cstk500v2", "-p", "atmega328p", "-P", "usb",
    "-Ulock:w:0x0F:m"])
